import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditAgency } from "../redux/redux";

import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";

import "react-html5-camera-photo/build/css/index.css";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";
import request from "sync-request";

import { geolocated } from "react-geolocated";

var emojiStrip = require("emoji-strip");
var uuidv1 = require("uuid/v1");

const empty = {
  description: "",
  cnpj: "",
  socialname: "",
  cep: "",
  street: "",
  number: "",
  complemento: "",
  bairro: "",
  city: "",
  state: "",
  fone1: "",
  email: ""
};

class RegisterPoint extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      type: { value: "none", label: "Selecione um expediente" },
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      dataUri: null,
      cameraerror: false
    };
  }

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  componentDidMount() {
    if (this.props.session.edit_agency != null) {
      console.log(this.props.session.edit_agency);
      this.setState({
        init: this.props.session.edit_agency,
        isEdit: true,
        image: this.props.session.edit_agency.photo
      });
    }
    this.setState({ show: true });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      description: Yup.string().required("observação obrigatoria")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  geoDecode = (lat, lng) => {
    axios
      .get(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
          lat +
          "," +
          lng +
          "&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk",
        {}
      )
      .then(response =>
        alert(
          "Local da Denuncia: " +
            JSON.stringify(response.data.results[0].formatted_address)
        )
      );
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    if (this.state.dataUri == null) {
      this.setState({
        showerror: true,
        messageerror: "Tire uma foto ou selecione uma imagem"
      });
      setSubmitting(false);
      return;
    }

    if (values.description == null) {
      this.setState({
        showerror: true,
        messageerror: "Digite a descrição da denucna"
      });
      setSubmitting(false);
      return;
    }

    var descr = emojiStrip(values.description);

    axios
      .post(
        "https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/register-point",
        {
          login: this.props.session.login.login,
          name: "pointimage" + uuidv1(),
          content: this.state.dataUri,
          description: descr,
          lat: this.props.session.point.lat,
          lng: this.props.session.point.lng,
          address: this.props.session.point.logradouro,
          pointtype: this.props.session.point.tipo
        },
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          }
        }
      )
      .then(res => {
        setSubmitting(false);
        this.setState({ showinfo: true });
      });

    return;
  };

  handleCloseInfo = e => {
    history.push("/app/select-map");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      console.log(fileInfo);
      this.setState({ dataUri: fileInfo.base64 });
    }; // reader.onload
  };

  render() {
    var _init = this.state.init;

    console.log(_init);

    return (
      <div className="animated fadeIn">
        <Formik
          initialValues={this.state.init}
          enableReinitialize
          validate={this.myValidator(this.validationSchemaComplete)}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Registrar Denúncia</CardHeader>
                  <CardBody>
                    <FormGroup>
                      {this.state.dataUri ? (
                        <img
                          className="col-md-10"
                          alt="imgCamera"
                          src={this.state.dataUri}
                        />
                      ) : (
                        <Camera
                          isDisplayStartCameraError={false}
                          onCameraError={error => {
                            this.setState({ cameraerror: true });
                          }}
                          idealFacingMode={FACING_MODES.ENVIRONMENT}
                          onTakePhoto={dataUri => {
                            this.setState({ dataUri: dataUri });
                          }}
                          imageType={IMAGE_TYPES.JPG}
                          imageCompression={0.8}
                          isImageMirror={false}
                        />
                      )}
                    </FormGroup>
                    <FormGroup>
                      <Label className="green-label" for="description">
                        Observação
                      </Label>
                      <Input
                        className="col-md-10"
                        invalid={touched.description && !!errors.description}
                        type="textarea"
                        rows="8"
                        name="description"
                        id="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>
                    {this.state.cameraerror ? (
                      <FormGroup>
                        <Col>
                          <input
                            className="col-md-3"
                            style={{ marginTop: "10px" }}
                            type="file"
                            onChange={e => this.handleFileSelect(e)}
                          />
                        </Col>
                        <FormFeedback>{errors.photo}</FormFeedback>
                      </FormGroup>
                    ) : (
                      <div />
                    )}
                  </CardBody>
                </Card>

                <div className="row justify-content-center mb-5 mt-3">
                  <LaddaButton
                    type="submit"
                    className="btn btn-primary btn-ladda"
                    loading={isSubmitting}
                    onClick={e => {
                      handleSubmit(e);
                      this.toggleBtn("expLeft");
                    }}
                  >
                    <i color="primary" className="fa fa-check" />
                    {isSubmitting ? " Enviando" : " Enviar"}
                  </LaddaButton>

                  <LaddaButton
                    type="button"
                    style={{ marginLeft: "10px" }}
                    className="btn btn-danger btn-ladda"
                    onClick={e => {
                      this.setState({ dataUri: null });
                      this.toggleBtn("expLeft");
                    }}
                  >
                    <i color="primary" className="fa fa-check" /> Nova Foto
                  </LaddaButton>
                </div>
                <div style={{ height: "50px" }} />
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>Ponto Viciado</ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Ponto Viciado</ModalHeader>
          <ModalBody>Registro efetuado com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency
};

const routerAgency = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RegisterPoint)
);

export default routerAgency;
