import React, { Component } from "react";

import { Input, CardBody, Card, Row, Col } from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Media  } from 'reactstrap';

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

import { connect } from "react-redux";
import { doLogin, addPoint } from "../redux/redux";

import { GoogleMap, LoadScript } from '@react-google-maps/api';
import { Marker } from '@react-google-maps/api';

import { Container, Button2, Link } from 'react-floating-action-button';

import axios from "axios";

import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { geolocated } from "react-geolocated";

import history from "../history/history";


import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      logradouro: null,
      lat: null,
      lng: null,
      dropdownOpen: false,
      tipo: "Tipo de Denuncia",
      marker: null,
      currentMarker: null,
      points: [],
      currentpoint: null
    }
  }
  
  toggle = () => {
    this.setState({ dropdownOpen: !this.state.dropdownOpen });
  }

  componentDidMount() {

    this.setState({ show: true, tipo: localStorage.getItem('type') });
    
    axios.post('https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/users', { type: 'findallpoints', login: this.props.session.login.login }, { headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } })
      .then(res => {
        console.log(res.data);
        this.setState({ points: res.data.body });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onShow = () => {
    this.setState({ show: true });
  };

  onHide = () => {
    this.setState({ show: false });
  };


  geoDecode = (lat1, lng1) => {
    console.log("update");
    if (!this.state.logradouro) {
      axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat1 + ',' + lng1 + '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk', {})
        .then(response => {
          this.setState({ logradouro: response.data.results[0].formatted_address, lat: lat1, lng: lng1 })
        })
    }
  }
  
  updateAddress = (lat1, lng1) => {
    axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat1 + ',' + lng1 + '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk', {})
      .then(response => {
        this.setState({ logradouro: response.data.results[0].formatted_address, lat: lat1, lng: lng1 })
      })
  }


  updateJustAddress = (lat1, lng1) => {
    axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat1 + ',' + lng1 + '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk', {})
      .then(response => {
        this.setState({ logradouro: response.data.results[0].formatted_address })
      })
  }
  
  renderMarkers = (points) => {
    if (points.length > 0) {
      return points.map((point, index) => (
        
        <Marker
              onClick={e => this.showPointDetails(point)}
              draggable={false}
              position={{
                lat: Number(point.lat),
                lng: Number(point.lng)
              }}
              
              icon={{
                  url: point.other == 'pointgrave'?'/assets/newicons/PINO_GRAVE.png':
                       point.other == 'pointleve'?'/assets/newicons/PINO_LEVE.png':
                       point.other == 'pointmedio'?'/assets/newicons/PINO_MEDIO.png':'',
                  rotation: this.props.coords.heading
                 
               }}
            >
            
           
            </Marker>
      ));
    }
    else return [];
  }
  
  showPointDetails = (point) => {
    this.setState({ currentpoint: point })
  }
  changeModal = () => {
    this.setState({ currentpoint: null })
  }

  render() {
    return (
      <div>
       { this.props.coords? this.geoDecode(this.props.coords.latitude,this.props.coords.longitude):null}
       
       
        
        
        
       <LoadScript
          id="script-loader"
          googleMapsApiKey="AIzaSyBwBkgyM2rZVTNKnRKMAy8AD4p5kD0eCGY"
        >
          { this.state.lat && this.state.lng?
  
           <GoogleMap
            id="InfoBox-example"
            mapContainerStyle={{
              height: "70vh"
            }}
            zoom={15}
            center={{
              lat: this.props.coords.latitude,
              lng: this.props.coords.longitude
            }}
             options={{gestureHandling: "greedy", mapTypeControl: false, styles: [ {
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#8ec3b9"
          			} ]
          		}, {
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1a3646"
          			} ]
          		}, {
          			"featureType" : "administrative",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "administrative.country",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#4b6878"
          			} ]
          		}, {
          			"featureType" : "administrative.land_parcel",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#64779e"
          			} ]
          		}, {
          			"featureType" : "administrative.province",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#4b6878"
          			} ]
          		}, {
          			"featureType" : "landscape.man_made",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#334e87"
          			} ]
          		}, {
          			"featureType" : "landscape.natural",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#023e58"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#283d6a"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#6f9ba5"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"featureType" : "poi.park",
          			"elementType" : "geometry.fill",
          			"stylers" : [ {
          				"color" : "#023e58"
          			} ]
          		}, {
          			"featureType" : "poi.park",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#3C7680"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#304a7d"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "labels.icon",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#98a5be"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#2c6675"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#255763"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#b0d5ce"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#023e58"
          			} ]
          		}, {
          			"featureType" : "transit",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "transit",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#98a5be"
          			} ]
          		}, {
          			"featureType" : "transit",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"featureType" : "transit.line",
          			"elementType" : "geometry.fill",
          			"stylers" : [ {
          				"color" : "#283d6a"
          			}, {
          				"visibility" : "on"
          			}, {
          				"weight" : 4
          			} ]
          		}, {
          			"featureType" : "transit.station",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#3a4762"
          			} ]
          		}, {
          			"featureType" : "water",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#0e1626"
          			} ]
          		}, {
          			"featureType" : "water",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#4e6d70"
          			} ]
          		} ]  }}
          >
          <Col className="mt-2" >
            <Dropdown className="mb-2" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
          <DropdownToggle style={{width:"100%"}} caret>
            {this.state.tipo=='pointmedio' ? 
              'Ponto Viciado Médio':
               this.state.tipo=='pointleve'?
               'Ponto Viciado Leve': 'Ponto Viciado Grave'
            }
          </DropdownToggle>
          <DropdownMenu style={{width:"100%"}} right>
                  
              <DropdownItem onClick={e => {this.setState({tipo:"pointleve"}); }}>Ponto Viciado Leve - Alguns Sacos</DropdownItem>
              <DropdownItem onClick={e => {this.setState({tipo:"pointmedio"}); }}>Ponto Viciado Médio - Até uma caçamba</DropdownItem>
              <DropdownItem onClick={e => {this.setState({tipo:"pointgrave"}); }}>Ponto Viciado Grave - Mais de uma caçamba</DropdownItem>
                 
          </DropdownMenu>
         
        </Dropdown>
        
        <Input
          className="mb-2"
          name="user"
          placeholder="CPF"
          returnKeyType='go'
          autoComplete="username"
          value={this.state.logradouro? this.state.logradouro:"Procurando localização..."}
          onChange={e => {
          this.setState({logradouro: e.target.value })
        }}
        />   
          </Col>
            <Marker
              onLoad={marker => {
                console.log('marker: ', marker)
                this.setState({currentMarker: marker});
              }}
              draggable={false}
              position={{
                lat: this.props.coords.latitude,
                lng: this.props.coords.longitude
              }}
              onDragEnd={e => {
                
                var latlng = JSON.parse(JSON.stringify(e.latLng));
                console.log(latlng);
                
                
              }}
              
               icon={{
                  url: '/assets/icone2.png',
                  rotation: this.props.coords.heading
                 
               }}
            />
            
            {this.state.lat?
             <Marker
              onLoad={marker => {
                console.log('marker: ', marker)
                this.setState({marker:marker})
              }}
              draggable={true}
              position={{
                lat: this.state.lat,
                lng: this.state.lng
              }}
              zIndex={20}
              onDragEnd={(e) => {
                  var latlng = JSON.parse(JSON.stringify(this.state.marker.position));
                  console.log(latlng);
                  
                  this.updateAddress(latlng.lat, latlng.lng);
              }}
              
               icon={{
                  url: this.state.tipo == 'pointgrave'?'/assets/newicons/PONTO_GRAVE_32x32.png':
                       this.state.tipo == 'pointleve'?'/assets/newicons/PONTO_LEVE_32x32.png':
                       this.state.tipo == 'pointmedio'?'/assets/newicons/PONTO_MEDIO_32x32.png':'',
               }}
            />:<div/>}
              <Col style={{position: 'absolute',    bottom: 0, width:"100%", marginBottom:'15px'}}>
              
                    
                          <LaddaButton
                       style={{marginLeft:'10%',  width:"75%"}}
                        
                        className="btn btn-primary btn-ladda"
                        
                        onClick={e => {
                          this.props.addPoint({lat:this.state.lat,lng:this.state.lng,logradouro:this.state.logradouro, tipo:this.state.tipo})
                        
                          if(this.state.tipo == 'Caçamba'){
                            history.push('/app/complaint/list')
                          } else {
                            history.push('/app/complaint/register')
                          }
                        }}
                      >
                        <i color="danger" className="fa fa-check" /> Registrar
                      </LaddaButton>
                    
                     
                      
                  
                   </Col>
                   
                   {this.renderMarkers(this.state.points)}
          </GoogleMap>
           :
          <div style={{color:"white"}}>Procurando localização...</div>}
        </LoadScript>
        
        <Modal isOpen={this.state.currentpoint != null}>
          <ModalHeader>
            {this.state.currentpoint? this.state.currentpoint.addreess : ""}
          </ModalHeader>
          <ModalHeader style={{color: '#FE574F', fontWeight:'bold'}}>
            {this.state.currentpoint? (this.state.currentpoint.other=='pointmedio' ? 
              'Ponto Viciado Médio':
               this.state.currentpoint.other=='pointleve'?
               'Ponto Viciado Leve': 'Ponto Viciado Grave') : ""
            }
          </ModalHeader>
          <ModalBody>
            <img style={{width: "100%"}} src={this.state.currentpoint? 'https://grx-images.s3.amazonaws.com/' + this.state.currentpoint.photo : "" } />
          </ModalBody>
          
          <ModalFooter>
            <Button color="primary" onClick={e => this.changeModal()}>Fechar</Button>{' '}
          </ModalFooter>
        </Modal>
        
      </div>
    )
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  addPoint
};

const MapConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);



export default geolocated({
  positionOptions: {
    enableHighAccuracy: true,
  },
  watchPosition: true,
  userDecisionTimeout: 5000,
})(MapConnect);


