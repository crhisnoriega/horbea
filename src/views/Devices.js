import React, { Component } from "react";

import {
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";

import { setEditDevice } from "../redux/redux";

const empty = { value: "Selecione", label: "Selecione" };

class Devices extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      expLeft: false,
      dropdownOpen: false,
      show: true,
      selectItem: "Selecione",
      groups: [],
      groupsTable: {},
      group: empty,
      category: empty,
      fuel: empty,
      value: ["UT", "OH"],
      modelo: "",
      placa: "",
      init: {
        name: "",
        phone: "",
        model: "",
        contact: "",
        placa: "",
        uniqueId: ""
      },
      image: null
    };
    this.groupsTable = {};
  }

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  componentDidMount() {
    var _this = this;
    console.log(this.props.session.deviceEdit);

    axios
      .post(
        "/api/getapicall",
        {
          url: "/groups",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        var selectg = null;
        var groups1 = [empty];

        console.log(_this.props.session.deviceEdit);

        res.data.forEach(item => {
          if (
            _this.props.session.deviceEdit != null &&
            _this.props.session.deviceEdit.groupId == item.id
          ) {
            selectg = item;
          }
          groups1.push({ value: item.id, label: item.name });
        });

        if (selectg == null) {
          selectg = { id: "Selecione", name: "Selecione" };
        }

        console.log(groups1);

        if (_this.props.session.deviceEdit) {
          this.setState({
            groups: groups1,
            group: { value: selectg.id, label: selectg.name },
            placa: _this.props.session.deviceEdit.attributes.plate
              ? _this.props.session.deviceEdit.attributes.plate
              : "",
            modelo: _this.props.session.deviceEdit.model
              ? _this.props.session.deviceEdit.model
              : "",
            category: {
              value: _this.props.session.deviceEdit.category,
              label: _this.props.session.deviceEdit.category
            },
            fuel: {
              value: _this.props.session.deviceEdit.attributes.fuel,
              label: _this.props.session.deviceEdit.attributes.fuel
            },
            init: {
              ..._this.props.session.deviceEdit,
              placa: _this.props.session.deviceEdit.attributes.plate
            },
            image: _this.props.session.deviceEdit.photo
          });
        } else {
          this.setState({
            groups: groups1,
            init: {
              name: "",
              phone: "",
              model: "",
              contact: "",
              placa: "",
              uniqueId: ""
            }
          });
        }
      });
  }

  // validation
  validationSchema = function(values) {
    return Yup.object().shape({
      uniqueId: Yup.string().required("IMEI do dispositivo obrigatorio"),
      category: Yup.string().required("categoria obrigatoria"),
      fuel: Yup.string().required("combustivel obrigatoria"),
      contact: Yup.string().required("contato obrigatorio"),
      phone: Yup.string().required("numero de telefone obrigatorio")
    });
  };

  myValidator = mySchema => {
    return values => {
      values.category = this.state.category;
      values.fuel = this.state.fuel;

      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        if (this.state.category == "") {
          return { category: "categoria obrigatorio" };
        }

        if (this.state.fuel == "") {
          return { fuel: "combustivel obrigatorio" };
        }

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(JSON.stringify(this.state));
    values.category = this.state.category;
    values.group = null;

    var deviceinfo = {};

    deviceinfo.uniqueId = values.uniqueId;
    deviceinfo.name = this.state.modelo + " " + this.state.placa;
    deviceinfo.groupId = this.state.group.value;
    deviceinfo.category = this.state.category.value;
    deviceinfo.phone = values.phone;
    deviceinfo.attributes = {
      plate: values.placa,
      fuel: this.state.fuel.value,
      operator: values.operator
    };
    deviceinfo.model = values.model;
    deviceinfo.contact = values.contact;
    deviceinfo.photo = this.state.image;

    console.log(JSON.stringify(deviceinfo));

    if (this.props.session.deviceEdit) {
      deviceinfo.id = this.props.session.deviceEdit.id;
      axios
        .post(
          "/api/putapicall",
          {
            url: "/devices/" + this.props.session.deviceEdit.id,
            data: deviceinfo,
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic " + this.props.session.token
            }
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          console.log(JSON.stringify(res));
          this.props.setEditDevice(null);
          this.props.session.deviceEdit = null;
          this.setState({
            category: empty,
            fuel: empty,
            group: empty,
            modelo: "",
            placa: "",
            image: null,
            init: {
              name: "",
              phone: "",
              model: "",
              contact: "",
              placa: "",
              uniqueId: ""
            }
          });
          setSubmitting(false);
        });
    } else {
      axios
        .post(
          "/api/apicall",
          {
            url: "/devices",
            data: deviceinfo,
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic " + this.props.session.token
            }
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          console.log(JSON.stringify(res));
          this.props.setEditDevice(null);
          this.setState({
            category: empty,
            fuel: empty,
            group: empty,
            modelo: "",
            placa: "",
            image: null,
            init: {
              name: "",
              phone: "",
              model: "",
              contact: "",
              placa: "",
              uniqueId: ""
            }
          });
          setSubmitting(false);
        });
    }
    return;
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      console.log(fileInfo);
      this.setState({ image: fileInfo.base64 });
    }; // reader.onload
  };

  render() {
    var categories = [
      "truck",
      "default",
      "taxi",
      "arrow",
      "pickup",
      "van",
      "animal",
      "car",
      "motorcycle",
      "offroad"
    ];

    var categories2 = categories.map(item => {
      return { value: item, label: item };
    });

    var fuels = ["Flex", "Gasolina", "Alcool", "Diesel", "GNV"];
    var fuels2 = fuels.map(item => {
      return { value: item, label: item };
    });

    var _init = this.state.init;

    return (
      <div>
        <Loading show={this.state.show} showSpinner={false} />
        <Card>
          <CardBody>
            <Formik
              enableReinitialize
              initialValues={_init}
              validate={this.myValidator(this.validationSchema)}
              onSubmit={this.handleSubmit}
              render={({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                setTouched
              }) => (
                <Container>
                  <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onBlur={handleBlur}
                        value={this.state.modelo + " " + this.state.placa}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="uniqueId">
                        Identificador/IMEI
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.uniqueId && !!errors.uniqueId}
                        type="text"
                        name="uniqueId"
                        id="uniqueId"
                        value={values.uniqueId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        mask="9999999999999999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.uniqueId}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="phone">
                        Telefone
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.phone && !!errors.phone}
                        type="text"
                        name="phone"
                        id="phone"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phone}
                        mask="(99) 99999-9999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.phone}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="operator">
                        Fornecedora
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.operator && !!errors.operator}
                        type="text"
                        name="operator"
                        id="operator"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.operator}
                      />
                      <FormFeedback>{errors.operator}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="phoneline">
                        Numero da Linha
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.operator && !!errors.phoneline}
                        type="text"
                        name="phoneline"
                        id="phoneline"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phoneline}
                      />
                      <FormFeedback>{errors.phoneline}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Modelo
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.model && !!errors.model}
                        type="text"
                        name="model"
                        id="model"
                        onChange={e => {
                          handleChange(e);
                          this.setState({ modelo: e.target.value });
                        }}
                        onBlur={handleBlur}
                        value={values.model}
                      />
                      <FormFeedback>{errors.model}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Placa
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.placa && !!errors.placa}
                        type="text"
                        name="placa"
                        id="placa"
                        onChange={e => {
                          handleChange(e);
                          this.setState({ placa: e.target.value });
                        }}
                        onBlur={handleBlur}
                        value={values.placa}
                        mask="aaa-9999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.placa}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="contact">
                        Contato
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.contact && !!errors.contact}
                        type="text"
                        name="contact"
                        id="contact"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.contact}
                      />
                      <FormFeedback>{errors.contact}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="photo">
                        Foto do Veiculo
                      </Label>
                      {this.state.image ? (
                        <img src={this.state.image} />
                      ) : (
                        <div />
                      )}
                      <input
                        className="col-md-6"
                        type="file"
                        onChange={e => this.handleFileSelect(e)}
                      />
                      <FormFeedback>{errors.photo}</FormFeedback>
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="category">
                          Categoria
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.category}
                          options={categories2}
                          onChange={category => {
                            errors.category = "";
                            if (category == null) {
                              category = "";
                            }
                            this.setState({ category: category });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.category}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="group">
                          Grupo
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.group}
                          options={this.state.groups}
                          onChange={group => {
                            errors.group = "";
                            if (group == null) {
                              group = "";
                            }
                            this.setState({ group });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.group}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="fuel">
                          Combustivel
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.fuel}
                          options={fuels2}
                          onChange={fuel => {
                            errors.fuel = "";
                            if (fuel == null) {
                              fuel = "";
                            }
                            this.setState({ fuel: fuel });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.fuel}
                        </Label>
                      </FormGroup>
                    </Row>

                    <FormGroup className="col-md-3">
                      <Label className="green-label" for="cpf">
                        Autonomia
                      </Label>
                      <InputGroup>
                        <Input
                          invalid={touched.autonomy && !!errors.autonomy}
                          type="text"
                          name="autonomy"
                          id="autonomy"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.autonomy}
                        />
                        <InputGroupAddon addonType="append">
                          KM/L
                        </InputGroupAddon>
                      </InputGroup>
                      <FormFeedback>{errors.autonomy}</FormFeedback>
                    </FormGroup>

                    <FormGroup className="col-md-3">
                      <Label className="green-label" for="cpf">
                        Limite de Velocidade
                      </Label>
                      <InputGroup>
                        <Input
                          invalid={touched.speed && !!errors.speed}
                          type="text"
                          name="speed"
                          id="speed"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.speed}
                        />
                        <InputGroupAddon addonType="append">
                          Km/h
                        </InputGroupAddon>
                      </InputGroup>
                      <FormFeedback>{errors.speed}</FormFeedback>
                    </FormGroup>

                    <Row className="justify-content-sm-start">
                      <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <LaddaButton
                          type="submit"
                          className="col-6 btn btn-primary btn-ladda"
                          loading={isSubmitting}
                          onClick={e => {
                            alert('vvv');
                            
                          }}
                        >
                          Enviar
                        </LaddaButton>
                      </Col>
                    </Row>
                  </Form>
                </Container>
              )}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  deviceEdit: state.deviceEdit
});

const mapDispatchToProps = {
  setEditDevice
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Devices)
);
