import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser } from "../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import history from "../history/history";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import Avatar from "react-avatar-edit";

const empty = {
  firstName: "",
  login: "",
  sexo: "",
  password: ""
};

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      init: empty,
      isEdit: false,
      showinfo: false,
      showerror: false
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    this.setState({
      show: true
    });

    console.log(this.props.session.login);

    if (this.props.session.login) {
      this.setState({
        show: true,
        avatar: this.props.session.login.photo,
        init: {
          firstName: this.props.session.login.firstName,
          login: this.props.session.login.login,
          sexo: "",
          password: ""
        }
      });
    }
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      firstName: Yup.string().required("nome do usuario"),
      password: Yup.string().required("senha obrigatoria"),
      login: Yup.string().required("CPF obrigatorio"),
      password2: Yup.string()
        .oneOf([Yup.ref("password"), null], "Senha nao bate")
        .required("Password confirm is required")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      firstName: Yup.string().required("nome do usuario"),
      login: Yup.string().required("CPF obrigatorio")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(values);

    values.id = this.props.session.login.id;
    values.photo = this.state.avatar;

    axios
      .put("/user/" + this.props.session.login.id, values, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ init: empty, showinfo: true });
        setSubmitting(false);
      })
      .catch(error => {
        console.log(error);
        this.setState({
          showerror: true,
          messageerror: JSON.stringify(error)
        });
        setSubmitting(false);
      });

    return;
  };

  onCrop(elem) {
    console.log(elem);
    this.setState({ avatar: elem });
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/users/list");
  };

  render() {
    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />

        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Perfil</CardHeader>
                  <CardBody>
                    <FormGroup>
                      <Label className="green-label" for="firstName">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.firstName && !!errors.firstName}
                        type="text"
                        name="firstName"
                        id="firstName"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.firstName}
                      />
                      <FormFeedback>{errors.firstName}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="login">
                        CPF (acesso)
                      </Label>
                      <Input
                        className="col-md-4"
                        name="login"
                        type="text"
                        autoComplete="username"
                        value={values.login}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        mask="999.999.999-99"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.login}</FormFeedback>
                    </FormGroup>

                    <Avatar
                      width={200}
                      height={200}
                      onCrop={this.onCrop}
                      onClose={this.onClose}
                      onCrop={e => {
                        this.onCrop(e);
                      }}
                      label={"Imagem"}
                      src={this.state.avatar}
                    />


                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="sexo">
                          Sexo
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.deviceReadonly}
                          options={[
                            { value: "Homem", label: "Homem" },
                            { value: "Mulher", label: "Mulher" }
                          ]}
                          onChange={sexo => {
                            errors.sexo = "";
                            if (sexo == null) {
                              sexo = "";
                            }
                            this.setState({ sexo });
                          }}
                          placeholder="Selecione"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup>
                        <Label className="green-label" for="password">
                          Senha
                        </Label>
                        <Input
                          className="col-md-10"
                          invalid={touched.password && !!errors.password}
                          type="password"
                          name="password"
                          id="password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password}
                        />
                        <FormFeedback>{errors.password}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="password2">
                          Confirmar Senha
                        </Label>
                        <Input
                          className="col-md-10"
                          invalid={touched.password2 && !!errors.password2}
                          type="password"
                          name="password2"
                          id="password2"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password2}
                        />
                        <FormFeedback>{errors.password2}</FormFeedback>
                      </FormGroup>
                    </Row>
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <LaddaButton
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </LaddaButton>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro salvando Vistoriador
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
