import React, { Component } from "react";

import { Input, CardBody, Card, Row, Col } from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Media } from 'reactstrap';

import Image from 'react-bootstrap/Image'

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";

import { GoogleMap, LoadScript } from '@react-google-maps/api';
import { Marker, InfoWindow } from '@react-google-maps/api';

import { Container, Button2, Link } from 'react-floating-action-button';

import axios from "axios";

import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { geolocated } from "react-geolocated";

import history from "../history/history";
import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

class MapAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      lat: null,
      lng: null,
      firstlat: null,
      firstlng: null,
      heading: 0,
      dropdownOpen: false,
      tipo: "Tipo de Denuncia",
      points: [],
      counter: 0,
      currentpoint: null,
      checkbutton: false,
      uncheckbutton: false,
      alreadycheck: undefined
    };

    this.counter = 0;
  }

  toggle = () => {
    this.setState({ dropdownOpen: !this.state.dropdownOpen });
  }


  componentDidMount() {
    window.scrollTo(0, 0)
    this.setState({ show: true });

    axios.post('https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/users', { type: 'findallpointsadmin', login: this.props.session.login.login }, { headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } })
      .then(res => {
        console.log(res.data);
        this.setState({ points: res.data.body });
      })
      .catch(error => {
        console.log(error);
      });

  }

  onShow = () => {
    this.setState({ show: true });
  };

  onHide = () => {
    this.setState({ show: false });
  };



  geoDecode = (lat1, lng1, heading1) => {
    if (!this.state.logradouro) {
      axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat1 + ',' + lng1 + '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk', {})
        .then(response => {
          this.setState({ logradouro: response.data.results[0].formatted_address, lat: lat1, lng: lng1 })
        })
    }

    if (!this.state.firstlat) {
      this.setState({ firstlat: lat1, firstlng: lng1, })
    }

    if (this.counter > 5) {
      this.setState({ heading: this.props.coords.heading })
      this.counter = 0;

    }
    else {
      this.counter++;
    }


    console.log(lat1);
    console.log(heading1);
  }

  updateAddress = (lat1, lng1) => {
    axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat1 + ',' + lng1 + '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk', {})
      .then(response => {
        this.setState({ logradouro: response.data.results[0].formatted_address, lat: lat1, lng: lng1 })
      })
  }

  storeChoose = (type) => {
    localStorage.setItem('type', type);
    history.push("/app/map")
  }

  renderMarkers = (points) => {
    if (points.length > 0) {
      return points.map((point, index) => (

        <Marker
              onClick={e => this.showPointDetails(point)}
              draggable={false}
              position={{
                lat: Number(point.lat),
                lng: Number(point.lng)
              }}
                icon={{
                  url: point.other == 'pointgrave'?'/assets/newicons/PINO_GRAVE.png':
                       point.other == 'pointleve'?'/assets/newicons/PINO_LEVE.png':
                       point.other == 'pointmedio'?'/assets/newicons/PINO_MEDIO.png':'',
                  rotation: this.props.coords.heading
                 
               }}
            >
            
            
            </Marker>
      ));
    }
    else return [];
  }

  showPointDetails = (point) => {
    this.setState({ currentpoint: point, alreadycheck: undefined })
    axios.post('https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/register-point', {
        op: 'alreadycheck',
        pointid: point.id,
        login: this.props.session.login.login

      }, {
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }
      })
      .then(res => {
        if (res.data.body) {
          this.setState({ alreadycheck: true });
        }
        else {
          this.setState({ alreadycheck: false });
        }
      });

  }
  changeModal = () => {
    this.setState({ currentpoint: null })
  }

  sendValidation = (status) => {
    var data = {
      op: 'pointvalidation',
      validation: {
        pointid: this.state.currentpoint.id,
        login: this.props.session.login.login,
        status: status
      }
    }

    console.log(data);
    axios.post('https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/register-point', data, {
        headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }
      })
      .then(res => {
        this.setState({ checkbutton: false, uncheckbutton: false, currentpoint: null });
      });
  }

  checkPoint = () => {
    this.setState({ checkbutton: true });
    this.sendValidation('confirm');
  }

  unCheckPoint = () => {
    this.setState({ uncheckbutton: true });
    this.sendValidation('confirm');
  }

  render() {

    return (
      <div>
       { this.props.coords? this.geoDecode(this.props.coords.latitude,this.props.coords.longitude, this.props.coords.heading):null}
       <LoadScript
          id="script-loader"
          googleMapsApiKey="AIzaSyBwBkgyM2rZVTNKnRKMAy8AD4p5kD0eCGY"
        >
          { window.google && this.state.lat && this.state.lng?
  
           <GoogleMap
            id="InfoBox-example"
            mapContainerStyle={{
              height: "70vh"
            }}
            zoom={15}
            center={{
              lat: this.state.firstlat,
              lng: this.state.firstlng
            }}
            
            options={{ gestureHandling: "greedy", mapTypeControl: false, styles: [ {
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#8ec3b9"
          			} ]
          		}, {
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1a3646"
          			} ]
          		}, {
          			"featureType" : "administrative",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "administrative.country",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#4b6878"
          			} ]
          		}, {
          			"featureType" : "administrative.land_parcel",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#64779e"
          			} ]
          		}, {
          			"featureType" : "administrative.province",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#4b6878"
          			} ]
          		}, {
          			"featureType" : "landscape.man_made",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#334e87"
          			} ]
          		}, {
          			"featureType" : "landscape.natural",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#023e58"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#283d6a"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#6f9ba5"
          			} ]
          		}, {
          			"featureType" : "poi",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"featureType" : "poi.park",
          			"elementType" : "geometry.fill",
          			"stylers" : [ {
          				"color" : "#023e58"
          			} ]
          		}, {
          			"featureType" : "poi.park",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#3C7680"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#304a7d"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "labels.icon",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#98a5be"
          			} ]
          		}, {
          			"featureType" : "road",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#2c6675"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "geometry.stroke",
          			"stylers" : [ {
          				"color" : "#255763"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#b0d5ce"
          			} ]
          		}, {
          			"featureType" : "road.highway",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#023e58"
          			} ]
          		}, {
          			"featureType" : "transit",
          			"stylers" : [ {
          				"visibility" : "off"
          			} ]
          		}, {
          			"featureType" : "transit",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#98a5be"
          			} ]
          		}, {
          			"featureType" : "transit",
          			"elementType" : "labels.text.stroke",
          			"stylers" : [ {
          				"color" : "#1d2c4d"
          			} ]
          		}, {
          			"featureType" : "transit.line",
          			"elementType" : "geometry.fill",
          			"stylers" : [ {
          				"color" : "#283d6a"
          			}, {
          				"visibility" : "on"
          			}, {
          				"weight" : 4
          			} ]
          		}, {
          			"featureType" : "transit.station",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#3a4762"
          			} ]
          		}, {
          			"featureType" : "water",
          			"elementType" : "geometry",
          			"stylers" : [ {
          				"color" : "#0e1626"
          			} ]
          		}, {
          			"featureType" : "water",
          			"elementType" : "labels.text.fill",
          			"stylers" : [ {
          				"color" : "#4e6d70"
          			} ]
          		} ]  }}
          >
               <Col className="mt-2" >
              <Dropdown className="mb-2" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle style={{width:"100%"}} caret>
                  {this.state.tipo}
                </DropdownToggle>
                <DropdownMenu style={{width:"100%"}} right>
                  
                  <DropdownItem onClick={e => {this.setState({tipo:"Ponto Viciado Leve"}); this.storeChoose("pointleve");}}>Ponto Viciado Leve - Alguns Sacos</DropdownItem>
                  <DropdownItem onClick={e => {this.setState({tipo:"Ponto Viciado Médio"}); this.storeChoose("pointmedio");}}>Ponto Viciado Médio - Até uma caçamba</DropdownItem>
                  <DropdownItem onClick={e => {this.setState({tipo:"Ponto Viciado Grave"}); this.storeChoose("pointgrave");}}>Ponto Viciado Grave - Mais de uma caçamba</DropdownItem>
                 
                </DropdownMenu>
              </Dropdown>
              </Col>
          
            <Marker
              onLoad={marker => {
                console.log('marker: ', marker)
              }}
              draggable={false}
              position={{
                lat: this.props.coords.latitude,
                lng: this.props.coords.longitude
              }}
              
              icon={{
                  url: '/assets/newicons/user.png',
                  rotation: this.props.coords.heading
                 
               }}
                      
              
            />
            
            
            {this.renderMarkers(this.state.points)}
          </GoogleMap>
           :
          <div style={{color:"white"}}>Carregando...</div>}
        </LoadScript>
        
        <Modal isOpen={false}>
          <ModalHeader>
            {this.state.currentpoint? this.state.currentpoint.addreess : ""}
          </ModalHeader>
          <ModalHeader style={{color: '#FE574F', fontWeight:'bold'}}>
            {this.state.currentpoint? (this.state.currentpoint.other=='pointmedio' ? 
              'Ponto Viciado Médio':
               this.state.currentpoint.other =='pointleve'?
               'Ponto Viciado Leve': 'Ponto Viciado Grave') : ""
            }
          </ModalHeader>
          <ModalBody>
            <Image style={{width: "100%"}} src={this.state.currentpoint? 'https://grx-images.s3.amazonaws.com/' + this.state.currentpoint.photo : "" } />
          </ModalBody>
          
          {this.state.alreadycheck == undefined?
            <ModalFooter>
              <h4>
              {"carregando..."}
              </h4>
            </ModalFooter>
            :
            (!this.state.alreadycheck?
            <ModalFooter>
              <LaddaButton 
                type="button"
                className="btn btn-primary btn-ladda"
                loading={this.state.checkbutton}
                color="primary" 
                onClick={e => this.checkPoint()}>Existe Mesmo
              </LaddaButton>{' '}
              <LaddaButton 
                type="button"
                className="btn btn-danger btn-ladda"
                loading={this.state.uncheckbutton}
                color="primary" 
                onClick={e => this.unCheckPoint()}>
                Não Existe
              </LaddaButton>
              <LaddaButton 
                    type="button"
                    className="btn btn-secondary btn-ladda"
                    loading={this.state.uncheckbutton}
                    color="primary" 
                    onClick={e => this.changeModal()}>
                    Fechar
                  </LaddaButton>
            </ModalFooter>
            :
               <ModalFooter>
                  <LaddaButton 
                    type="button"
                    className="btn btn-danger btn-ladda"
                    loading={this.state.uncheckbutton}
                    color="primary" 
                    onClick={e => this.changeModal()}>
                    Ponto já avaliado
                  </LaddaButton>
               </ModalFooter>
            )
          }
        </Modal>
     
      </div>
    )
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

const MapConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(MapAdmin);



export default geolocated({
  positionOptions: {
    enableHighAccuracy: true,
  },
  watchPosition: true,
  userDecisionTimeout: 5000,
})(MapConnect);
