import React, { Component } from "react";

import { CardBody, Card, Button, Row, Col } from "reactstrap";

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";

import GoogleMapReact from 'google-map-react';
const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Map extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };
  
  constructor(props) {
    super(props);
    this.state = {
      show: true
    };
  }

  componentDidMount() {
  }

  onShow = () => {
  };

  onHide = () => {
  };

  render() {
     return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyBwBkgyM2rZVTNKnRKMAy8AD4p5kD0eCGY" }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          />
        </GoogleMapReact>
      </div>
      
     
     )
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);
