import React, { Component } from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
}
from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";
import "ladda/dist/ladda-themeless.min.css";

import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setInvestidor } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import "moment/locale/pt-br";

import InputMask from "react-input-mask";

import UserRowWithSelect from "./UserRowWithSelect";
import DeviceRowWithSelect from "./DeviceRowWithSelect";
import DeviceRow from "./DeviceRow";

import "spinkit/css/spinkit.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

import ThemedStyleSheet from "react-with-styles/lib/ThemedStyleSheet";
import DefaultTheme from "react-dates/lib/theme/DefaultTheme";

import AsyncSelect from "react-select/async";

const download = require("js-file-download");

moment.locale("pt-br");

class Reports extends Component {
  constructor(props) {
    super(props);
    this.selUsers = {};
    this.selDevices = [];

    this.state = {
      filter: "",
      showprogress: false,
      users: [],
      devices: [],
      submiting: false,
      value: ["UT", "OH"],
      windowWidth: window.innerWidth,
      orientation: "horizontal",
      openDirection: "down"
    };
  }

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  componentDidMount() {}

  searchUsers(filter) {
    this.setState({ showprogress: true });
    axios
      .post(
        "/api/getapicall", {
          url: "/users/search",
          data: "?query=" + filter,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data.error != null) {
          alert(JSON.stringify(res.data.error));
        }
        else {
          this.setState({
            users: res.data
          });
        }
        this.setState({
          showprogress: false,
          showusers: true
        });
      });
  }

  searchDevices(filter) {
    this.setState({ showprogress: true });
    this.selDevices = [];
    axios
      .post(
        "/api/getapicall", {
          url: "/devices/search",
          data: "?query=" + filter,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data != null) {
          this.setState({
            devices: res.data,
            showprogress: false,
            showdevices: true
          });
        }
      });
  }

  onChangeUser = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUsers(filter);
        }
        else {
          _this.setState({ users: [] });
        }
      }, 1000)
    });
  };

  onChangeDevice = e => {
    this.setState({ filter: e.target.value });
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchDevices(filter);
        }
        else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.selUsers[user.id] = user;
  };

  callbackDevice = (device, checked) => {
    if (checked) {
      this.selDevices.push(device);
    }
    else {
      this.selDevices = this.selDevices.filter(d => d.id != device.id);
    }
  };

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  handleChange = selectedOptions => {
    this.setState({ techFilter: selectedOptions });
  };

  doLinkPermission = (userid, deviceid) => {
    axios
      .post(
        "/api/apicall", {
          url: "/permissions",
          data: { userId: userid, deviceId: deviceid },
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        this.setState({
          submiting: false,
          devices: [],
          userOp: [],
          filter: ""
        });
      });
  };

  doSubmit = async() => {
    this.setState({
      submiting: true
    });

    var query = "";

    this.state.devices.forEach(device => {
      query = query + "deviceId=" + device.value + "&";
    });

    query = query + "type=alarm&";
    query = query + "from=" + moment(this.state.startDate).format() + "&";
    query = query + "to=" + moment(this.state.endDate).format() + "&";

    console.log(this.state.event);

    if (this.state.event.length >= 1 && this.state.event[0].value == 'routes') {
      axios
        .post(
          "/api/getreport", {
            url: "/reports/route?" + query,
            data: "",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic " + this.props.session.token,
              Accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            }
          }, {
            responseType: "arraybuffer",
            headers: {
              "Content-Type": "application/json",
              Accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            }
          }
        )
        .then(res => {
          download(res.data, "report.xlsx");
          this.setState({
            submiting: false
          });
        });
    }
    else {

      this.state.event.forEach(event => {
        var eventstr = event.value.split("-");
        query = query + "keys=" + eventstr[0] + "&";
        query = query + "values=" + eventstr[1] + "&";
      });

      axios
        .post(
          "/api/getreport", {
            url: "/reports/routedetails?" + query,
            data: "",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic " + this.props.session.token,
              Accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            }
          }, {
            responseType: "arraybuffer",
            headers: {
              "Content-Type": "application/json",
              Accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            }
          }
        )
        .then(res => {
          download(res.data, "report.xlsx");
          this.setState({
            submiting: false
          });
        });
    }
  };

  searchDevicesOptions = inputValue => {
    if (inputValue.length < 3) return null;

    return axios
      .post(
        "/api/getapicall", {
          url: "/devices/search",
          data: "?query=" + inputValue + "&max=20",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];
        res.data.forEach(device => {
          results.push({ value: device.id, label: device.name });
        });

        console.log(results);

        return results;
      });
  };

  searchType = inputValue =>
    new Promise(resolve => {
      resolve([
        { value: "alarm-powerCut", label: "Corte Energia" },
        { value: "ignition-true", label: "Ignição Ligada" },
        { value: "ignition-false", label: "Ignição Desligada" },
        { value: "motion-true", label: "Em movimento" },
        { value: "routes", label: "Rotas" }

      ]);
    });

  render() {
    let userLines = this.state.users.map(user => {
      return <UserRowWithSelect callback={this.callback} user={user} />;
    });




    return (
      <div className="animated fadeIn">
        <InputGroup className="col-12">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-user" />
            </InputGroupText>
          </InputGroupAddon>

          <AsyncSelect
            className="my-col-8"
            isMulti
            defaultOptions
            loadOptions={this.searchDevicesOptions}
            onChange={e => {
              this.setState({ devices: e });
            }}
          />
        </InputGroup>

        <InputGroup className="mt-2 col-12">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-directions" />
            </InputGroupText>
          </InputGroupAddon>

          <AsyncSelect
            className="my-col-8"
            isMulti
            defaultOptions
            loadOptions={this.searchType}
            onChange={e => {
              this.setState({ event: e });
            }}
          />
        </InputGroup>

        <InputGroup className="mb-3 mt-2 col-12">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-directions" />
            </InputGroupText>
          </InputGroupAddon>
          <DateRangePicker
            startDatePlaceholderText="Data Inicio"
            endDatePlaceholderText="Data Final"
            isOutsideRange={() => false}
            startDate={this.state.startDate}
            startDateId="startDate"
            endDate={this.state.endDate}
            endDateId="endDate"
            onDatesChange={({ startDate, endDate }) =>
              this.setState({ startDate, endDate })
            }
            focusedInput={this.state.focusedInput}
            onFocusChange={focusedInput => this.setState({ focusedInput })}
            orientation={this.state.orientation}
            openDirection={this.state.openDirection}
          />
        </InputGroup>

        <Row className="justify-content-sm-start">
          <Col sm="12" md={{ size: 6, offset: 3 }}>
            <LaddaButton
              className="col-6 btn btn-primary btn-ladda"
              loading={this.state.submiting}
              onClick={e => {
                this.toggleBtn("expLeft");
                this.doSubmit();
              }}
            >
              Confirmar
            </LaddaButton>
          </Col>
        </Row>


      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports);
