import React, { Component } from "react";

import {
  ButtonGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditSurver } from "../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import AsyncSelect from "react-select/async";
import CreatableSelect from "react-select/creatable";
import AsyncCreatableSelect from "react-select/async-creatable";

const empty = {
  name: ""
};

class ItemType extends Component {
  constructor(props) {
    super(props);

    this.currenttable = this.props.items;

    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: this.props.selected ? this.props.selected : [],
      showall: false,
      cSelected: this.props.selected ? this.props.selected : [],
      showtable: this.props.selected ? true : false,
      laudoexample: this.props.example
    };

    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
  }

  toggleBtn = name => {};

  componentDidMount() {
    this.setState({
      table: this.currenttable
    });

    this.props.select(this.props.selected);
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do ambiente")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do ambiente")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(values);
    console.log(this.state.cSelected);

    setSubmitting(false);

    return;
  };

  handleSubmit2 = (values, { props = this.props, setSubmitting }) => {
    console.log(values);

    if (this.state.isEdit) {
      axios
        .put("/surveyor/" + this.props.session.edit_surver.id, values, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          console.log(res.data);

          this.props.setEditSurver(null);
          this.setState({ init: empty, showinfo: true });

          this.setState({ init: empty });
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    } else {
      axios
        .post("/surveyor", values, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          console.log(res.data);
          this.setState({ init: empty, showinfo: true });
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    }
    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/cadastro/register");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  loadQual = inputValue => {
    if (inputValue.length < 3) return null;

    return axios
      .post(
        "/api/getapicall",
        {
          url: "/devices/search",
          data: "?query=" + inputValue + "&max=20",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        console.log(results);

        return results;
      })
      .catch(error => {
        var results = [{ value: "all", label: "Todos" }];

        console.log(results);

        return results;
      });
  };

  onRadioBtnClick(rSelected) {
    this.setState({ rSelected });
  }

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
    this.props.select(this.state.cSelected);
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Vistoriador"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  addItem = () => {
    if (this.state.itemname != null && this.state.itemname.length > 0) {
      this.currenttable.push({
        name: this.state.itemname,
        createdAt: new Date()
      });
      this.setState({ table: this.currenttable, itemname: "" });
    }
  };

  search = term => {
    let filtered = this.currenttable.filter(item =>
      item.name.toLowerCase().includes(term.toLowerCase())
    );
    this.setState({ table: filtered });
  };

  showTodos = () => {
    this.setState({ table: this.currenttable });
  };

  showSelected = () => {
    this.setState({ table: this.state.cSelected });
  };

  selectAll = () => {
    if (!this.state.showall) {
      this.setState({ cSelected: this.currenttable });
    } else {
      this.setState({ cSelected: [] });
    }
    this.setState({ showall: !this.state.showall });
  };

  render() {
    var _init = this.state.init;

    return (
      <Card>
        <FormGroup check style={{ margin: "10px" }}>
          <Label className="green-label">{this.props.name}</Label>
          <br />
          <Button
            data-for="check"
            data-tip="Habilitar"
            color={this.state.showtable ? "primary" : "danger"}
            className="btn-brand"
            onClick={e => this.setState({ showtable: !this.state.showtable })}
          >
            <i className="fa fa-check" />
          </Button>
          <Label style={{ marginLeft: "10px" }} className="green-label">
            {"   "}Habilitar
          </Label>
          <br />
        </FormGroup>

        {this.state.showtable ? (
          <FormGroup style={{ margin: "10px" }}>
            <Label className="green-label">
              {"Exemplo do laudo"}
            </Label>
            <Input
              disabled
              style={{ marginLeft: "8px" }}
              className="col-md-6"
              type="text"
              name="laudoexample"
              id="laudoexample"
              onChange={e => {
                this.setState({ laudoexample: e.target.value });
              }}
              value={this.state.laudoexample}
            />

            <Label>Seleção</Label>
            <br />
            <ButtonGroup>
              <Button
                color="primary"
                onClick={() => this.onRadioBtnClick(1)}
                active={this.state.rSelected === 1}
              >
                Unica
              </Button>
              <Button
                color="primary"
                onClick={() => this.onRadioBtnClick(2)}
                active={this.state.rSelected === 2}
              >
                Multipla
              </Button>
              <Button
                color="primary"
                onClick={() => this.onRadioBtnClick(3)}
                active={this.state.rSelected === 3}
              >
                Customizada (unico)
              </Button>
            </ButtonGroup>
            <br />
            <br />
            <Label>Opções</Label>
            <InputGroup>
              <Input
                style={{ marginLeft: "8px" }}
                className="col-md-6"
                type="text"
                name="itemname"
                id="itemname"
                onChange={e => {
                  this.setState({ itemname: e.target.value });
                  this.search(e.target.value);
                }}
                value={this.state.itemname}
              />

              <InputGroupAddon addonType="prepend">
                <Button
                  style={{ marginLeft: "8px" }}
                  type="button"
                  color="primary"
                  onClick={e => {
                    this.addItem();
                  }}
                >
                  <i className="fa fa-search" /> Adicionar
                </Button>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup style={{ marginTop: "20px" }}>
              <Button
                data-for="check"
                data-tip="Todos"
                color={this.state.showall ? "primary" : "blue"}
                className="btn-brand"
                onClick={e => this.selectAll()}
              >
                <i className="fa fa-check" />
              </Button>

              <Label
                style={{ marginTop: "10px", marginLeft: "10px" }}
                className="green-label"
              >
                {"   "}Selecionar Todos
              </Label>

              <InputGroupAddon addonType="prepend">
                <Button
                  style={{ marginLeft: "40%" }}
                  color="primary"
                  onClick={() => {
                    this.onRadioBtnClick(4);
                    this.showTodos();
                  }}
                  active={this.state.rSelected === 4}
                >
                  Todos
                </Button>

                <Button
                  style={{ marginLeft: "8px" }}
                  color="primary"
                  onClick={() => {
                    this.onRadioBtnClick(5);
                    this.showSelected();
                  }}
                  active={this.state.rSelected === 5}
                >
                  Selecionados
                </Button>
              </InputGroupAddon>
            </InputGroup>

            <BootstrapTable
              data={this.state.table}
              version="4"
              bordered={false}
              hover
              options={{
                noDataText: "Sem Informações"
              }}
              searchPlaceholder={"Busca"}
            >
              <TableHeaderColumn
                width="50"
                dataField="button"
                dataFormat={this.checkFormatter}
              />
              <TableHeaderColumn width="300" isKey dataField="name" dataSort>
                Nome
              </TableHeaderColumn>
            </BootstrapTable>
          </FormGroup>
        ) : (
          <div />
        )}
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ItemType)
);
