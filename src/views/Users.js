import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser } from "../redux/redux";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import history from "../history/history";

import InputMask from "react-input-mask";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

const empty = {
  firstName: "",
  name: "",
  email: "",
  login: "",
  sexo: "",
  password: "",
  password2: ""
};

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      init: empty,
      isEdit: false,
      showinfo: false,
      showerror: false
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    this.setState({ show: true });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      firstName: Yup.string().required("nome do usuario"),
      password: Yup.string().required("senha obrigatoria"),
      email: Yup.string()
        .email("email inválido")
        .required("email obrigatorio"),
      login: Yup.string().required("CPF obrigatorio"),
      password2: Yup.string()
        .oneOf([Yup.ref("password"), null], "senha nao bate")
        .required("confirmação da senha requerida")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({});
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(values);

    var cpf = values.login
      .replace(".", "")
      .replace(".", "")
      .replace(".", "")
      .replace(".", "")
      .replace("/", "")
      .replace("-", "");
    values.login = cpf;

    if (!this.checkCPF(cpf)) {
      this.setState({ showcpfinvalido: true }, () => {
        setSubmitting(false);
      });
    } else {
      axios
        .post(
          "https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/users",
          { type: "createuser", user: values },
          {
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Origin": "*"
            }
          }
        )
        .then(res => {
          console.log(res.data);

          if (res.data.statusCode == 200) {
            this.setState({ init: empty, showinfo: true });
          } else {
            this.setState({
              showerror: true,
              messageerror: res.data.body
            });
          }
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    }
    return;
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/login");
  };

  checkCPF = strCPF => {
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000") return false;

    for (var i = 1; i <= 9; i++)
      Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;
    for (var i = 1; i <= 10; i++)
      Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
  };

  render() {
    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <div
              id="login_body"
              className="login_app flex-row align-items-center"
            >
              <Container>
                <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                  <Card>
                    <CardHeader>Cadastro de Usuário</CardHeader>
                    <CardBody>
                      <FormGroup>
                        <Label className="green-label" for="firstName">
                          Nome
                        </Label>
                        <Input
                          className="col-md-8"
                          invalid={touched.firstName && !!errors.firstName}
                          type="text"
                          name="firstName"
                          id="firstName"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.firstName}
                        />
                        <FormFeedback>{errors.firstName}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="login">
                          CPF (acesso)
                        </Label>
                        <Input
                          className="col-md-4"
                          invalid={touched.login && !!errors.login}
                          name="login"
                          type="tel"
                          autoComplete="username"
                          value={values.login}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          mask="999.999.999-99"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.login}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="email">
                          Email
                        </Label>
                        <Input
                          className="col-md-8"
                          invalid={touched.email && !!errors.email}
                          type="text"
                          name="email"
                          id="email"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                        <FormFeedback>{errors.email}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="password">
                          Senha
                        </Label>
                        <Input
                          className="col-md-6"
                          invalid={touched.password && !!errors.password}
                          type="password"
                          name="password"
                          id="password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password}
                        />
                        <FormFeedback>{errors.password}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="password2">
                          Confirmar Senha 22
                        </Label>
                        <Input
                          className="col-md-6"
                          invalid={touched.password2 && !!errors.password2}
                          type="password"
                          name="password2"
                          id="password2"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password2}
                        />
                        <FormFeedback>{errors.password2}</FormFeedback>
                      </FormGroup>
                    </CardBody>
                  </Card>

                  <div className="row justify-content-center">
                    <LaddaButton
                      style={{ marginTop: "5px", width: "80%" }}
                      loading={isSubmitting}
                      className="btn btn-primary btn-ladda"
                      onClick={e => {
                        handleSubmit(e);
                        this.toggleBtn("expLeft");
                      }}
                    >
                      <i color="danger" className="fa fa-check" /> Registrar
                    </LaddaButton>
                  </div>
                </Form>
              </Container>
            </div>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showcpfinvalido}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Usuário</ModalHeader>
          <ModalBody>{"CPF inválido"}</ModalBody>
          <ModalFooter>
            <Button
              color="danger"
              onClick={e => this.setState({ showcpfinvalido: false })}
            >
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro salvando Usuário
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Usuário</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
