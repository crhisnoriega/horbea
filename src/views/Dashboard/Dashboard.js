import React, { Component } from "react";
import { Bar, Line } from "react-chartjs-2";
import {
  Media,
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardGroup,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from "reactstrap";
import Widget03 from "../../views/Widgets/Widget03";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import {
  getStyle,
  hexToRgba,
} from "@coreui/coreui-pro/dist/js/coreui-utilities";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";

import Websocket from "react-websocket";
import qs from "qs";

import moment from "moment";
import "moment/locale/pt-br";
import ReactTableContainer from "react-table-container";

import icon1 from "../../assets/img/ico_prancheta_1.png";
import icon2 from "../../assets/img/ico_prancheta_2.png";
import icon3 from "../../assets/img/ico_calendario_1.png";
import icon4 from "../../assets/img/ico_calendario_2.png";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import {
  doLogin,
  setEditUser,
  setEditAgency,
  setEditSurver,
} from "../../redux/redux";

import history from "../../history/history";

import { table_services } from "../table_services";

const brandPrimary = getStyle("--primary");
const brandSuccess = getStyle("--success");
const brandInfo = getStyle("--info");
const brandWarning = getStyle("--warning");
const brandDanger = getStyle("--danger");

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
        gridLines: {
          color: "rgba(0, 0, 0, 0)",
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 10,
        },
      },
    ],
  },
};

// Card Chart 1
const cardChartData1 = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: brandPrimary,
      borderColor: "rgba(255,255,255,.55)",
      data: [65, 59, 84, 84, 51, 55, 40],
    },
  ],
};

const cardChartOpts1 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: "transparent",
          zeroLineColor: "transparent",
        },
        ticks: {
          fontSize: 2,
          fontColor: "transparent",
        },
      },
    ],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData1.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData1.datasets[0].data) + 5,
        },
      },
    ],
  },
  elements: {
    line: {
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

// Card Chart 2
const cardChartData2 = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: brandInfo,
      borderColor: "rgba(255,255,255,.55)",
      data: [1, 18, 9, 17, 34, 22, 11],
    },
  ],
};

const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: "transparent",
          zeroLineColor: "transparent",
        },
        ticks: {
          fontSize: 2,
          fontColor: "transparent",
        },
      },
    ],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData2.datasets[0].data) + 5,
        },
      },
    ],
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

// Card Chart 3
const cardChartData3 = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: "rgba(255,255,255,.2)",
      borderColor: "rgba(255,255,255,.55)",
      data: [78, 81, 80, 45, 34, 12, 40],
    },
  ],
};

const cardChartOpts3 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      },
    ],
    yAxes: [
      {
        display: false,
      },
    ],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

// Card Chart 4
const cardChartData4 = {
  labels: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
    "January",
    "February",
    "March",
    "April",
  ],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: "rgba(255,255,255,.3)",
      borderColor: "transparent",
      data: [78, 81, 80, 45, 34, 12, 40, 75, 34, 89, 32, 68, 54, 72, 18, 98],
    },
  ],
};

const cardChartOpts4 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
        barPercentage: 0.6,
      },
    ],
    yAxes: [
      {
        display: false,
      },
    ],
  },
};

var bar = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: [65, 59, 80, 81, 56, 55, 40],
    },
  ],
};

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      stats: {},
      dropdownOpen: false,
      radioSelected: 1,
      table: [],
      powerCutEvents: [],
      deviceOnline: [],
      deviceOffline: [],
      showDays: false,
      showHours: false,
      days: 0,
      hours: 0,
      deviceAlreadyOn: [],
      bar: {},
      survers: [],
      agencies: [],
    };
  }

  componentDidMount() {
    var session = JSON.parse(localStorage.getItem("redux"));
    try {
      this.vTaxes = session.session.login[0].vTaxes;
      this.admin_id = session.session.login[0].iAdminId;
    } catch (e) {}
    
    this.setState({ show: true });

    axios
      .post(
        "/agenda/ajax_stats.php?admin_id=" + this.admin_id,
        { admin_id: this.admin_id },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        var total =
          parseInt(res.data.finished) +
          parseInt(res.data.cancel) +
          parseInt(res.data.active);

        this.setState({ stats: res.data, total });
      });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  handleData(data) {
    let result = JSON.parse(data);
    this.setState({ count: this.state.count + result.movement });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  getValue = (table, stat) => {
    if (table == null) {
      return "0";
    }

    if (table.offline != null && !stat) {
      return table.offline;
    }

    if (table.online != null && stat) {
      return table.online;
    }
    return "0";
  };

  markAsRead = (device) => {
    this.setState((prevState) => ({
      powerCutEvents: prevState.powerCutEvents.filter(
        (el) => el.id != device.id
      ),
    }));
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("DD/MM/YYYY HH:mm:SS");
  };

  tipoServicoFormat = (cell, row) => {
    return table_services[cell];
  };

  vistoriadorFormat = (cell, row) => {
    console.log(row);
    return row.vistoriador ? row.vistoriador.name : "";
  };

  logoFormatter = (cell, row) => {
    if (cell) {
      return (
        <div className="sem-imagem-tiny" style={{ backgroundColor: cell }} />
      );
    } else {
      return "-";
    }
  };

  rowAgencyClick = (agency) => {
    this.props.setEditAgency(agency);
    history.push("/app/agency/register");
  };

  rowSurverClick = (surver) => {
    this.props.setEditSurver(surver);
    history.push("/app/surver/create");
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>Dashboard</CardHeader>
          <CardBody style={{ backgroundColor: "white" }}>
            <CardGroup
              style={{
                background: "rgba(233, 84, 39, 0.02)",
                borderRadius: "5px",
                border: "1px solid #E95327",
              }}
            >
              <Row>
                <Col xs="12" sm="6" lg="3">
                  <Card className="m-4">
                    <CardBody style={{ height: "100px" }}>
                      <Row>
                        <Col xs="4">
                          <img src={icon1} />
                        </Col>
                        <Col xs="8">
                          <div style={{ fontSize: "12px" }}>
                            Corridas Finalizadas{" "}
                            {
                              "                                                                         "
                            }
                          </div>
                          <div style={{ fontSize: "12px" }}>
                            {
                              "                                                                         "
                            }
                          </div>
                        </Col>
                      </Row>
                      <Row style={{ marginTop: "-35px", marginLeft: "10px" }}>
                        <div
                          style={{
                            fontSize: "30px",
                            margin: "0 auto",
                          }}
                        >
                          {this.state.stats.finished}
                        </div>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>

                <Col xs="12" sm="6" lg="3">
                  <Card className="m-4">
                    <CardBody style={{ height: "100px" }}>
                      <Row>
                        <Col xs="4">
                          <img src={icon2} />
                        </Col>
                        <Col xs="8">
                          <div style={{ fontSize: "12px" }}>
                            Corridas Activas
                          </div>
                        </Col>
                      </Row>

                      <Row style={{ marginTop: "-35px" }}>
                        <div style={{ fontSize: "30px", margin: "0 auto" }}>
                          {this.state.stats.active}
                        </div>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col xs="12" sm="6" lg="3">
                  <Card className="m-4">
                    <CardBody style={{ height: "120px" }}>
                      <Row>
                        <Col xs="4">
                          <img style={{ marginLeft: "-10px" }} src={icon3} />
                        </Col>
                        <Col style={{}} xs="8">
                          <div style={{ fontSize: "12px" }}>
                            Corridas Canceladas
                          </div>
                          <div style={{ fontSize: "12px" }}>
                            {"Mes "}
                            {moment(new Date()).format("MMMM")}
                          </div>
                        </Col>
                      </Row>
                      <Row style={{ marginTop: "-10px" }}>
                        <div style={{ fontSize: "30px", margin: "0 auto" }}>
                          {this.state.stats.cancel}
                        </div>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>

                <Col
                  xs="12"
                  sm="6"
                  lg="3"
                  style={{
                    paddingLeft: "5px !important",
                    paddinRight: "5px !important",
                  }}
                >
                  <Card className="m-4">
                    <CardBody style={{ height: "120px" }}>
                      <Row>
                        <Col xs="4">
                          <img style={{ marginLeft: "-10px" }} src={icon4} />
                        </Col>
                        <Col xs="8">
                          <div style={{ fontSize: "12px" }}>
                            Total de Corridas
                          </div>
                          <div style={{ fontSize: "12px" }}>
                            {"Mes "}
                            {moment(new Date()).format("MMMM")}
                          </div>
                        </Col>
                      </Row>
                      <Row style={{ marginTop: "-10px" }}>
                        <div style={{ fontSize: "30px", margin: "0 auto" }}>
                          {this.state.total}
                        </div>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>

              <Row>
                <Col style={{ height: "300px" }} xs="10">
                  <Card>
                    <CardHeader>Ultimos Agendamentos</CardHeader>
                    <CardBody>
                      <ButtonGroup>
                        <Button
                          color="primary"
                          onClick={(e) => {
                            history.push("/app/booking");
                          }}
                        >
                          <i color="primary" className="fa fa-edit" /> Novo
                          Agendamento
                        </Button>
                      </ButtonGroup>
                      <BootstrapTable
                        data={this.state.stats.last_bookings}
                        version="4"
                        bordered={false}
                        hover
                        options={{
                          noDataText: "Sem Informações",
                          onRowClick: this.rowSurverClick,
                        }}
                      >
                        <TableHeaderColumn
                          width="180"
                          dataField="dBooking_date"
                          isKey
                          dataSort
                        >
                          Data Agendamento
                        </TableHeaderColumn>
                        <TableHeaderColumn
                          width="200"
                          dataField="rider"
                          dataSort
                        >
                          Passageiro
                        </TableHeaderColumn>

                        <TableHeaderColumn
                          width="300"
                          dataField="vSourceAddresss"
                          dataSort
                        >
                          Destino
                        </TableHeaderColumn>
                      </BootstrapTable>
                    </CardBody>
                  </Card>
                </Col>
                <Col style={{ height: "300px" }} xs="6">
                  <Card></Card>
                </Col>
              </Row>
            </CardGroup>
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  session: state.session,
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency,
  setEditSurver,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Dashboard)
);
