import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
}
from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditSurver } from "../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";
import request from "sync-request";

const empty = {
  name: "",
  cpf: "",
  orgao: "",
  rg: "",
  cep: "",
  street: "",
  number: "",
  complemento: "",
  bairro: "",
  city: "",
  state: "",
  fone1: "",
  email: "",
  password: ""
};

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    if (this.props.session.edit_surver != null) {
      console.log(this.props.session.edit_surver);

      this.props.session.edit_surver.password = "";
      this.setState({
        init: this.props.session.edit_surver,
        isEdit: true,
        image: this.props.session.edit_surver.photo,
        expediente: {
          value: this.props.session.edit_surver.expe,
          label: this.props.session.edit_surver.expe
        },
        sexo: {
          value: this.props.session.edit_surver.sexo,
          label: this.props.session.edit_surver.sexo
        }
      });
    }
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do vistoriador"),
      cpf: Yup.string().required("CPF do vistoriador"),
      rg: Yup.string().required("RG do vistoriador"),

      cep: Yup.string().required("CEP obrigatorio"),
      street: Yup.string().required("logradouro obrigatorio"),
      number: Yup.string().required("numero obrigatorio"),
      bairro: Yup.string().required("bairro obrigatorio"),
      city: Yup.string().required("cidade obrigatorio"),
      state: Yup.string().required("estado obrigatorio"),
      fone1: Yup.string().required("telefone"),
      email: Yup.string()
        .email("email invalido")
        .required("email"),
      password: Yup.string().required("password"),
      password2: Yup.string()
        .oneOf([Yup.ref("password"), null], "Senha nao bate")
        .required("Password confirm is required")
    });
  };

  checkCPF = (strCPF) => {
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000") return false;

    for (var i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;
    for (var i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
  }

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do vistoriador"),
      cpf: Yup.string().required("CPF do vistoriador"),
      rg: Yup.string().required("RG do vistoriador"),

      cep: Yup.string().required("CEP obrigatorio"),
      street: Yup.string().required("logradouro obrigatorio"),
      number: Yup.string().required("numero obrigatorio"),
      bairro: Yup.string().required("bairro obrigatorio"),
      city: Yup.string().required("cidade obrigatorio"),
      state: Yup.string().required("estado obrigatorio"),
      fone1: Yup.string().required("telefone"),
      email: Yup.string()
        .email()
        .required("email")
    });
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      console.log(fileInfo);
      console.log(fileInfo.base64.length);
      this.setState({ image: fileInfo.base64 });
    }; // reader.onload
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        if (this.state.image == null) {
          return { photo: "imagem obrigatoria" };
        }

        return {};
      }
      catch (error) {
        var err = this.extractError(error);

        if (this.state.image == null) {
          return { ...err, photo: "imagem obrigatoria" };
        }

        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    values.photo = this.state.image;
    values.expe = this.state.expediente.value;
    values.sexo = this.state.sexo.value;

    console.log(values);

    var cpf = values.cpf.replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace("/", "").replace("-", "");

    if (!this.checkCPF(cpf)) {
      this.setState({ showcpfinvalido: true }, () => { setSubmitting(false); })
    }
    else {

      if (this.state.isEdit) {
        axios
          .put("/surveyor/" + this.props.session.edit_surver.id, values, {
            headers: {
              "Content-Type": "application/json"
            }
          })
          .then(res => {
            console.log(res.data);

            this.props.setEditSurver(null);
            this.setState({ init: empty, showinfo: true });

            this.setState({ init: empty });
            setSubmitting(false);
          })
          .catch(error => {
            console.log(error);
            this.setState({
              showerror: true,
              messageerror: "Usuario já existe"
            });
            setSubmitting(false);
          });
      }
      else {
        axios
          .post("/surveyor", values, {
            headers: {
              "Content-Type": "application/json"
            }
          })
          .then(res => {
            console.log(res.data);
            this.setState({ init: empty, showinfo: true });
            setSubmitting(false);
          })
          .catch(error => {
            console.log(error);
            this.setState({
              showerror: true,
              messageerror: "Usuario já existe"
            });
            setSubmitting(false);
          });
      }
    }
    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/surveyors/list");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  render() {
    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />
        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Dados</CardHeader>
                  <CardBody>
                    <FormGroup>
                      <Label className="green-label" for="name">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="cpf">
                          CPF (acesso)
                        </Label>
                        <Input
                          invalid={touched.cpf && !!errors.cpf}
                          type="text"
                          name="cpf"
                          id="cpf"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.cpf}
                          mask="999.999.999-99"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.cpf}</FormFeedback>
                      </FormGroup>
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="rg">
                          RG
                        </Label>
                        <Input
                          invalid={touched.rg && !!errors.rg}
                          type="text"
                          name="rg"
                          id="rg"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.rg}
                          mask="99.999.999-9"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.rg}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <FormGroup>
                      <Label className="green-label" for="orgao">
                        Orgao Emissor
                      </Label>
                      <Input
                        className="col-md-4"
                        invalid={touched.orgao && !!errors.orgao}
                        type="text"
                        name="orgao"
                        id="orgao"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.orgao}
                      />
                      <FormFeedback>{errors.orgao}</FormFeedback>
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="sexo">
                          Sexo
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.sexo}
                          options={[
                            { value: "Homem", label: "Homem" },
                            { value: "Mulher", label: "Mulher" }
                          ]}
                          onChange={sexo => {
                            errors.sexo = "";
                            if (sexo == null) {
                              sexo = "";
                            }
                            this.setState({ sexo });
                          }}
                          placeholder="Selecione"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.sexo}
                        </Label>
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="naturalidade">
                          Naturalidade
                        </Label>
                        <Input
                          className="col-md-10"
                          invalid={
                            touched.naturalidade && !!errors.naturalidade
                          }
                          type="text"
                          name="naturalidade"
                          id="naturalidade"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.naturalidade}
                        />
                        <FormFeedback>{errors.naturalidade}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="nacionalidade">
                          Nacionalidade
                        </Label>
                        <Input
                          invalid={
                            touched.nacionalidade && !!errors.nacionalidade
                          }
                          type="text"
                          name="nacionalidade"
                          id="nacionalidade"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.nacionalidade}
                        />
                        <FormFeedback>{errors.nacionalidade}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="estadocivil">
                          Estado Civil
                        </Label>
                        <Input
                          invalid={touched.estadocivil && !!errors.estadocivil}
                          type="text"
                          name="estadocivil"
                          id="estadocivil"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.estadocivil}
                        />
                        <FormFeedback>{errors.estadocivil}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <FormGroup>
                      <Label className="green-label" for="description">
                        Observação
                      </Label>
                      <Input
                        className="col-md-10"
                        invalid={touched.description && !!errors.description}
                        type="textarea"
                        name="description"
                        id="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="photo">
                        Imagem
                      </Label>
                      {this.state.image ? (
                        <img className="sem-imagem" src={this.state.image} />
                      ) : (
                        <div className="sem-imagem col-md-4">sem imagem</div>
                      )}
                      <input
                        className="col-md-6"
                        style={{ marginTop: "10px" }}
                        type="file"
                        onChange={e => this.handleFileSelect(e)}
                      />
                      <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                        {errors.photo}
                      </Label>
                    </FormGroup>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Funcionario</CardHeader>
                  <CardBody>
                    <FormGroup className="col-md-8">
                      <Label className="green-label" for="expediente">
                        Expediente
                      </Label>
                      <Select
                        name="form-field-name2"
                        value={this.state.expediente}
                        options={[
                          {
                            value: "Selecione",
                            label: "Selecione"
                          },
                          { value: "padrao", label: "Padrão" }
                        ]}
                        onChange={expediente => {
                          errors.expediente = "";
                          if (expediente == null) {
                            expediente = "";
                          }
                          this.setState({ expediente });
                        }}
                        placeholder="Selecione"
                        theme={theme => ({
                          ...theme,
                          borderRadius: 0,
                          colors: {
                            ...theme.colors,
                            primary25: "white",
                            primary: "#97B834"
                          }
                        })}
                      />

                      <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                        {errors.expediente}
                      </Label>
                    </FormGroup>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Endereço</CardHeader>
                  <CardBody>
                    <Row form className="col-md-15">
                      <FormGroup className="col-3">
                        <Label className="green-label" for="cep">
                          CEP
                        </Label>
                        <Input
                          invalid={touched.cep && !!errors.cep}
                          type="text"
                          name="cep"
                          id="cep"
                          onChange={e => {
                            handleChange(e);

                            var cep = e.target.value;
                            cep = cep.replace("-", "");
                            cep = cep.replace(" ", "");

                            if (cep.length == 8) {
                              var res = request(
                                "GET",
                                "https://viacep.com.br/ws/" + cep + "/json/"
                              );

                              console.log(res);
                              var add = JSON.parse(res.body);
                              values.street = add.logradouro;
                              values.bairro = add.bairro;
                              values.city = add.localidade;
                            }
                          }}
                          onBlur={handleBlur}
                          value={values.cep}
                          mask="99999-999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.cep}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-6">
                        <Label className="green-label" for="street">
                          Logradouro
                        </Label>
                        <Input
                          invalid={touched.street && !!errors.street}
                          type="text"
                          name="street"
                          id="street"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.street}
                        />
                        <FormFeedback>{errors.street}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-2">
                        <Label className="green-label" for="number">
                          Numero
                        </Label>
                        <Input
                          invalid={touched.number && !!errors.number}
                          type="text"
                          name="number"
                          id="number"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.number}
                        />
                        <FormFeedback>{errors.number}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-3">
                        <Label className="green-label" for="complemento">
                          Complemento
                        </Label>
                        <Input
                          invalid={touched.complemento && !!errors.complemento}
                          type="text"
                          name="complemento"
                          id="complemento"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.complemento}
                        />
                        <FormFeedback>{errors.complemento}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-5">
                        <Label className="green-label" for="bairro">
                          Bairro
                        </Label>
                        <Input
                          invalid={touched.bairro && !!errors.bairro}
                          type="text"
                          name="bairro"
                          id="bairro"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.bairro}
                        />
                        <FormFeedback>{errors.bairro}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-5">
                        <Label className="green-label" for="city">
                          Cidade
                        </Label>
                        <Input
                          invalid={touched.city && !!errors.city}
                          type="text"
                          name="city"
                          id="city"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.city}
                        />
                        <FormFeedback>{errors.city}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-3">
                        <Label className="green-label" for="state">
                          Estado
                        </Label>
                        <Input
                          invalid={touched.state && !!errors.state}
                          type="text"
                          name="state"
                          id="state"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.state}
                        />
                        <FormFeedback>{errors.state}</FormFeedback>
                      </FormGroup>
                    </Row>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Contato</CardHeader>
                  <CardBody>
                    <Row form className="col-md-15">
                      <FormGroup className="col-5">
                        <Label className="green-label" for="fone1">
                          Telefone 1
                        </Label>
                        <Input
                          invalid={touched.fone1 && !!errors.fone1}
                          type="text"
                          name="fone1"
                          id="fone1"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.fone1}
                          mask="(99) 99999-9999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.fone1}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-5">
                        <Label className="green-label" for="fone2">
                          Telefone 2
                        </Label>
                        <Input
                          invalid={touched.fone2 && !!errors.fone2}
                          type="text"
                          name="fone2"
                          id="fone2"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.fone2}
                          mask="(99) 99999-9999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.fone2}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-8">
                        <Label className="green-label" for="email">
                          Email
                        </Label>
                        <Input
                          invalid={touched.email && !!errors.email}
                          type="text"
                          name="email"
                          id="email"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                        <FormFeedback>{errors.email}</FormFeedback>
                      </FormGroup>
                    </Row>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Acesso ao Aplicativo</CardHeader>
                  <CardBody>
                    <Row form className="col-md-15">
                      <FormGroup className="col-5">
                        <Label className="green-label" for="password">
                          Senha
                        </Label>
                        <Input
                          invalid={touched.password && !!errors.password}
                          type="password"
                          name="password"
                          id="password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password}
                        />
                        <FormFeedback>{errors.password}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-5">
                        <Label className="green-label" for="password2">
                          Confirme a senha
                        </Label>
                        <Input
                          invalid={touched.password2 && !!errors.password2}
                          type="password"
                          name="password2"
                          id="password2"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password2}
                        />
                        <FormFeedback>{errors.password2}</FormFeedback>
                      </FormGroup>
                    </Row>
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <LaddaButton
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </LaddaButton>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />
        
        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showcpfinvalido}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
        <ModalHeader toggle={this.toggleInfo}>
           Salvar Vistoriador
          </ModalHeader>
          <ModalBody>{"CPF inválido"}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.setState({showcpfinvalido:false})}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>


        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro salvando Vistoriador
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
