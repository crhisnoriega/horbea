import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser, setEditSurver } from "../redux/redux";
import history from "../history/history";

import { registerLocale, setDefaultLocale } from "react-datepicker";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import pt from "date-fns/locale/pt";

import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import data from "./_data_agency";

// select
import Select from "react-select";

import ReactTooltip from "react-tooltip";

registerLocale("br", pt);

const empty = {
  imobiliarias: "",
  tipo_servico: "",
  vistoriador: "",
  data_inicio: "",
  data_termino: "",
  situacao: ""
};

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: [],
      filter: "",
      show: false,
      users: [],
      startDate: new Date(),
      endDate: new Date(),
      imobiliarias: { value: "all", label: "Todos" },
      tipo_servico: { value: "all", label: "Todos" },
      vistoriador: { value: "all", label: "Todos" },
      situacao: { value: "all", label: "Todas" },
      issub: false,
      listimobiliarias: [],
      listvistoriadores: []
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
  }

  componentDidMount() {
    axios
      .get("/surveyor/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        if (res.data) {
          var list = [];
          res.data.forEach(element => {
            list.push({ value: element.id, label: element.name });
          });
          this.setState({ listvistoriadores: list });
        }
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("/agency/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        if (res.data) {
          var list = [];
          res.data.forEach(element => {
            list.push({ value: element.id, label: element.name });
          });
          this.setState({ listimobiliarias: list });
        }
      })
      .catch(error => {
        console.log(error);
      });
    axios
      .get("/surver/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        } else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.props.setEditUser(user);
  };

  onSurverSelect = surver => {
    console.log(surver);
    this.props.setEditSurver(surver);
    history.push("/app/surver/create");
  };

  doSearch = () => {
    this.setState({ issub: true });
    axios
      .get("/surver/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);

        var filterlist = res.data.filter(item => {
          var d1 = moment(this.state.startDate);
          var d2 = moment(this.state.endDate);
          var current = moment(item.createdAt);

          console.log(current.isAfter(d1));

          return (
            current.isAfter(d1) &&
            d2.isAfter(current) &&
            (this.state.situacao.value == "1" ||
              this.state.situacao.value == "all")
          );
        });
        this.setState({ table: filterlist, issub: false });
      })
      .catch(error => {
        this.setState({ table: [], issub: false });
      });
  };

  onDeleteSurver = surver => {
    axios
      .delete("/surver/" + surver.id, {
        headers: { "Content-type": "json/application" }
      })
      .then(res => {
        var list = this.state.table.filter(item => item.id != surver.id);
        this.setState({ table: list, showconfirm: false });
      })
      .catch(error => {
        alert(error);
      });
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="edit"
          data-tip="Visualizar Vistoria"
          color="primary"
          className="btn-brand"
          onClick={e => {
            e.stopPropagation();
            this.rowHandleSurverShow(row);
          }}
        >
          <i color="primary" className="fa fa-glasses" />
        </Button>

        <Button
          style={{ marginLeft: "5px" }}
          data-for="edit"
          data-tip="Logs"
          className="btn-brand"
          onClick={e => {
            e.stopPropagation();
            history.push("/app/logs/surver");
          }}
        >
          <i className="fa fa-file" />
        </Button>

        <Button
          data-for="edit"
          data-tip="Remover"
          style={{ marginLeft: "5px" }}
          color="danger"
          className="btn-brand"
          onClick={e => {
            e.stopPropagation();
            this.setState({ currentsurver: row, showconfirm: true });
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="edit" multiline={true} />
        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  agencyFormater = (cell, row) => {
    console.log(row);
    return (
      <Row>
        <Col xs="5">
          {row.photo ? (
            <img
              style={{ width: "40px" }}
              src={row.photo}
              className="img-avatar"
            />
          ) : (
            <div
              style={{ margin: "0 auto" }}
              className="img-avatar sem-avatar-small col-md-4"
            >
              sem imagem
            </div>
          )}
        </Col>
        <Col xs="5">
          <div style={{ marginLeft: "-20px" }}>
            {row.agency && row.agency.label
              ? row.agency.label.split(" ")[0]
              : ""}
          </div>
          <Row style={{ marginLeft: "-20px" }}>
            <div className="text-muted">
              {row.vistoriador && row.vistoriador.name? row.vistoriador.name.split(" ")[0] : ""}
            </div>
            <div style={{ fontStyle: "italic" }}>{"(vistoriador)"}</div>
          </Row>
        </Col>
      </Row>
    );
  };

  servicoFormater = (cell, row) => {
    console.log(row);
    return (
      <Row>
        <Col xs="5">
          <div style={{ fontSize: "18px" }}>{"Contrato"}</div>
          <div style={{ fontSize: "18px" }}>{"-"}</div>
          <div>
            {row.tipo_servico == "vistoria_entrada"
              ? "Vistoria de Entrada"
              : ""}
          </div>
          <div className="text-muted" style={{ fontSize: "14px" }}>
            {row.status == "vistoriador"
              ? "Em posse do vistoriador"
              : "Aguardando atribuir ao vistoriador"}
          </div>
          <div style={{ marginTop: "10px" }} className="text-muted">
            {"ultima ação"}
          </div>
          <div className="text-muted">
            {moment(row.createdAt).format("DD/MM/YYYY HH:mm:SS")}
          </div>
        </Col>
      </Row>
    );
  };

  imovelFormater = (cell, row) => {
    if(cell){
    return (
      
      <div>
        <div>{cell.street}</div>
        <div>
          {cell.bairro} {cell.cep}
        </div>
        <div>{cell.city}</div>
      </div>
    );
    }else{
      return(<div/>)
    }
  };

  dateFormatter = (cell, row) => {
    return (
      <div style={{ margin: "0 auto", marginLeft: "20px" }}>
        <div>{moment(cell).format("DD/MM/YYYY")}</div>
        <div
          style={{ margin: "0 auto", marginLeft: "20px" }}
          className="text-muted"
        >
          {moment(cell).format("HH:mm")}
        </div>
      </div>
    );
  };

  colorFormatter = (cell, row) => {
    return (
      <div
        className="sem-imagem img-avatar"
        style={{ backgroundColor: cell }}
      />
    );
  };

  rowHandleSurver = surver => {
    this.props.setEditSurver(surver);
    history.push("/app/surver/create");
  };

  rowHandleSurverShow = surver => {
    this.props.setEditSurver(surver);
    history.push("/app/surver/show");
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Formik
          //  initialValues={initialValues}
          //  validate={validate(validationSchema)}
          //  onSubmit={onSubmit}
          render={({
            values,
            errors,
            touched,
            status,
            dirty,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            handleReset,
            setTouched
          }) => (
            <Row>
              <Col>
                <Card>
                  <CardHeader>Vistorias</CardHeader>
                  <CardBody>
                    <ButtonGroup>
                      <Button
                        style={{ width: "200px" }}
                        color="primary"
                        onClick={e => {
                          history.push("/app/surver/create");
                        }}
                      >
                        <i color="primary" className="fa fa-plus" />
                        Criar Vistoria
                      </Button>
                    </ButtonGroup>
                    <ButtonGroup>
                      <Button
                        style={{ width: "200px", marginLeft: "10px" }}
                        color="primary"
                        onClick={e => {
                          history.push("/app/calendar");
                        }}
                      >
                        <i color="primary" className="fa fa-edit" />
                        Agenda
                      </Button>
                    </ButtonGroup>
                     <Row
                      form
                      classNadme="col-md-15"
                      style={{ marginTop: "30px" }}
                    >
                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="imobiliarias">
                          Imobiliárias
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.imobiliarias}
                          options={this.state.listimobiliarias}
                          onChange={imobiliarias => {
                            errors.imobiliarias = "";
                            if (imobiliarias == null) {
                              imobiliarias = "";
                            }
                            this.setState({ imobiliarias });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="tipo_servico">
                          Tipo de Serviço
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.tipo_servico}
                          options={[
                            { value: "all", label: "Todos" },
                            {
                              value: "vistoria_entrada",
                              label: "Vistoria de Entrada"
                            }
                          ]}
                          onChange={tipo_servico => {
                            errors.tipo_servico = "";
                            if (tipo_servico == null) {
                              tipo_servico = "";
                            }
                            this.setState({ tipo_servico });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="vistoriador">
                          Vistoriador
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.vistoriador}
                          options={this.state.listvistoriadores}
                          onChange={vistoriador => {
                            errors.vistoriador = "";
                            if (vistoriador == null) {
                              vistoriador = "";
                            }
                            this.setState({ vistoriador });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="situacao">
                          Situação
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.situacao}
                          options={[
                            { value: "all", label: "Todas" },
                            {
                              value: "1",
                              label: "Aguardando atribuir ao vistoriador"
                            },
                            { value: "2", label: "Atribuido ao vistoriador" },
                            { value: "3", label: "Em posse do vistoriador" },
                            { value: "4", label: "Transmitido" }
                          ]}
                          onChange={situacao => {
                            errors.situacao = "";
                            if (situacao == null) {
                              situacao = "";
                            }
                            this.setState({ situacao });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Col md="4">
                          <Label htmlFor="data_inicio">Inicio</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.startDate}
                            onChange={e => {
                              this.setState({ startDate: e });
                            }}
                            locale="br"
                          />
                        </Col>

                        <Col md="4">
                          <Label htmlFor="data_termino">Término</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.endDate}
                            onChange={e => {
                              this.setState({ endDate: e });
                            }}
                            locale="br"
                          />

                          <LaddaButton
                            type="submit"
                            className="col-8 btn btn-primary btn-ladda"
                            loading={this.state.issub}
                            onClick={e => {
                              this.doSearch();
                            }}
                          >
                            <i color="primary" className="fa fa-search" />{" "}
                            Procurar
                          </LaddaButton>
                        </Col>
                      </FormGroup>
                    </Row>
                     <Row
                      form
                      classNadme="col-md-15"
                      style={{ marginTop: "30px" }}
                    >
                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="imobiliarias">
                          Imobiliárias
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.imobiliarias}
                          options={this.state.listimobiliarias}
                          onChange={imobiliarias => {
                            errors.imobiliarias = "";
                            if (imobiliarias == null) {
                              imobiliarias = "";
                            }
                            this.setState({ imobiliarias });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="tipo_servico">
                          Tipo de Serviço
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.tipo_servico}
                          options={[
                            { value: "all", label: "Todos" },
                            {
                              value: "vistoria_entrada",
                              label: "Vistoria de Entrada"
                            }
                          ]}
                          onChange={tipo_servico => {
                            errors.tipo_servico = "";
                            if (tipo_servico == null) {
                              tipo_servico = "";
                            }
                            this.setState({ tipo_servico });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="vistoriador">
                          Vistoriador
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.vistoriador}
                          options={this.state.listvistoriadores}
                          onChange={vistoriador => {
                            errors.vistoriador = "";
                            if (vistoriador == null) {
                              vistoriador = "";
                            }
                            this.setState({ vistoriador });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="situacao">
                          Situação
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.situacao}
                          options={[
                            { value: "all", label: "Todas" },
                            {
                              value: "1",
                              label: "Aguardando atribuir ao vistoriador"
                            },
                            { value: "2", label: "Atribuido ao vistoriador" },
                            { value: "3", label: "Em posse do vistoriador" },
                            { value: "4", label: "Transmitido" }
                          ]}
                          onChange={situacao => {
                            errors.situacao = "";
                            if (situacao == null) {
                              situacao = "";
                            }
                            this.setState({ situacao });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Col md="4">
                          <Label htmlFor="data_inicio">Inicio</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.startDate}
                            onChange={e => {
                              this.setState({ startDate: e });
                            }}
                            locale="br"
                          />
                        </Col>

                        <Col md="4">
                          <Label htmlFor="data_termino">Término</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.endDate}
                            onChange={e => {
                              this.setState({ endDate: e });
                            }}
                            locale="br"
                          />

                          <LaddaButton
                            type="submit"
                            className="col-8 btn btn-primary btn-ladda"
                            loading={this.state.issub}
                            onClick={e => {
                              this.doSearch();
                            }}
                          >
                            <i color="primary" className="fa fa-search" />{" "}
                            Procurar
                          </LaddaButton>
                        </Col>
                      </FormGroup>
                    </Row>
                     <Row
                      form
                      classNadme="col-md-15"
                      style={{ marginTop: "30px" }}
                    >
                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="imobiliarias">
                          Imobiliárias
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.imobiliarias}
                          options={this.state.listimobiliarias}
                          onChange={imobiliarias => {
                            errors.imobiliarias = "";
                            if (imobiliarias == null) {
                              imobiliarias = "";
                            }
                            this.setState({ imobiliarias });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="tipo_servico">
                          Tipo de Serviço
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.tipo_servico}
                          options={[
                            { value: "all", label: "Todos" },
                            {
                              value: "vistoria_entrada",
                              label: "Vistoria de Entrada"
                            }
                          ]}
                          onChange={tipo_servico => {
                            errors.tipo_servico = "";
                            if (tipo_servico == null) {
                              tipo_servico = "";
                            }
                            this.setState({ tipo_servico });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="vistoriador">
                          Vistoriador
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.vistoriador}
                          options={this.state.listvistoriadores}
                          onChange={vistoriador => {
                            errors.vistoriador = "";
                            if (vistoriador == null) {
                              vistoriador = "";
                            }
                            this.setState({ vistoriador });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="situacao">
                          Situação
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.situacao}
                          options={[
                            { value: "all", label: "Todas" },
                            {
                              value: "1",
                              label: "Aguardando atribuir ao vistoriador"
                            },
                            { value: "2", label: "Atribuido ao vistoriador" },
                            { value: "3", label: "Em posse do vistoriador" },
                            { value: "4", label: "Transmitido" }
                          ]}
                          onChange={situacao => {
                            errors.situacao = "";
                            if (situacao == null) {
                              situacao = "";
                            }
                            this.setState({ situacao });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Col md="4">
                          <Label htmlFor="data_inicio">Inicio</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.startDate}
                            onChange={e => {
                              this.setState({ startDate: e });
                            }}
                            locale="br"
                          />
                        </Col>

                        <Col md="4">
                          <Label htmlFor="data_termino">Término</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.endDate}
                            onChange={e => {
                              this.setState({ endDate: e });
                            }}
                            locale="br"
                          />

                          <LaddaButton
                            type="submit"
                            className="col-8 btn btn-primary btn-ladda"
                            loading={this.state.issub}
                            onClick={e => {
                              this.doSearch();
                            }}
                          >
                            <i color="primary" className="fa fa-search" />{" "}
                            Procurar
                          </LaddaButton>
                        </Col>
                      </FormGroup>
                    </Row>
                    <Row
                      form
                      classNadme="col-md-15"
                      style={{ marginTop: "30px" }}
                    >
                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="imobiliarias">
                          Imobiliárias
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.imobiliarias}
                          options={this.state.listimobiliarias}
                          onChange={imobiliarias => {
                            errors.imobiliarias = "";
                            if (imobiliarias == null) {
                              imobiliarias = "";
                            }
                            this.setState({ imobiliarias });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="tipo_servico">
                          Tipo de Serviço
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.tipo_servico}
                          options={[
                            { value: "all", label: "Todos" },
                            {
                              value: "vistoria_entrada",
                              label: "Vistoria de Entrada"
                            }
                          ]}
                          onChange={tipo_servico => {
                            errors.tipo_servico = "";
                            if (tipo_servico == null) {
                              tipo_servico = "";
                            }
                            this.setState({ tipo_servico });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="vistoriador">
                          Vistoriador
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.vistoriador}
                          options={this.state.listvistoriadores}
                          onChange={vistoriador => {
                            errors.vistoriador = "";
                            if (vistoriador == null) {
                              vistoriador = "";
                            }
                            this.setState({ vistoriador });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="situacao">
                          Situação
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.situacao}
                          options={[
                            { value: "all", label: "Todas" },
                            {
                              value: "1",
                              label: "Aguardando atribuir ao vistoriador"
                            },
                            { value: "2", label: "Atribuido ao vistoriador" },
                            { value: "3", label: "Em posse do vistoriador" },
                            { value: "4", label: "Transmitido" }
                          ]}
                          onChange={situacao => {
                            errors.situacao = "";
                            if (situacao == null) {
                              situacao = "";
                            }
                            this.setState({ situacao });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Col md="4">
                          <Label htmlFor="data_inicio">Inicio</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.startDate}
                            onChange={e => {
                              this.setState({ startDate: e });
                            }}
                            locale="br"
                          />
                        </Col>

                        <Col md="4">
                          <Label htmlFor="data_termino">Término</Label>
                        </Col>
                        <Col xs="15" md="11">
                          <DatePicker
                            className="form-control"
                            selected={this.state.endDate}
                            onChange={e => {
                              this.setState({ endDate: e });
                            }}
                            locale="br"
                          />

                          <LaddaButton
                            type="submit"
                            className="col-8 btn btn-primary btn-ladda"
                            loading={this.state.issub}
                            onClick={e => {
                              this.doSearch();
                            }}
                          >
                            <i color="primary" className="fa fa-search" />{" "}
                            Procurar
                          </LaddaButton>
                        </Col>
                      </FormGroup>
                    </Row>

                    <BootstrapTable
                      data={this.state.table}
                      version="4"
                      bordered={false}
                      hover
                      pagination
                      options={{
                        noDataText: "Sem Informações",
                        onRowClick: this.rowHandleSurver
                      }}
                    >
                      <TableHeaderColumn
                        width="300"
                        dataField="agency"
                        dataSort
                        dataFormat={this.agencyFormater}
                      >
                        Empresa
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="200"
                        isKey
                        dataField="tipo_servico"
                        dataSort
                        dataFormat={this.servicoFormater}
                      >
                        Serviço
                      </TableHeaderColumn>
                      <TableHeaderColumn width="150" dataField="fone1" dataSort>
                        Clientes
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="200"
                        dataField="imovel"
                        dataSort
                        dataFormat={this.imovelFormater}
                      >
                        Imóvel
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        defaultSorted
                        width="200"
                        dataField="createdAt"
                        dataSort
                        dataFormat={this.dateFormatter}
                      >
                        Agendamento
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="150"
                        dataField="button"
                        dataFormat={this.buttonFormatter}
                      />
                    </BootstrapTable>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}
        />

        <Modal
          isOpen={this.state.showconfirm}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}>Apagar Vistoria</ModalHeader>
          <ModalBody>
            Deseja apagar o a vistoria{" "}
            {this.state.currentsurver ? this.state.currentsurver.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => this.onDeleteSurver(this.state.currentsurver)}
            >
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={e => this.setState({ showconfirm: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);
