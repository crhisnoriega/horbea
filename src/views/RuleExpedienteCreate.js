import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { addRule, doLogin, setEditUser, setEditSurver } from "../redux/redux";

import InputMask from "react-input-mask";
import { CompactPicker } from "react-color";
// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ItemType from "./ItemType";

import AsyncCreatableSelect from "react-select/async-creatable";
import AsyncSelect from "react-select/async";

import { registerLocale, setDefaultLocale } from "react-datepicker";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import pt from "date-fns/locale/pt";

const empty = {
  name: ""
};

const options = [
  { value: "sim", label: "Sim" },
  { value: "nao", label: "Não" }
];

class ServiceSurverCreate extends Component {
  constructor(props) {
    super(props);

    this.itensTable = {};

    this.days_week = [];

    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: [],
      cSelected: [],
      itens: [],
      values: {},
      data: {},
      currentrow: null,
      currentvalue: "",

      currentitens: [],

      selectedrow: null,
      color: "red",

      showColor: null,
      selectedOption: null,

      radioposition: -1,

      startDate: new Date()
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    axios
      .get("/key/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome da regra")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome da chave")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    let rules = this.props.session.rules;

    if (rules == null) {
      rules = [];
    }

    if (this.state.radioposition == 0) {
      values.type = "semanal";
      values.days_week = this.days_week;
    }

    if (this.state.radioposition == 1) {
      values.type = "anual";
      values.day = this.state.day;
      values.mes = this.state.mes;
    }

    if (this.state.radioposition == 2) {
      values.type = "dia";
      values.oneday = this.state.startDate;
    }

    values.createdAt = new Date();
    values.work = this.state.work.label;

    rules.push(values);

    this.props.addRule(rules);

    history.push("/app/expediente/create");

    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/keys/list");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Chave"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  loadItens = inputValue => {
    return axios
      .get("/environments/itens", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  loadEnvironments = inputValue => {
    return axios
      .get("/environments", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleEditableInput = e => {};

  handleInputChange = (row, event) => {
    let data = [...this.state.data];
    data[row.value] = event.target.value;

    this.setState({ data });
  };

  addItensToEnv = (agencyid, value) => {
    console.log(agencyid);
    console.log(value);

    this.itensTable[agencyid] = value;
  };

  editFormater = (cell, row) => {
    return (
      <div>
        {this.state.currentrow && this.state.currentrow.id == row.id ? (
          <InputGroup>
            <Input
              name="input"
              type="text"
              onChange={e => {
                this.setState({ currentvalue: e.target.value }, () => {
                  this.addItensToEnv(
                    this.state.currentrow.id,
                    this.state.currentvalue
                  );
                });
              }}
              value={this.state.currentvalue}
              mask="999.99"
              maskChar=" "
              tag={InputMask}
            />
            <InputGroupAddon addonType="prepend">
              <Button
                type="button"
                color="danger"
                onClick={e => {
                  row.label = this.state.currentvalue;
                  this.addItensToEnv(this.state.currentrow.id, null);
                  this.setState({
                    currentrow: null,
                    currentvalue: null
                  });
                }}
              >
                <i className="fa fa-remove" /> Cancelar
              </Button>
            </InputGroupAddon>
          </InputGroup>
        ) : (
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button
                type="button"
                color="primary"
                onClick={e => {
                  this.setState({
                    currentrow: row
                  });
                }}
              >
                <i className="fa fa-check" /> Habilitar
              </Button>
            </InputGroupAddon>
          </InputGroup>
        )}
      </div>
    );
  };

  photoFormater = (cell, row) => {
    return <src className="sem-imagem-small" src={row.photo} />;
  };

  addToWeek = (day, check) => {
    if (check) {
      this.days_week.push(day);
    } else {
      this.days_week = this.days_week.filter(item => item != day);
    }
  };

  render() {
    var _init = this.state.init;

    let days = [];
    for (var index = 0; index < 31; index++) {
      days.push({ value: index, label: index });
    }
    let meses = [
      { value: 1, label: "Janeiro" },
      { value: 2, label: "Fevereiro" },
      { value: 3, label: "Março" },
      { value: 4, label: "Abril" },
      { value: 5, label: "Maio" },
      { value: 6, label: "Junho" },
      { value: 7, label: "Juho" }
    ];

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />

        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Dados da Regra</CardHeader>
                  <CardBody>
                    <Row>
                      <Col xs="8">
                        <FormGroup>
                          <Label className="green-label" for="name">
                            Nome
                          </Label>
                          <InputGroup>
                            <Input
                              invalid={touched.name && !!errors.name}
                              type="text"
                              name="name"
                              id="name"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.name}
                            />
                            <FormFeedback>{errors.name}</FormFeedback>
                          </InputGroup>
                          <FormFeedback>{errors.name}</FormFeedback>
                        </FormGroup>
                      </Col>

                      <Col xs="4">
                        <FormGroup className="col-md-6">
                          <Label className="green-label" for="work">
                            Trabalha
                          </Label>
                          <InputGroup>
                            <Select
                              className="my-col-8"
                              value={this.state.work}
                              onChange={work => {
                                errors.work = "";
                                this.setState({ work });
                              }}
                              options={options}
                              placeholder="Selecionar"
                              theme={theme => ({
                                ...theme,
                                borderRadius: 0,
                                colors: {
                                  ...theme.colors,
                                  primary25: "white",
                                  primary: "#97B834"
                                }
                              })}
                            />
                            <FormFeedback>{errors.work}</FormFeedback>
                          </InputGroup>
                          <FormFeedback>{errors.work}</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="4">
                        <FormGroup>
                          <Label className="green-label" for="inicio">
                            Hora de inicio
                          </Label>
                          <InputGroup>
                            <Input
                              invalid={touched.inicio && !!errors.inicio}
                              type="text"
                              name="inicio"
                              id="inicio"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.inicio}
                              mask="99:99"
                              maskChar=" "
                              tag={InputMask}
                            />
                            <FormFeedback>{errors.inicio}</FormFeedback>
                          </InputGroup>
                          <FormFeedback>{errors.inicio}</FormFeedback>
                        </FormGroup>
                      </Col>

                      <Col xs="4">
                        <FormGroup>
                          <Label className="green-label" for="final">
                            Hora de Termino
                          </Label>
                          <InputGroup>
                            <Input
                              invalid={touched.final && !!errors.final}
                              type="text"
                              name="final"
                              id="final"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.final}
                              mask="99:99"
                              maskChar=" "
                              tag={InputMask}
                            />
                            <FormFeedback>{errors.final}</FormFeedback>
                          </InputGroup>
                          <FormFeedback>{errors.final}</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Mês/Semana/Dia</CardHeader>
                  <CardBody>
                    <Row style={{ marginLeft: "30px" }}>
                      <Col xs="8">
                        <FormGroup tag="fieldset">
                          <FormGroup>
                            <Label check>
                              <Input
                                onClick={e =>
                                  this.setState({ radioposition: 0 })
                                }
                                type="radio"
                                name="radio1"
                              />{" "}
                              Semanal
                            </Label>
                            {this.state.radioposition == 0 ? (
                              <FormGroup>
                                <Row>
                                  <Label className="col-md-3" check>
                                    <Input
                                      onClick={e =>
                                        this.addToWeek("seg", e.target.checked)
                                      }
                                      type="checkbox"
                                    />{" "}
                                    Segunda-feira
                                  </Label>
                                  <Label className="col-md-3" check>
                                    <Input
                                      onClick={e =>
                                        this.addToWeek("ter", e.target.checked)
                                      }
                                      type="checkbox"
                                    />{" "}
                                    Terça-feira
                                  </Label>
                                  <Label className="col-md-3" check>
                                    <Input
                                      onClick={e =>
                                        this.addToWeek("qua", e.target.checked)
                                      }
                                      type="checkbox"
                                    />{" "}
                                    Quarta-feira
                                  </Label>
                                  <Label
                                    onClick={e =>
                                      this.addToWeek("qui", e.target.checked)
                                    }
                                    className="col-md-3"
                                    check
                                  >
                                    <Input type="checkbox" /> Quinta-feira
                                  </Label>
                                  <Label className="col-md-3" check>
                                    <Input type="checkbox" /> Sexta-feira
                                  </Label>
                                  <Label className="col-md-3" check>
                                    <Input type="checkbox" /> Sabado
                                  </Label>
                                  <Label className="col-md-3" check>
                                    <Input type="checkbox" /> Domingo
                                  </Label>
                                </Row>
                              </FormGroup>
                            ) : null}
                          </FormGroup>
                          <FormGroup>
                            <Label check>
                              <Input
                                onClick={e =>
                                  this.setState({ radioposition: 1 })
                                }
                                type="radio"
                                name="radio1"
                              />{" "}
                              Anual
                            </Label>
                            {this.state.radioposition == 1 ? (
                              <FormGroup>
                                <Row>
                                  <Select
                                    className="col-3"
                                    placeholder="Selecionar"
                                    value={this.state.day}
                                    onChange={this.handleChange}
                                    options={days}
                                    theme={theme => ({
                                      ...theme,
                                      borderRadius: 0,
                                      colors: {
                                        ...theme.colors,
                                        primary25: "white",
                                        primary: "#97B834"
                                      }
                                    })}
                                  />
                                  {"/"}
                                  <Select
                                    placeholder="Selecionar"
                                    className="col-4"
                                    value={this.state.mes}
                                    onChange={this.handleChange}
                                    options={meses}
                                    theme={theme => ({
                                      ...theme,
                                      borderRadius: 0,
                                      colors: {
                                        ...theme.colors,
                                        primary25: "white",
                                        primary: "#97B834"
                                      }
                                    })}
                                  />
                                </Row>
                              </FormGroup>
                            ) : null}
                          </FormGroup>
                          <FormGroup>
                            <Label check>
                              <Input
                                onClick={e =>
                                  this.setState({ radioposition: 2 })
                                }
                                type="radio"
                                name="radio1"
                              />{" "}
                              Diario
                            </Label>
                            {this.state.radioposition == 2 ? (
                              <FormGroup className="col-md-4">
                                <DatePicker
                                  className="form-control"
                                  style={{ color: "red" }}
                                  selected={this.state.startDate}
                                  onChange={e =>
                                    this.setState({ startDate: e })
                                  }
                                  locale="br"
                                />
                                {errors.date && touched.date ? (
                                  <div className="invalid-feedback d-block">
                                    {errors.date}
                                  </div>
                                ) : null}
                              </FormGroup>
                            ) : null}
                          </FormGroup>
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <LaddaButton
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </LaddaButton>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro efetuando Cadastro
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  addRule
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ServiceSurverCreate)
);
