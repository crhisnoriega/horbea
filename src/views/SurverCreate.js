import React, { Component } from "react";

import {
  ButtonGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditSurver } from "../redux/redux";

import InputMask from "react-input-mask";

import moment from "moment";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import AsyncSelect from "react-select/async";
import Select from "react-select";

// import "react-select/dist/react-select.min.css";

import history from "../history/history";

import PropertyModal from "./Modals/PropertyModal";
import CustomerModal from "./Modals/CustomerModal";
import SurveyorModal from "./Modals/SurveyorModal";

import { registerLocale, setDefaultLocale } from "react-datepicker";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import pt from "date-fns/locale/pt";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from "react-tooltip";

registerLocale("br", pt);

const empty = {
  tipo_servico: {
    value: "vistoria_entrada",
    label: "Vistoria de Entrada"
  },
  modelo_vistoria: { value: "padrao", label: "Casa Padrão" },
  contrato: "",
  data: "",
  hora: "",
  imobiliaria: "",
  situacao: "",
  nota: ""
};

class Users extends Component {
  constructor(props) {
    var day = Date.parse(localStorage.getItem("day"));

    super(props);
    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      startDate: day ? day : new Date(),

      showVistoriador: false,
      vistoriador: null,

      imovel: null,
      showImovel: false,

      locatarios: [],
      showLocatario: false,

      locadores: [],
      showLocador: false,

      fiadores: [],
      showFiador: false,

      testemunhas: [],
      showTestemunha: false,

      tipo_servico: {
        value: "vistoria_entrada",
        label: "Vistoria de Entrada"
      },
      modelo_vistoria: { value: "padrao", label: "Casa Padrão" },

      allagencies: []
    };

    this.handleChange = this.handleChange.bind(this);
  }

  toggleBtn = name => {};

  componentDidMount() {
    console.log("edit");
    console.log(this.props.session.edit_surver);
    axios
      .get("/agency/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ allagencies: results });

        if (this.props.session.edit_surver != null) {
          this.setState({
            vistoriador: this.props.session.edit_surver.vistoriador,
            imobiliaria: this.props.session.edit_surver.agency,
            imovel: this.props.session.edit_surver.imovel,
            tipo_servico: this.props.session.edit_surver.tipo_servico,
            modelo_vistoria: this.props.session.edit_surver.modelo_vistoria,
            locadores: this.props.session.edit_surver.locadores,
            locatarios: this.props.session.edit_surver.locatarios,
            fiadores: this.props.session.edit_surver.fiadores
              ? this.props.session.edit_surver.fiadores
              : [],
            testemunhas: this.props.session.edit_surver.testemunhas
              ? this.props.session.edit_surver.testemunhas
              : []
          });

          this.props.setEditSurver(null);
        }
        return results;
      })
      .catch(error => {
        console.log(error);
      });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      contrato: Yup.string().required("Número de contrato")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      contrato: Yup.string().required("Número de contrato")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log("handle values: " + JSON.stringify(values));

    console.log(this.state.tipo_servico);
    console.log(this.state.modelo_vistoria);

    values.agency = this.state.imobiliaria;
    values.imovel = this.state.imovel;
    values.tipo_servico = this.state.tipo_servico
      ? this.state.tipo_servico.value
      : null;
    values.modelo_vistoria = this.state.modelo_vistoria
      ? this.state.modelo_vistoria.value
      : null;

    if (this.state.vistoriador != null) {
      this.state.vistoriador.photo = "";
    }
    values.vistoriador = this.state.vistoriador;
    values.locadores = this.state.locadores;
    values.locatarios = this.state.locatarios;

    console.log("handle values with state: " + JSON.stringify(values));

    axios
      .post("/surver", values, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ showinfo: true });
        setSubmitting(false);
      })
      .catch(error => {
        console.log(error);
        this.setState({ showerror: true, messageerror: error });
        setSubmitting(false);
      });

    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/surver/list");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  loadAgency = inputValue => {
    return axios
      .get("/agency/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  buttonFormatterLocador = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="remove"
          data-tip="Remover"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.onRemoveCustomer("locador", row);
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  buttonFormatterLocatario = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="remove"
          data-tip="Remover"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.onRemoveCustomer("locatario", row);
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  buttonFormatterFiador = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="remove"
          data-tip="Remover"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.onRemoveCustomer("fiador", row);
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  buttonFormatterTestemunha = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="remove"
          data-tip="Remover"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.onRemoveCustomer("testemunha", row);
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("LLL");
  };

  showVistoriador = () => {
    this.setState({ showVistoriador: true });
  };

  callbackVistoriador = e => {
    this.setState({ showVistoriador: false, vistoriador: e });
  };

  showImovel = () => {
    this.setState({ showImovel: true });
  };

  callbackImovel = e => {
    this.setState({ showImovel: false, imovel: e });
  };

  showLocatario = () => {
    this.setState({ showLocatario: true });
  };

  callbackLocatario = e => {
    if (e != null) {
      this.setState({
        showLocatario: false,
        locatarios: [e, ...this.state.locatarios]
      });
    } else {
      this.setState({
        showLocatario: false
      });
    }
  };

  showLocador = () => {
    this.setState({ showLocador: true });
  };

  callbackLocador = e => {
    if (e != null) {
      this.setState({
        showLocador: false,
        locadores: [e, ...this.state.locadores]
      });
    } else {
      this.setState({
        showLocador: false
      });
    }
  };

  callbackTestemunha = e => {
    if (e != null) {
      this.setState({
        showTestemunha: false,
        testemunhas: [e, ...this.state.testemunhas]
      });
    } else {
      this.setState({
        showTestemunha: false
      });
    }
  };

  callbackFiador = e => {
    if (e != null) {
      this.setState({
        showFiador: false,
        fiadores: [e, ...this.state.fiadores]
      });
    } else {
      this.setState({
        showFiador: false
      });
    }
  };

  onRemoveCustomer = (customertype, customer) => {
    if (customertype == "locador") {
      let filteredArray = this.state.locadores.filter(
        s => s.id !== customer.id
      );
      this.setState({ locadores: filteredArray });
    }

    if (customertype == "locatario") {
      let filteredArray = this.state.locatarios.filter(
        s => s.id !== customer.id
      );
      this.setState({ locatarios: filteredArray });
    }

    if (customertype == "fiador") {
      let filteredArray = this.state.fiadores.filter(s => s.id !== customer.id);
      this.setState({ fiadores: filteredArray });
    }

    if (customertype == "testemunha") {
      let filteredArray = this.state.testemunhas.filter(
        s => s.id !== customer.id
      );
      this.setState({ testemunhas: filteredArray });
    }
  };

  render() {
    console.log(this.state.showVistoriador);

    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        {this.state.showVistoriador ? (
          <SurveyorModal
            name={"Vistoriadores"}
            callback={this.callbackVistoriador}
            isShow={true}
            type={"vistoriador"}
          />
        ) : (
          ""
        )}

        {this.state.showImovel ? (
          <PropertyModal
            name={"Vistoriador"}
            callback={this.callbackImovel}
            isShow={true}
          />
        ) : (
          ""
        )}

        {this.state.showLocatario ? (
          <CustomerModal
            name={"Locatario"}
            callback={this.callbackLocatario}
            isShow={true}
            type={"locatarios"}
          />
        ) : (
          ""
        )}

        {this.state.showLocador ? (
          <CustomerModal
            name={"Locador"}
            callback={this.callbackLocador}
            isShow={true}
            type={"locador"}
          />
        ) : (
          ""
        )}

        {this.state.showFiador ? (
          <CustomerModal
            name={"Fiador"}
            callback={this.callbackFiador}
            isShow={true}
            type={"fiador"}
          />
        ) : (
          ""
        )}

        {this.state.showTestemunha ? (
          <CustomerModal
            name={"Testemunha"}
            callback={this.callbackTestemunha}
            isShow={true}
            type={"testemunha"}
          />
        ) : (
          ""
        )}

        <Loading show={this.state.show} showSpinner={false} />

        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Dados</CardHeader>
                  <CardBody>
                    <Row form className="col-md-15">
                      <FormGroup className="col-md-5">
                        <Label className="green-label" for="situacao">
                          Situação
                        </Label>
                        <Input
                          type="text"
                          name="situacao"
                          disabled="disabled"
                          id="situacao"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.situacao}
                          placeholder="Aguardando atribuir ao vistoriador"
                        />
                        <FormFeedback>{errors.situacao}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="tipo_servico">
                          Tipo de Serviço
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.tipo_servico}
                          options={[
                            {
                              value: "vistoria_entrada",
                              label: "Vistoria de Entrada"
                            }
                          ]}
                          onChange={tipo_servico => {
                            errors.tipo_servico = "";
                            if (tipo_servico == null) {
                              tipo_servico = "";
                            }
                            this.setState({ tipo_servico });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.tipo_servico}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="modelo_vistoria">
                          Modelo para Vistoria
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.modelo_vistoria}
                          options={[
                            { value: "padrao", label: "Casa Padrão" },
                            { value: "apartamento", label: "Apartamento" }
                          ]}
                          onChange={modelo_vistoria => {
                            errors.modelo_vistoria = "";
                            if (modelo_vistoria == null) {
                              modelo_vistoria = "";
                            }
                            this.setState({ modelo_vistoria });
                          }}
                          placeholder="Selecionar"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.modelo_vistoria}
                        </Label>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="contrato">
                          Número Contrato
                        </Label>
                        <Input
                          invalid={touched.contrato && !!errors.contrato}
                          type="text"
                          name="contrato"
                          id="contrato"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.contrato}
                          mask="99999999"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.contrato}</FormFeedback>
                      </FormGroup>

                      <FormGroup className="col-md-4">
                        <Label className="green-label" for="date">
                          Data
                        </Label>
                        <br />
                        <DatePicker
                          className="form-control"
                          style={{ color: "red" }}
                          selected={this.state.startDate}
                          onChange={this.handleChange}
                          locale="br"
                        />
                        {errors.date && touched.date ? (
                          <div className="invalid-feedback d-block">
                            {errors.date}
                          </div>
                        ) : null}
                      </FormGroup>

                      <FormGroup className="col-md-2">
                        <Label className="green-label" for="hora">
                          Hora
                        </Label>
                        <Input
                          invalid={touched.hora && !!errors.hora}
                          type="text"
                          name="hora"
                          id="hora"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.hora}
                          mask="99:99"
                          maskChar=" "
                          tag={InputMask}
                        />
                        <FormFeedback>{errors.hora}</FormFeedback>
                      </FormGroup>
                    </Row>

                    <FormGroup>
                      <Label>Vistoriador</Label>

                      <InputGroup>
                        <Input
                          className="col-md-6"
                          invalid={touched.vistoriador && !!errors.vistoriador}
                          type="text"
                          name="vistoriador"
                          id="vistoriador"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={
                            this.state.vistoriador
                              ? this.state.vistoriador.name
                              : ""
                          }
                        />
                        <InputGroupAddon addonType="prepend">
                          {this.state.vistoriador != null ? (
                            <Button
                              style={{ marginLeft: "8px" }}
                              type="button"
                              color="danger"
                              onClick={e => {
                                this.setState({ vistoriador: null });
                              }}
                            >
                              <i className="fa fa-remove" /> Remover
                            </Button>
                          ) : (
                            ""
                          )}

                          <Button
                            style={{ marginLeft: "8px" }}
                            type="button"
                            color="primary"
                            onClick={e => {
                              this.showVistoriador();
                            }}
                          >
                            <i className="fa fa-search" /> Busca
                          </Button>
                        </InputGroupAddon>
                      </InputGroup>

                      <FormFeedback>{errors.vistoriador}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="nota">
                        Nota para o Vistoriador
                      </Label>
                      <Input
                        className="col-md-10"
                        invalid={touched.nota && !!errors.nota}
                        type="textarea"
                        name="nota"
                        id="nota"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.nota}
                      />
                      <FormFeedback>{errors.nota}</FormFeedback>
                    </FormGroup>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Imobiliária</CardHeader>
                  <CardBody>
                    <Row form className="col-md-15">
                      <FormGroup className="col-md-5">
                        <Label className="green-label" for="imobiliaria">
                          Imobiliária
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.imobiliaria}
                          options={this.state.allagencies}
                          onChange={imobiliaria => {
                            errors.imobiliaria = "";
                            if (imobiliaria == null) {
                              imobiliaria = "";
                            }
                            this.setState({ imobiliaria });
                          }}
                          noOptionsMessage={e => {
                            return "Digite uma imobiliaria";
                          }}
                          placeholder="Digite uma imobiliaria"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.deviceReadonly}
                        </Label>
                      </FormGroup>
                    </Row>

                    <FormGroup>
                      <Label>Imovel</Label>

                      <InputGroup>
                        <Input
                          className="col-md-6"
                          invalid={touched.imovel && !!errors.imovel}
                          type="text"
                          name="imovel"
                          id="imovel"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={
                            this.state.imovel ? this.state.imovel.street : ""
                          }
                        />
                        <InputGroupAddon addonType="prepend">
                          {this.state.imovel != null ? (
                            <Button
                              style={{ marginLeft: "8px" }}
                              type="button"
                              color="danger"
                              onClick={e => {
                                this.setState({ imovel: null });
                              }}
                            >
                              <i className="fa fa-remove" /> Remover
                            </Button>
                          ) : (
                            ""
                          )}

                          <Button
                            style={{ marginLeft: "8px" }}
                            type="button"
                            color="primary"
                            onClick={e => {
                              this.showImovel();
                            }}
                          >
                            <i className="fa fa-search" /> Busca
                          </Button>
                        </InputGroupAddon>
                      </InputGroup>

                      <FormFeedback>{errors.vistoriador}</FormFeedback>
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-10">
                        <Label className="green-label" for="locatario">
                          Locatários
                        </Label>
                        <br />
                        <ButtonGroup>
                          <Button
                            style={{ width: "210px" }}
                            color="primary"
                            onClick={e => {
                              this.showLocatario();
                            }}
                          >
                            <i color="primary" className="fa fa-plus" />{" "}
                            Adicionar Locatario
                          </Button>
                        </ButtonGroup>

                        <BootstrapTable
                          data={this.state.locatarios}
                          version="4"
                          bordered={false}
                          hover
                          options={{
                            noDataText: "Nada Selecionado"
                          }}
                          searchPlaceholder={"Busca"}
                        >
                          <TableHeaderColumn
                            width="300"
                            isKey
                            dataField="name"
                            dataSort
                          >
                            Nome
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            defaultSorted
                            width="200"
                            dataField="cpf"
                            dataSort
                          >
                            CPF
                          </TableHeaderColumn>
                          <TableHeaderColumn
                            width="150"
                            dataField="button"
                            dataFormat={this.buttonFormatterLocatario}
                          />
                        </BootstrapTable>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-10">
                        <Label className="green-label" for="locador">
                          Locador
                        </Label>
                        <br />
                        <ButtonGroup>
                          <Button
                            style={{ width: "210px" }}
                            color="primary"
                            onClick={e => {
                              this.showLocador();
                            }}
                          >
                            <i color="primary" className="fa fa-plus" />{" "}
                            Adicionar Locador
                          </Button>
                        </ButtonGroup>

                        <BootstrapTable
                          data={this.state.locadores}
                          version="4"
                          bordered={false}
                          hover
                          options={{
                            noDataText: "Nada Selecionado"
                          }}
                          searchPlaceholder={"Busca"}
                        >
                          <TableHeaderColumn
                            width="300"
                            isKey
                            dataField="name"
                            dataSort
                          >
                            Nome
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            defaultSorted
                            width="200"
                            dataField="cpf"
                            dataSort
                          >
                            CPF
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            width="150"
                            dataField="button"
                            dataFormat={this.buttonFormatterLocador}
                          />
                        </BootstrapTable>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-10">
                        <Label className="green-label" for="locatario">
                          Fiador
                        </Label>
                        <br />
                        <ButtonGroup>
                          <Button
                            style={{ width: "210px" }}
                            color="primary"
                            onClick={e => {
                              this.setState({ showFiador: true });
                            }}
                          >
                            <i color="primary" className="fa fa-plus" />{" "}
                            Adicionar Fiador
                          </Button>
                        </ButtonGroup>

                        <BootstrapTable
                          data={this.state.fiadores}
                          version="4"
                          bordered={false}
                          hover
                          options={{
                            noDataText: "Nada Selecionado"
                          }}
                          searchPlaceholder={"Busca"}
                        >
                          <TableHeaderColumn
                            width="300"
                            isKey
                            dataField="name"
                            dataSort
                          >
                            Nome
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            defaultSorted
                            width="200"
                            dataField="cpf"
                            dataSort
                          >
                            CPF
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            width="150"
                            dataField="button"
                            dataFormat={this.buttonFormatterFiador}
                          />
                        </BootstrapTable>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-10">
                        <Label className="green-label" for="locatario">
                          Testemunha
                        </Label>
                        <br />
                        <ButtonGroup>
                          <Button
                            style={{ width: "210px" }}
                            color="primary"
                            onClick={e => {
                              this.setState({ showTestemunha: true });
                            }}
                          >
                            <i color="primary" className="fa fa-plus" />{" "}
                            Adicionar Testemunha
                          </Button>
                        </ButtonGroup>

                        <BootstrapTable
                          data={this.state.testemunhas}
                          version="4"
                          bordered={false}
                          hover
                          options={{
                            noDataText: "Nada Selecionado"
                          }}
                          searchPlaceholder={"Busca"}
                        >
                          <TableHeaderColumn
                            width="300"
                            isKey
                            dataField="name"
                            dataSort
                          >
                            Nome
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            defaultSorted
                            width="200"
                            dataField="cpf"
                            dataSort
                          >
                            CPF
                          </TableHeaderColumn>

                          <TableHeaderColumn
                            width="150"
                            dataField="button"
                            dataFormat={this.buttonFormatterTestemunha}
                          />
                        </BootstrapTable>
                      </FormGroup>
                    </Row>
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <LaddaButton
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </LaddaButton>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>Erro no cadastro</ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
