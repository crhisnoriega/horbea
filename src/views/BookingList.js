import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  ButtonGroup,
  Button,
  Card,
  CardGroup,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser, setEditAgency } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from "react-tooltip";

import data from "./_data_agency";

class ContainerAdminList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: [],
      filter: "",
      show: false,
      users: [],
      showconfirm: false,
      currentagency: null
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.setState({ show: true });
    var session = JSON.parse(localStorage.getItem("redux"));
    try {
      this.vTaxes = session.session.login[0].vTaxes;
      this.admin_id = session.session.login[0].iAdminId;
    } catch (e) {}

    axios
      .get(
        "/agenda/cab_booking_json.php?admin_id=" + this.admin_id,
        { type: "findallpointsadmin", admin_id: this.admin_id },
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        } else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  refresh_booking = booking_id => {
    console.log(booking_id);

    var url_query =
      "/agenda/cab_booking_json.php?action=refresh&hdn_del_id=" +
      booking_id +
      "&dBooking_date=" +
      moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    console.log(url_query);

    axios
      .get(
        url_query,
        {},
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        this.setState({ showconfirmrefresh: false });
        this.componentDidMount();
      })
      .catch(error => {
        console.log(error);
      });
  };

  cancel_booking = booking_id => {
    console.log(booking_id);
    axios
      .get(
        "/agenda/cab_booking_json.php?action=delete&hdn_del_id=" + booking_id,
        {},
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        this.setState({ showconfirmrefresh: false });
        this.componentDidMount();
      })
      .catch(error => {
        console.log(error);
      });
  };

  callback = user => {
    this.props.setEditUser(user);
  };

  onEditAgency = agency => {
    console.log(agency);
    this.props.setEditAgency(agency);
    history.push("/app/agency/register");
  };

  onDeleteAgency = agency => {
    console.log(agency);
    axios
      .delete("/agency/" + agency.id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        let filteredArray = this.state.table.filter(s => s.id !== agency.id);
        this.setState({ table: filteredArray, showconfirm: false });
      })
      .catch(error => console.log(error));
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("DD/MM/YYYY HH:mm:SS");
  };

  imgFormatter = (cell, row) => {
    var icon = "";
    if (cell == "Pending") {
      icon = "../assets/booking/realizado.png";
    }

    if (cell == "Cancel") {
      icon = "../assets/booking/cancelado.png";
    }

    if (row.iTripId != 0 && cell == "Pending") {
      icon = "../assets/booking/realizado.png";
    }

    if (cell == "Completed" && row.iTripId != 0) {
      icon = "../assets/booking/confirmado.png";
    }

    console.log("row: " + JSON.stringify(cell));
    return (
      <div style={{ width: "55px" }}>
        <img style={{ width: "24px", height: "24px" }} src={icon} />
      </div>
    );
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    if (row.eStatus != "Cancel") {
      return (
        <div>
          <Button
            data-for="remove"
            data-tip="Cancelar"
            color="danger"
            style={{ marginLeft: "5px" }}
            className="btn-brand"
            onClick={e => {
              this.setState({ currentagency: row, showconfirm: true });
            }}
          >
            <i color="danger" className="fa fa-remove" />
          </Button>

          <Button
            data-for="remove"
            data-tip="Reagendar"
            color="info"
            style={{ marginLeft: "5px" }}
            className="btn-brand"
            onClick={e => {
              this.setState({ currentagency: row, showconfirmrefresh: true });
            }}
          >
            <i color="danger" className="fa fa-refresh"></i>
          </Button>

          <ReactTooltip id="edit" multiline={true} />
          <ReactTooltip id="remove" multiline={true} />
        </div>
      );
    } else {
      return <div />;
    }
  };

  rowStyleFormat = (row, rowIdx) => {
    return {};
  };

  descriptionPoint = (cell, row) => {
    console.log(cell);
    return "";
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>Agendamentos</CardHeader>
              <CardBody style={{ backgroundColor: "white" }}>
                <CardGroup
                  style={{
                    background: "rgba(233, 84, 39, 0.02)",
                    borderRadius: "5px",
                    border: "1px solid #E95327"
                  }}
                >
                  <div
                    className="card m-4"
                    style={{ backgroundColor: "white" }}
                  >
                    <BootstrapTable
                      data={this.state.table}
                      trStyle={this.rowStyleFormat}
                      version="4"
                      bordered={false}
                      hover
                      pagination
                      search
                      options={{
                        noDataText: "Sem Informações"
                      }}
                      searchPlaceholder={"Busca"}
                    >
                      <TableHeaderColumn
                        width="100"
                        isKey
                        dataField="iCabBookingId"
                        dataSort
                      >
                        Id
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        width="120"
                        dataField="button"
                        dataFormat={this.buttonFormatter}
                      >
                        Operações
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="100"
                        dataField="iTripId"
                        dataSort
                        dataFormat={(cell, row) => {
                          if (cell == "0") {
                            return "--";
                          } else {
                            return cell;
                          }
                        }}
                      >
                        iTripId
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="110"
                        dataField="vBookingNo"
                        dataSort
                      >
                        Booking No
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        width="100"
                        dataField="eStatus"
                        dataFormat={this.imgFormatter}
                      >
                        Status
                      </TableHeaderColumn>

                      <TableHeaderColumn width="100" dataField="eStatus">
                        eStatus
                      </TableHeaderColumn>

                      <TableHeaderColumn width="100" dataField="iCronStage">
                        Tentativas
                      </TableHeaderColumn>

                      <TableHeaderColumn width="150" dataField="rider">
                        Passageiro
                      </TableHeaderColumn>

                      <TableHeaderColumn width="150" dataField="driver">
                        Motorista
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataFormat={this.dateFormatter}
                        dataField="dBooking_date"
                      >
                        Data Agendamento
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="300"
                        dataField="vSourceAddresss"
                      >
                        Endereço Inicio
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="300"
                        dataField="tDestAddress"
                        dataSort
                      >
                        Endereço Final
                      </TableHeaderColumn>
                    </BootstrapTable>
                  </div>
                </CardGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.showconfirm}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}></ModalHeader>
          <ModalBody>
            Deseja cancelar o Agendamento{" "}
            {this.state.currentagency ? this.state.currentagency.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e =>
                this.cancel_booking(this.state.currentagency.iCabBookingId)
              }
            >
              Confirmar
            </Button>{" "}
            <Button
              color="info"
              onClick={e => this.setState({ showconfirm: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.showconfirmrefresh}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}></ModalHeader>
          <ModalBody>
            Deseja reagendar a corrida{" "}
            {this.state.currentagency ? this.state.currentagency.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e =>
                this.refresh_booking(this.state.currentagency.iCabBookingId)
              }
            >
              Confirmar
            </Button>{" "}
            <Button
              color="info"
              onClick={e => this.setState({ showconfirmrefresh: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContainerAdminList);
