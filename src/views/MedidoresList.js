import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser, setEditSurver } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from "react-tooltip";

import data from "./_data_agency";

class ServiceSurveList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: [],
      filter: "",
      show: false,
      users: [],
      showconfirm: false,
      currentsurver: null
    };
  }

  componentDidMount() {
    axios
      .get("/medidor/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        } else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.props.setEditUser(user);
  };

  onEditSurver = surver => {
    console.log(surver);
    this.props.setEditSurver(surver);
    history.push("/app/surveyors/register");
  };

  onDeleteMedidor = surver => {
    console.log(surver);

    let filteredArray = this.state.table.filter(s => s.id !== surver.id);
    this.setState({ table: filteredArray, showconfirm: false });

    axios
      .delete("/medidor/" + surver.id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        let filteredArray = this.state.table.filter(s => s.id !== surver.id);
        this.setState({ table: filteredArray, showconfirm: false });
      })
      .catch(error => console.log(error));
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="remove"
          data-tip="Apagar Medidor"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.setState({ currentsurver: row, showconfirm: true });
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="edit" multiline={true} />
        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("DD/MM/YYYY HH:mm:SS");
  };

  colorFormatter = (cell, row) => {
    return (
      <div className="sem-imagem-tiny" style={{ backgroundColor: cell }} />
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>Medidor</CardHeader>

              <CardBody>
                <ButtonGroup>
                  <Button
                    color="primary"
                    onClick={e => {
                      this.props.setEditSurver(null);
                      history.push("/app/medidor/create");
                    }}
                  >
                    <i color="primary" className="fa fa-edit" /> Novo Medidor
                  </Button>
                </ButtonGroup>
                <BootstrapTable
                  data={this.state.table}
                  version="4"
                  bordered={false}
                  hover
                  pagination
                  search
                  options={{
                    noDataText: "Sem Informações"
                  }}
                  searchPlaceholder={"Busca"}
                >
                  <TableHeaderColumn
                    width="200"
                    isKey
                    dataField="name"
                    dataSort
                  >
                    Nome
                  </TableHeaderColumn>

                  <TableHeaderColumn
                    width="200"                    
                    dataField="unit"
                    dataSort
                  >
                    Unidade
                  </TableHeaderColumn>

                  <TableHeaderColumn
                    defaultSorted
                    width="200"
                    dataField="createdAt"
                    dataSort
                    dataFormat={this.dateFormatter}
                  >
                    Ultima Atualização
                  </TableHeaderColumn>

                  <TableHeaderColumn
                    defaultSorted
                    width="200"
                    dataField="createdAt"
                    dataSort
                    dataFormat={this.buttonFormatter}
                  />
                </BootstrapTable>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.showconfirm}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}>Apagar Medidor</ModalHeader>
          <ModalBody>
            Deseja apagar o medidor{" "}
            {this.state.currentsurver ? this.state.currentsurver.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => this.onDeleteMedidor(this.state.currentsurver)}
            >
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={e => this.setState({ showconfirm: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceSurveList);
