import React, { Component } from "react";
import {
  Tooltip,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  ButtonGroup,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
}
from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from 'react-tooltip'

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: "",
      show: false,
      users: [],
      table: [],
      showconfirm: false,
      currentuser: null
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
  }

  componentDidMount() {
    axios
      .get("/user/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        }
        else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.props.setEditUser(user);
    history.push("/app/users/register");
  };

  onDeleteUser = user => {
    console.log(user);
    axios
      .delete("/user/" + user.login, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        let filteredArray = this.state.table.filter(
          s => s.login !== user.login
        );
        this.setState({ table: filteredArray, showconfirm: false });
      })
      .catch(error => console.log(error));
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="main"
          data-tip="Apagar Usuario"
          color="danger"
          className="btn-brand"
          onClick={e => {
            this.setState({currentuser:row, showconfirm:true});
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>
       <ReactTooltip id='main' multiline={true}/>
        
      </div>
    );
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("LLL");
  };

  render() {
    return (
      <div className="animated fadeIn">
      
        <Row>
          <Col>
            <Card>
              <CardHeader>Acessos</CardHeader>
              <CardBody>
                <ButtonGroup>
                  <Button
                  
                    color="primary"
                    onClick={e => {
                      history.push("/app/users/register");
                    }}
                  >
                    <i color="primary" className="fa fa-edit" /> Novo Usuario
                  </Button>
                </ButtonGroup>
                <BootstrapTable
                  data={this.state.table}
                  version="4"
                  bordered={false}
                  hover
                  pagination
                  search
                  options={{
                    noDataText: "Sem Informações"
                  }}
                  searchPlaceholder={"Busca"}
                >
                  <TableHeaderColumn width="300" dataField="firstName" dataSort>
                    Nome
                  </TableHeaderColumn>
                  <TableHeaderColumn width="150" isKey dataField="login">
                    CPF
                  </TableHeaderColumn>
                  <TableHeaderColumn width="100" dataField="sexo" dataSort>
                    Sexo
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="createdAt"
                    dataSort
                    width="200"
                    dataFormat={this.dateFormatter}
                  >
                    Data
                  </TableHeaderColumn>

                  <TableHeaderColumn
                    width="150"
                    dataField="button"
                    dataFormat={this.buttonFormatter}
                  />
                </BootstrapTable>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
        <Modal isOpen={this.state.showconfirm} className={this.props.className} backdrop={this.state.backdrop}>
          <ModalHeader toggle={this.toggle}>Apagar Usuario</ModalHeader>
          <ModalBody>
            Deseja apagar o usuario {this.state.currentuser? this.state.currentuser.firstName:""} ?
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={(e) => this.onDeleteUser(this.state.currentuser)}>Confirmar</Button>{' '}
            <Button color="secondary" onClick={(e) => this.setState({showconfirm:false})}>Cancelar</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);
