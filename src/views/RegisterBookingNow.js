import React, { Component } from "react";

import { Input, CardBody, Card, Row, Col } from "reactstrap";
import {
  Table,
  CardGroup,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Media,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  CardHeader,
  FormGroup,
  Label,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import { Formik, Form, ErrorMessage } from "formik";
import InputMask from "react-input-mask";
import Select from "react-select";

import Image from "react-bootstrap/Image";

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";

import { GoogleMap, LoadScript } from "@react-google-maps/api";
import { Marker, InfoWindow } from "@react-google-maps/api";

import { Container2, Button2, Link } from "react-floating-action-button";

import axios from "axios";

import GooglePlacesAutocomplete from "react-google-places-autocomplete";
// If you want to use the provided css
import "react-google-places-autocomplete/dist/assets/index.css";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import { geocodeByAddress, getLatLng } from "react-google-places-autocomplete";
// import { geolocated } from "react-geolocated";
import * as Yup from "yup";
import ReactSearchBox from "react-search-box";
import SearchField from "react-search-field";
import history from "../history/history";
import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";
import GoogleMapReact from "google-map-react";

import parse from "html-react-parser";

import ReactLoading from "react-loading";
import moment from "moment";

import * as Datetime from "react-datetime";
import CurrencyFormat from "react-currency-format";

import { IconContext } from "react-icons";
import { FaSearch } from "react-icons/fa";

require("moment/locale/pt");
const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Map extends Component {
  static defaultProps = {
    center: {
      lat: -23.533773,
      lng: -46.62529
    },
    zoom: 11
  };

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      lat: null,
      lng: null,
      firstlat: null,
      firstlng: null,
      heading: 0,
      dropdownOpen: false,
      tipo: "Tipo de Denuncia",
      points: [],
      counter: 0,
      currentpoint: null,
      checkbutton: false,
      uncheckbutton: false,
      alreadycheck: undefined,
      timechecked: 0,
      driverlist: "",
      center: {
        lat: -23.533773,
        lng: -46.62529
      },
      calculating: false,
      submitting: false,
      submittingnow: false,
      bookdate: new Date(),
      finding_user: false,
      driver_type: "driver_any",
      showerror: false,
      messageerror: "",
      booking_after: false,
      elas: false,
      typingTimeout: 0
    };

    this.counter = 0;
    this.icons_pins = [];
    // this.checkUserBalance = this.checkUserBalance.bind(this);
  }

  validationSchemaComplete = function (values) {
    return Yup.object().shape({});
  };

  validationSchemaEdit = function (values) {
    return Yup.object().shape({});
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  toggle = () => {
    this.setState({ dropdownOpen: !this.state.dropdownOpen });
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    this.setState({ show: true });

    this.vTaxes = 0;
    var session = JSON.parse(localStorage.getItem("redux"));
    try {
      this.vTaxes = session.session.login[0].vTaxes;
      this.admin_id = session.session.login[0].iAdminId;
    } catch (e) { }

    this.populateCarTypes(this.admin_id);
  }

  onShow = () => {
    this.setState({ show: true });
  };

  onHide = () => {
    this.setState({ show: false });
  };

  geoDecode = (lat1, lng1, callback) => {
    if (!this.state.logradouro) {
      axios
        .get(
          "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
          lat1 +
          "," +
          lng1 +
          "&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk",
          {}
        )
        .then(response => {
          /*this.setState({
            logradouro: response.data.results[0].formatted_address,
            lat: lat1,
            lng: lng1
          });*/
          callback(response);
        });
    }

    if (!this.state.firstlat) {
      this.setState({ firstlat: lat1, firstlng: lng1 });
    }

    if (this.counter > 5) {
      this.setState({ heading: this.props.coords.heading });
      this.counter = 0;
    } else {
      this.counter++;
    }
  };

  updateAddress = (lat1, lng1) => {
    axios
      .get(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
        lat1 +
        "," +
        lng1 +
        "&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk",
        {}
      )
      .then(response => {
        this.setState({
          logradouro: response.data.results[0].formatted_address,
          lat: lat1,
          lng: lng1
        });
      });
  };

  storeChoose = type => {
    localStorage.setItem("type", type);
    history.push("/app/map");
  };

  calculateFares = (distance, duration) => {
    if (this.state.vehicleType && this.state.source && this.state.target) {
      var query_params =
        "vehicleId=" +
        this.state.vehicleType +
        "&FromLatLong=" +
        this.state.source.lat +
        "," +
        this.state.source.lng +
        "&ToLatLong=" +
        this.state.target.lat +
        "," +
        this.state.target.lng;
      console.log(query_params);
      axios
        .post("/agenda/ajax_estimate_by_vehicle_type.php", query_params, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        })
        .then(res => {
          console.log(res.data);
          var basicFare =
            parseFloat(res.data.iBaseFare) +
            (parseFloat(res.data.fPricePerKM) / 1000) * distance +
            (parseFloat(res.data.fPricePerMin) / 60) * duration;

          axios
            .get(
              "/webservice_bepay.php?type=taxes&vehicleTypeID=" +
              this.state.vehicleType +
              "&tripDistance=" +
              parseFloat(distance / 1000) +
              "&real_total_fare=" +
              basicFare,
              {}
            )
            .then(res_tax => {
              console.log(res_tax.data);

              var taxesTotal = 0;
              var taxesNames = "";
              for (var i = 0; i < res_tax.data.length; i++) {
                taxesTotal += parseFloat(res_tax.data[i].fTotal);
                taxesNames += res_tax.data[i].vName + ",";
              }

              taxesNames += "@";
              taxesNames = taxesNames.replace(",@", "");

              var totalFare = taxesTotal + basicFare + parseFloat(this.vTaxes);

              console.log(totalFare);
              console.log(this.vTaxes);

              this.setState({
                fares: {
                  ...res.data,
                  ...{ taxesTotal },
                  ...{ taxesNames },
                  ...{ totalFare },
                  ...{ basicFare },
                  ...{ distance },
                  ...{ duration }
                },
                calculating: false,
                distance: distance,
                duration: duration
              });
            });
        });
    }
  };

  calculateRoute = () => {
    if (this.state.vehicleType && this.state.source && this.state.target) {
      this.setState({ calculating: true, fares: null });
      var request = {
        origin: this.state.source.lat + "," + this.state.source.lng, // From locations latlongs
        destination: this.state.target.lat + "," + this.state.target.lng, // To locations latlongs
        travelMode: this.maps.TravelMode.DRIVING // Set the Path of Driving
      };

      this.directionsService.route(request, (result, status) => {
        this.directionsDisplay.setMap(this.map);
        this.directionsDisplay.setOptions({ suppressMarkers: true }); //, preserveViewport: true, suppressMarkers: false for setting auto markers from google api
        this.directionsDisplay.setDirections(result);
        console.log(result);
        var route = result.routes[0];
        var distance = 0;
        var duration = 0;
        for (var i = 0; i < route.legs.length; i++) {
          console.log(route.legs[i].distance.value);
          console.log(route.legs[i].duration.value);
          distance = route.legs[i].distance.value;
          duration = route.legs[i].duration.value;
        }

        this.calculateFares(distance, duration);
      });
    }
  };

  populateDrivers = keyword => {
    axios
      .post(
        "/agenda/get_available_driver_list_json.php",
        "eLadiesRide=" +
        (this.state.elas ? "Yes" : "No") +
        "&iVehicleTypeId=" +
        this.state.vehicleType +
        "&keyword=" +
        keyword,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        var drivers = res.data;

        let driver_list = drivers.map((driver, i) => {


          var statusIcon;
          if (driver.vAvailability == "Available") {
            statusIcon = "../assets/img/green-icon.png";
          } else if (driver.vAvailability == "Active") {
            statusIcon = "../assets/img/red.png";
            return;
          } else if (driver.vAvailability == "On Going Trip") {
            statusIcon = "../assets/img/yellow.png";
            return;
          } else if (driver.vAvailability == "Arrived") {
            statusIcon = "../assets/img/blue.png";
            return;
          } else {
            statusIcon = "../assets/img/offline-icon.png";
            return;
          }

          driver.statusIcon = statusIcon;

          return (
            <li key={i}>
              <label class="map-tab-img">
                <label class="map-tab-img1">
                  <img src={driver.vImageDriver} />
                </label>
                <img src={statusIcon} />
              </label>
              <p>
                {driver.FULLNAME}
                <b></b>
              </p>
              <Button
                color="primary"
                onClick={e =>
                  this.setState({ driver_selected: driver, driverlist: null })
                }
              >
                Selecionar
              </Button>
            </li>
          );
        });

        this.setState({ driverlist: driver_list });
      });
  };

  populateCarTypes = admin_id => {
    axios
      .post("/agenda/ajax_booking_details_json.php?admin_id=" + admin_id, "", {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => {
        console.log(res.data);
        var cars = res.data;

        let cars_list = cars.map((car, i) => {
          return (
            <option value={car.iVehicleTypeId}>
              {car.vVehicleType + " - " + car.vLocationName}
            </option>
          );
        });

        if (cars != null && cars.length > 0) {
          this.setState({
            cars_list: cars_list,
            vehicleType: cars[0].iVehicleTypeId
          });
        }
      });
  };

  triggerSeachEmail = (email) => {
    const self = this;

    if (self.state.typingTimeout) {
      clearTimeout(self.state.typingTimeout);
    }

    self.setState({

      typingTimeout: setTimeout(function () {
        self.findUserByEmail(email);
      }, 1000)
    });
  }


  triggerSeachPhone = (phone) => {
    const self = this;

    if (self.state.typingTimeout) {
      clearTimeout(self.state.typingTimeout);
    }

    self.setState({

      typingTimeout: setTimeout(function () {
        self.findUserByPhone(phone);
      }, 1000)
    });
  }

  findUserByPhone = phone => {
    phone = phone
      .replace(/\./g, "")
      .replace(/\-/g, "")
      .replace(/\_/g, "")
      .replace(/\ /g, "")
      .replace(/\(/g, "")
      .replace(/\)/g, "");
    console.log(phone);
    console.log(phone.length);

    this.setState({ finding_user: true });

    axios
      .post(
        "/agenda/ajax_find_rider_by_number.php",
        "phone=" + phone + "&phonecode=55",
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(res => {
        console.log("@" + res.data + "@");
        console.log("@" + res.data.length + "@");
        if (res.data && res.data.length > 0) {
          var parts = res.data.split(":");
          this.setState({
            name: parts[0],
            surname: parts[1],
            email: parts[2],
            finding_user: false
          });
        } else {
          this.setState({
            name: "",
            surname: "",
            email: "",
            finding_user: false
          });
        }
      })
      .catch(error => {
        this.setState({ finding_user: false });
      });
  };

  findUserByEmail = email => {

    this.setState({ finding_user: true });

    axios
      .post(
        "/agenda/ajax_find_rider_by_number.php",
        "email=" + email,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(res => {
        console.log("@" + res.data + "@");
        console.log("@" + res.data.length + "@");
        if (res.data && res.data.length > 0) {
          var parts = res.data.split(":");
          this.setState({
            name: parts[0],
            surname: parts[1],
            email: parts[2],
            phone: parts[5],
            finding_user: false
          });
        } else {
          this.setState({
        
            finding_user: false
          });
        }
      })
      .catch(error => {
        this.setState({ finding_user: false });
      });
  };

  doRender = async () => {
    var _map = this.map;
    var _maps = this.maps;

    console.log(this.state.vehicleType);

    axios
      .post(
        "/agenda/get_map_drivers_list.php",
        "eLadiesRide=" +
        (this.state.elas ? "Yes" : "No") +
        "&iVehicleTypeId=" +
        this.state.vehicleType,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        if (res.data) {
          var locations = res.data.locations;
          var map_center = null;

          var infowindow = new _maps.InfoWindow();
          for (var i = 0; i < locations.length; i++) {
            var location = locations[i];

            if (
              location.location_carType != null &&
              !location.location_carType.includes(this.state.vehicleType)
            ) {
              continue;
            }

            if (location.location_type != "Available") {
              continue;
            }

            var point = {
              lat: parseFloat(location.google_map.lat),
              lng: parseFloat(location.google_map.lng)
            };

            if (map_center == null) {
              map_center = point;
            }

            let marker = new this.maps.Marker({
              position: point,
              map: _map,
              icon: location.location_icon
            });

            var newName = locations[i].location_name;
            var newOnlineSt = locations[i].location_online_status;
            var newLat = locations[i].google_map.lat;
            var newLong = locations[i].google_map.lng;
            var newDriverImg = locations[i].location_image;
            var newMobile = locations[i].location_mobile;
            var newDriverID = locations[i].location_ID;
            var newImg = locations[i].location_icon;

            var content =
              '<table><tr><td rowspan="4"><img src="' +
              newDriverImg +
              '" height="60" width="60"></td></tr><tr><td>&nbsp;&nbsp;Nome: </td><td><b>' +
              newName +
              "</b></td></tr><tr><td></td><td><b></b></td></tr></table>";

            _maps.event.addListener(
              marker,
              "click",
              (function (marker, content, infowindow) {
                return function () {
                  infowindow.setContent(content);
                  infowindow.open(_map, marker);
                };
              })(marker, content, infowindow)
            );
          }

          if (map_center != null) {
            _map.panTo(map_center);
          }
        }
      });
    this.calculateRoute();
  };

  renderMarkers = (map, maps) => {
    this.map = map;
    this.maps = maps;
    this.doRender();
    var directionsOptions = {
      // For Polyline Route line options on map
      polylineOptions: {
        strokeColor: "#FF7E00",
        strokeWeight: 5
      }
    };
    this.directionsService = new maps.DirectionsService(directionsOptions);
    this.directionsDisplay = new maps.DirectionsRenderer(directionsOptions);
    return null;
  };

  setPinMarker = (origin, coords) => {
    var icon = "";

    if (origin == "source") {
      icon = "https://colabora.mobi/webimages/upload/mapmarker/PinFrom.png";
      if (this.pin_source != null) {
        this.pin_source.setMap(null);
      }
    }

    if (origin == "target") {
      icon = "https://colabora.mobi/webimages/upload/mapmarker/PinTo.png";
      if (this.pin_target != null) {
        this.pin_target.setMap(null);
      }
    }

    let marker = new this.maps.Marker({
      position: coords,
      map: this.map,
      icon: icon,
      animation: this.maps.Animation.DROP,
      draggable: true
    });

    if (origin == "source") {
      this.pin_source = marker;
    }

    if (origin == "target") {
      this.pin_target = marker;
    }

    marker.addListener("dragend", function (event) {
      // console.log(event);
      var lat = event.latLng.lat();
      var lng = event.latLng.lng();
      console.log(lat);
      console.log(lng);

      this.geoDecode(lat, lng, result => { });
    });
    this.map.setCenter(coords);
  };

  doSubmit = now => {
    if (now) {
      this.setState({ submittingnow: true });
    } else {
      this.setState({ submitting: true });
    }
    if (this.state.email == null || this.state.email.length == 0) {
      this.setState({
        showerror: true,
        messageerror: "Selecione um usuario válido",
        submitting: false
      });
      return;
    }

    if (this.state.source == null || this.state.target == null) {
      this.setState({
        showerror: true,
        messageerror: "Selecione origem e destino",
        submitting: false
      });
      return;
    }

    var query_params =
      "vEmail=" +
      this.state.email +
      "&vName=" +
      this.state.name +
      "&vLastName=" +
      this.state.surname +
      "&iVehicleTypeId=" +
      this.state.vehicleType +
      "&from_lat_long=" +
      this.state.source.lat +
      "," +
      this.state.source.lng +
      "&to_lat_long=" +
      this.state.target.lat +
      "," +
      this.state.target.lng +
      "&distance=" +
      this.state.distance +
      "&duration=" +
      this.state.duration +
      "&vEmail=" +
      this.state.email +
      "&vSourceAddresss=" +
      this.state.sourceAddress +

      "&admin_id=" +
      this.admin_id +

      "&central_taxes=" +
      this.vTaxes +
      "&tDestAddress=" +
      this.state.targetAddress +
      "&eType=Ride" +
      "&dBooking_date=" +
      (now
        ? moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
        : moment(this.state.bookdate).format("YYYY-MM-DD HH:mm:ss")) +
      (this.state.driver_selected
        ? "&iDriverId=" + this.state.driver_selected.iDriverId
        : "") +
      "&eAutoAssign=" +
      (this.state.driver_type == "driver_any" ? "Yes" : "No") +
      "&eFemaleDriverRequest=" +
      (this.state.elas ? "Yes" : "No");

    console.log(query_params);
    axios
      .post("/agenda/my_action_booking.php", query_params, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => {
        console.log(res.data);

        this.setState({ submitting: false, submittingnow: false });
        if (res.data.success == "1") {
          history.push("/app/bookinglist");
        } else {
          this.setState({ showerror: true, messageerror: res.data.var_msg });
        }
      })
      .catch(error => {
        this.setState({ submitting: false, submittingnow: false });
      });
  };

  checkUserBalance = driverID => {
    console.log(driverID);
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Formik
          enableReinitialize
          initialValues={{}}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
              <Row
                id="login_body"
                className="login_app flex-row align-items-center"
              >
                <Col>
                  <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                    <Card>
                      <CardHeader>Dados da Corrida</CardHeader>
                      <CardBody>
                        <CardGroup
                          style={{
                            background: "rgba(233, 84, 39, 0.02)",
                            borderRadius: "5px",
                            border: "1px solid #E95327"
                          }}
                        >
                          <Card className="m-4">
                            <Label className="font-weight-bold">
                              Dados do Passageiro
                          </Label>
                            <Row className="mt-2">
                              <Col sm={{ size: "3" }}>
                                <Input
                                  placeholder={"Busca por telefone"}
                                  invalid={touched.phone && !!errors.phone}
                                  type="tel"
                                  name="phone"
                                  id="phone"
                                  onChange={e => {
                                    handleChange(e);
                                    this.triggerSeachPhone(e.target.value);
                                  }}
                                  onBlur={handleBlur}
                                  value={this.state.phone}
                                  mask="(99)99999-9999"
                                  maskChar=" "
                                  tag={InputMask}
                                />
                                <FormFeedback>{errors.phone}</FormFeedback>
                              </Col>

                              <Col sm={{ size: "3" }}>
                                <Input
                                  placeholder={"Nome"}
                                  invalid={
                                    touched.firstname && !!errors.firstname
                                  }
                                  name="login"
                                  type="text"
                                  autoComplete="username"
                                  value={this.state.name}
                                  onChange={e => {
                                    this.setState({ name: e.target.value });
                                  }}
                                  onBlur={handleBlur}
                                />
                                <FormFeedback>{errors.firstname}</FormFeedback>
                              </Col>
                              <Col sm={{ size: "3" }}>
                                <Input
                                  placeholder={"Sobrenome"}
                                  invalid={touched.surname && !!errors.surname}
                                  type="text"
                                  name="surname"
                                  id="surname"
                                  onChange={e => {
                                    this.setState({ surname: e.target.value });
                                  }}
                                  onBlur={handleBlur}
                                  value={this.state.surname}
                                />
                                <FormFeedback>{errors.surname}</FormFeedback>
                              </Col>

                              <Col sm={{ size: "3" }}>
                                <Input
                                  placeholder={"Busca por Email"}
                                  invalid={touched.email && !!errors.email}
                                  type="text"
                                  name="email"
                                  id="email"
                                  onChange={e => {
                                    this.setState({
                                      email: e.target
                                        .value
                                    });
                                    this.triggerSeachEmail(e.target.value);
                                  }}
                                  onBlur={handleBlur}
                                  value={this.state.email}
                                />
                              </Col>
                            </Row>
                            <Row>
                              {this.state.finding_user ? (
                                <Col>
                                  <ReactLoading
                                    className="col-md-5"
                                    type={"bubbles"}
                                    color={"#E95327"}
                                  />
                                </Col>
                              ) : (
                                  <div />
                                )}
                            </Row>
                          </Card>
                        </CardGroup>

                        <CardGroup
                          style={{
                            background: "rgba(233, 84, 39, 0.02)",
                            borderRadius: "5px",
                            border: "1px solid #E95327",
                            marginTop: "10px"
                          }}
                        >
                          <Card className="m-4">
                            <Label className="mb-3 font-weight-bold">
                              Dados da Corrida
                          </Label>
                            <GooglePlacesAutocomplete
                              className="col-md-2"
                              placeholder="Ponto de Inicio"
                              autocompletionRequest={{
                                componentRestrictions: {
                                  country: "br"
                                }
                              }}
                              onSelect={e => {
                                geocodeByAddress(e.description)
                                  .then(results => getLatLng(results[0]))
                                  .then(coords => {
                                    console.log(coords);
                                    this.setState({
                                      source: coords,
                                      sourceAddress: e.description
                                    });
                                    this.setPinMarker("source", coords);
                                    this.calculateRoute();
                                  });
                              }}
                            />

                            <GooglePlacesAutocomplete
                              currentLocation={true}
                              style={{ marginTop: "10px" }}
                              className="col-md-2"
                              placeholder="Ponto de Fim"
                              autocompletionRequest={{
                                componentRestrictions: {
                                  country: "br"
                                },
                                location: this.state.source,
                                radius: 1000
                              }}
                              styles={{
                                marginTop: "10px",
                                textInputContainer: {
                                  width: "300px"
                                }
                              }}
                              onSelect={e => {
                                console.log(e);

                                geocodeByPlaceId(e.place_id)
                                  .then(results => console.log(results))
                                  .catch(error => console.error(error));

                                geocodeByAddress(e.description)
                                  .then(results => getLatLng(results[0]))
                                  .then(coords => {
                                    console.log(coords);
                                    this.setState({
                                      target: coords,
                                      targetAddress: e.description
                                    });
                                    this.setPinMarker("target", coords);
                                    this.calculateRoute();
                                  });
                              }}
                            />
                            <Row
                              className="mt-2"
                              style={{ marginLeft: "5px", marginRight: "8px" }}
                            >
                              <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <Input
                                      addon
                                      type="checkbox"
                                      checked={this.state.elas}
                                      onClick={() =>
                                        this.setState({ elas: !this.state.elas })
                                      }
                                      aria-label="Checkbox for following text input"
                                    />
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Label className="font-weight-bold m-1">
                                  Programa ELAS
                              </Label>
                              </InputGroup>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <select
                                  onChange={e => {
                                    this.setState(
                                      { vehicleType: e.target.value },
                                      () => {
                                        this.calculateRoute();
                                        this.doRender();
                                      }
                                    );
                                  }}
                                  class="col-md-4 form-control form-control-select"
                                >
                                  {this.state.cars_list}
                                </select>
                                <h1></h1>
                              </Col>
                            </Row>
                          </Card>
                        </CardGroup>
                        {this.state.fares ? (
                          <CardGroup
                            style={{
                              background: "rgba(233, 84, 39, 0.02)",
                              borderRadius: "5px",
                              border: "1px solid #E95327",
                              marginTop: "10px"
                            }}
                          >
                            <Card className="m-4">
                              <Label className="mb-2 font-weight-bold">
                                Valores da Corrida
                            </Label>
                              <Table
                                borderless
                                className="col-md-4"
                                style={{ margin: "0 auto" }}
                              >
                                <tbody>
                                  <tr>
                                    <td>
                                      <Label className="mb-2 font-weight-bold">
                                        Tarifa Base
                                    </Label>
                                    </td>
                                    <td>
                                      <CurrencyFormat
                                        value={this.state.fares.iBaseFare}
                                        renderText={value => (
                                          <div align="right">{value}</div>
                                        )}
                                        displayType={"text"}
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        thousandSeparator={true}
                                        prefix={"R$"}
                                      />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                      <Label className="mb-2 font-weight-bold">
                                        Tarifa KM (
                                      {this.state.fares.distance / 1000} Km)
                                    </Label>
                                    </td>
                                    <td>
                                      <CurrencyFormat
                                        value={
                                          (this.state.fares.fPricePerKM *
                                            this.state.fares.distance) /
                                          1000
                                        }
                                        renderText={value => (
                                          <div align="right">{value}</div>
                                        )}
                                        displayType={"text"}
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        thousandSeparator={true}
                                        prefix={"R$"}
                                      />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                      <Label className="mb-2 font-weight-bold">
                                        Tarifa Tempo (
                                      {parseFloat(
                                        this.state.fares.duration / 60
                                      ).toFixed(2)}{" "}
                                      min)
                                    </Label>
                                    </td>
                                    <td>
                                      <CurrencyFormat
                                        value={parseFloat(
                                          (this.state.fares.fPricePerMin *
                                            this.state.fares.duration) /
                                          60
                                        ).toFixed(2)}
                                        renderText={value => (
                                          <div align="right">{value}</div>
                                        )}
                                        displayType={"text"}
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        thousandSeparator={true}
                                        prefix={"R$"}
                                      />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                      <Label className="mb-2 font-weight-bold">
                                        Taxas
                                    </Label>
                                    </td>
                                    <td>
                                      <CurrencyFormat
                                        value={parseFloat(
                                          this.state.fares.taxesTotal
                                        ).toFixed(2)}
                                        renderText={value => (
                                          <div align="right">{value}</div>
                                        )}
                                        displayType={"text"}
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        thousandSeparator={true}
                                        prefix={"R$"}
                                      />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                      <Label className="mb-2 font-weight-bold">
                                        Tarifa Central
                                    </Label>
                                    </td>
                                    <td>
                                      <CurrencyFormat
                                        value={this.vTaxes}
                                        renderText={value => (
                                          <div align="right">{value}</div>
                                        )}
                                        displayType={"text"}
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        thousandSeparator={true}
                                        prefix={"R$"}
                                      />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                      <h3
                                        style={{
                                          color: "#E95327",
                                          fontWweight: "bold"
                                        }}
                                      >
                                        Valor Total
                                    </h3>
                                    </td>
                                    <td>
                                      <CurrencyFormat
                                        value={this.state.fares.totalFare}
                                        displayType={"text"}
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        thousandSeparator={true}
                                        prefix={"R$"}
                                        renderText={value => (
                                          <div
                                            align="right"
                                            className="font-weight-bold"
                                          >
                                            <h3 style={{ color: "#E95327" }}>
                                              {value}
                                            </h3>
                                          </div>
                                        )}
                                      />
                                    </td>
                                  </tr>
                                </tbody>
                              </Table>
                            </Card>
                          </CardGroup>
                        ) : (
                            <Row>
                              {this.state.calculating ? (
                                <Col>
                                  <ReactLoading
                                    className="col-md-5"
                                    type={"bubbles"}
                                    color={"#E95327"}
                                  />
                                </Col>
                              ) : (
                                  <div />
                                )}
                            </Row>
                          )}

                        {this.state.fares ? (
                          <CardGroup
                            style={{
                              background: "rgba(233, 84, 39, 0.02)",
                              borderRadius: "5px",
                              border: "1px solid #E95327",
                              marginTop: "10px"
                            }}
                          >
                            <Card className="m-4">
                              <Label className="mb-2 font-weight-bold">
                                Escolher Motorista
                            </Label>

                              <Row className="mt-2">
                                <Col>
                                  <select
                                    onChange={e => {
                                      this.setState({
                                        driver_type: e.target.value
                                      });

                                      if (e.target.value == "driver_any") {
                                        this.setState({
                                          driver_selected: null
                                        });
                                      }
                                    }}
                                    class="col-md-4 form-control form-control-select"
                                  >
                                    <option value="driver_any">
                                      Qualquer Motorista
                                  </option>
                                    <option value="driver_assigned">
                                      Escolher Motorista
                                  </option>
                                  </select>
                                  <h1></h1>
                                </Col>
                              </Row>

                              {this.state.driver_selected ? (
                                <Col className="driverlist">
                                  <ul>
                                    <li>
                                      <label class="map-tab-img">
                                        <label class="map-tab-img1">
                                          <img
                                            src={
                                              this.state.driver_selected
                                                .vImageDriver
                                            }
                                          />
                                        </label>
                                        <img
                                          src={
                                            this.state.driver_selected.statusIcon
                                          }
                                        />
                                      </label>
                                      <p>
                                        {this.state.driver_selected.FULLNAME}
                                        <b></b>
                                      </p>
                                    </li>
                                  </ul>
                                </Col>
                              ) : (
                                  <div />
                                )}

                              {this.state.driver_type == "driver_assigned" ? (
                                <Row className="mt-2">
                                  <Col>
                                    <InputGroup>
                                      <Input
                                        className="col-md-4"
                                        placeholder={""}
                                        invalid={
                                          touched.search && !!errors.search
                                        }
                                        type="tel"
                                        name="search"
                                        id="search"
                                        onChange={e => {
                                          handleChange(e);
                                          this.populateDrivers(e.target.value);
                                        }}
                                        onBlur={handleBlur}
                                        value={values.search}
                                      />
                                      <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                          Procurar Motorista
                                      </InputGroupText>
                                      </InputGroupAddon>
                                    </InputGroup>
                                  </Col>
                                </Row>
                              ) : (
                                  <div />
                                )}
                              {this.state.driverlist ? (
                                <Row>
                                  <Col className="driverlist">
                                    <ul
                                      style={{ height: "300px", width: "100%" }}
                                    >
                                      {this.state.driverlist}
                                    </ul>
                                  </Col>
                                </Row>
                              ) : (
                                  <div />
                                )}
                            </Card>
                          </CardGroup>
                        ) : (
                            <div />
                          )}
                      </CardBody>
                    </Card>

                    <div className="row justify-content-center m-4">
                      <LaddaButton
                        style={{ marginTop: "5px", width: "80%" }}
                        loading={this.state.submitting}
                        className="btn btn-secondary btn-ladda"
                        onClick={e => {
                          this.setState({
                            booking_after: !this.state.booking_after
                          });
                        }}
                      >
                        <i color="danger" className="fa fa-calendar" />
                        {this.state.booking_after
                          ? " Cancelar"
                          : " Agendar para Depois"}
                      </LaddaButton>
                    </div>
                    {this.state.booking_after == true ? (
                      <CardGroup
                        style={{
                          background: "rgba(233, 84, 39, 0.02)",
                          borderRadius: "5px",
                          border: "1px solid #E95327",
                          margin: "20px"
                        }}
                      >
                        <Card className="m-4">
                          <Label className="mb-3 font-weight-bold">Data</Label>

                          <Row>
                            <div className="col-md-4">
                              <Datetime
                                placeholder="Data e hora da corrida"
                                value={this.state.bookdate}
                                locale="pr-BR"
                                timeFormat={"HH:mm:ss"}
                                onChange={e => {
                                  this.setState({ bookdate: e });
                                }}
                              />
                            </div>
                          </Row>
                        </Card>
                      </CardGroup>
                    ) : (
                        <div />
                      )}

                    <div className="row justify-content-center m-4">
                      <LaddaButton
                        style={{ marginTop: "5px", width: "80%" }}
                        loading={this.state.submittingnow}
                        className="btn btn-primary btn-ladda"
                        onClick={e => {
                          this.doSubmit(!this.state.booking_after);
                        }}
                      >
                        <i color="danger" className="fa fa-check" />
                        {this.state.booking_after ? "Agendar" : "Agende Agora"}
                      </LaddaButton>
                    </div>

                    <Card>
                      <Row className="m-3">
                        <Col>
                          <GoogleMapReact
                            style={{ width: "100%", height: "400px" }}
                            bootstrapURLKeys={{
                              key: "AIzaSyDbJurSiaNCKuRQmSz_0Nv4ELHav0-iGsI"
                            }}
                            defaultCenter={{
                              lat: this.state.center.lat,
                              lng: this.state.center.lng
                            }}
                            defaultZoom={11}
                            yesIWantToUseGoogleMapApiInternals
                            onGoogleApiLoaded={({ map, maps }) =>
                              this.renderMarkers(map, maps)
                            }
                          ></GoogleMapReact>
                        </Col>
                      </Row>
                    </Card>
                  </Form>
                </Col>
              </Row>
            )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
        >
          <ModalHeader toggle={this.toggleInfo}>Erro Agendamento</ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button
              color="danger"
              onClick={e => this.setState({ showerror: false })}
            >
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

const MapConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);

/*
export default geolocated({
  positionOptions: {
    enableHighAccuracy: true
  },
  watchPosition: true,
  userDecisionTimeout: 5000
})(MapConnect);*/

export default MapConnect;
