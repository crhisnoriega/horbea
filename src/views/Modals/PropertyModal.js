import React, { Component } from "react";

import {
  ButtonGroup,
  Collapse,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
}
from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditSurver } from "../../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../../history/history";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ItemType from "../ItemType";

import AsyncCreatableSelect from "react-select/async-creatable";
import AsyncSelect from "react-select/async";

import request from "sync-request";
import moment from "moment";

const empty = {
  name: ""
};

class PropertyModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: [],
      cSelected: [],
      itens: [],
      isOpen: false,
      isShow: this.props.isShow
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    axios
      .get("/property/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({});
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({});
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      }
      catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(values);
    axios
      .post("/property", values, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        setSubmitting(false);
        this.setState({ isOpen: false });
        this.componentDidMount();
      })
      .catch(error => {
        console.log(error);
        setSubmitting(false);
      });

    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/cadastro/register");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    }
    else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Vistoriador"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  loadItens = inputValue => {
    return axios
      .get("/environments/itens", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  passToCallback = e => {
    this.props.callback(e);
  };

  render() {
    var _init = this.state.init;

    return (
      <div style={{ width: "80%" }} className="animated fadeIn">
        <Modal
          style={{ width: "80%" }}
          isOpen={this.state.isShow}
          toggle={this.toggleInfo}
          className={"modal-primary modal-lg"}
        >
          <ModalHeader toggle={this.toggleInfo}>Imovel</ModalHeader>
          <ModalBody>
            <ButtonGroup>
              <Button
                color="primary"
                onClick={e => {
                  this.setState({ isOpen: !this.state.isOpen });
                }}
              >
                <i color="primary" className="fa fa-edit" /> Novo Imovel
              </Button>
            </ButtonGroup>

            <BootstrapTable
              data={this.state.table}
              version="4"
              bordered={false}
              hover
              pagination
              search
              options={{
                noDataText: "Sem Informações",
                onRowClick: this.passToCallback
              }}
              searchPlaceholder={"Busca"}
            >
            
             <TableHeaderColumn width="100" dataField="type" dataSort>
                Tipo
              </TableHeaderColumn>
              
              <TableHeaderColumn width="300" isKey dataField="street" dataSort>
                Logradouro
              </TableHeaderColumn>

              <TableHeaderColumn width="300" dataField="city" dataSort>
                Cidade
              </TableHeaderColumn>

              <TableHeaderColumn
                defaultSorted
                width="200"
                dataField="state"
                dataSort
              >
                Estado
              </TableHeaderColumn>
              <TableHeaderColumn
                width="150"
                dataField="button"
                dataFormat={this.buttonFormatter}
              />
            </BootstrapTable>

            <br />

            <Collapse isOpen={this.state.isOpen}>
              <Formik
                initialValues={this.state.init}
                enableReinitialize
                validate={this.myValidator(this.validationSchemaComplete)}
                onSubmit={this.handleSubmit}
                render={({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  isValid,
                  setTouched
                }) => (
                  <Container>
                    <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                      <Card>
                        <CardHeader>Dados do Imovel</CardHeader>
                        <CardBody>
                          <FormGroup className="col-md-8">
                            <Label className="green-label" for="type">
                              Tipo
                            </Label>
                            <Select
                              name="form-field-name2"
                              value={this.state.type}
                              options={[
                                {
                                  value: "none",
                                  label: "Selecione um expediente"
                                },
                                { value: "padrao", label: "Padrão" }
                              ]}
                              onChange={type => {
                                errors.type = "";
                                if (type == null) {
                                  type = "";
                                }
                                this.setState({ type });
                              }}
                              placeholder="Selecione"
                              theme={theme => ({
                                ...theme,
                                borderRadius: 0,
                                colors: {
                                  ...theme.colors,
                                  primary25: "white",
                                  primary: "#97B834"
                                }
                              })}
                            />

                            <Label
                              style={{ color: "#f63c3a", fontSize: "80%" }}
                            >
                              {errors.type}
                            </Label>
                          </FormGroup>

                          <Row form className="col-md-15">
                            <FormGroup className="col-3">
                              <Label className="green-label" for="cep">
                                CEP
                              </Label>
                              <Input
                                invalid={touched.cep && !!errors.cep}
                                type="text"
                                name="cep"
                                id="cep"
                                onChange={e => {
                                  handleChange(e);

                                  var cep = e.target.value;
                                  cep = cep.replace("-", "");
                                  cep = cep.replace(" ", "");

                                  if (cep.length == 8) {
                                    var res = request(
                                      "GET",
                                      "https://viacep.com.br/ws/" +
                                        cep +
                                        "/json/"
                                    );

                                    console.log(res);
                                    var add = JSON.parse(res.body);
                                    values.street = add.logradouro;
                                    values.bairro = add.bairro;
                                    values.city = add.localidade;
                                  }
                                }}
                                onBlur={handleBlur}
                                value={values.cep}
                                mask="99999-999"
                                maskChar=" "
                                tag={InputMask}
                              />
                              <FormFeedback>{errors.cep}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-6">
                              <Label className="green-label" for="street">
                                Logradouro
                              </Label>
                              <Input
                                invalid={touched.street && !!errors.street}
                                type="text"
                                name="street"
                                id="street"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.street}
                              />
                              <FormFeedback>{errors.street}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-2">
                              <Label className="green-label" for="number">
                                Numero
                              </Label>
                              <Input
                                invalid={touched.number && !!errors.number}
                                type="text"
                                name="number"
                                id="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.number}
                              />
                              <FormFeedback>{errors.number}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-3">
                              <Label className="green-label" for="complemento">
                                Complemento
                              </Label>
                              <Input
                                invalid={
                                  touched.complemento && !!errors.complemento
                                }
                                type="text"
                                name="complemento"
                                id="complemento"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.complemento}
                              />
                              <FormFeedback>{errors.complemento}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-3">
                              <Label className="green-label" for="floor">
                                Andar
                              </Label>
                              <Input
                                invalid={touched.floor && !!errors.floor}
                                type="text"
                                name="floor"
                                id="floor"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.floor}
                              />
                              <FormFeedback>{errors.floor}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-5">
                              <Label className="green-label" for="bairro">
                                Bairro
                              </Label>
                              <Input
                                invalid={touched.bairro && !!errors.bairro}
                                type="text"
                                name="bairro"
                                id="bairro"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.bairro}
                              />
                              <FormFeedback>{errors.bairro}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-5">
                              <Label className="green-label" for="city">
                                Cidade
                              </Label>
                              <Input
                                invalid={touched.city && !!errors.city}
                                type="text"
                                name="city"
                                id="city"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.city}
                              />
                              <FormFeedback>{errors.city}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-3">
                              <Label className="green-label" for="state">
                                Estado
                              </Label>
                              <Input
                                invalid={touched.state && !!errors.state}
                                type="text"
                                name="state"
                                id="state"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.state}
                              />
                              <FormFeedback>{errors.state}</FormFeedback>
                            </FormGroup>
                          </Row>
                        </CardBody>
                      </Card>

                      <div style={{ height: "100px", width: "100%" }}>
                        <Row className="justify-content-sm-start">
                          <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <LaddaButton
                              type="submit"
                              className="col-6 btn btn-primary btn-ladda"
                              loading={isSubmitting}
                              onClick={e => {
                                handleSubmit(e);
                                this.toggleBtn("expLeft");
                              }}
                            >
                              Confirmar
                            </LaddaButton>
                          </Col>
                        </Row>
                      </div>
                    </Form>
                  </Container>
                )}
              />
            </Collapse>
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => {
                if (this.state.isOpen) {
                  this.setState({ isOpen: false });
                } else {
                  this.setState({ isShow: false });
                }

                this.props.callback;
              }}
            >
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PropertyModal)
);
