import React, { Component } from "react";

import {
  ButtonGroup,
  Collapse,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditSurver } from "../../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../../history/history";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ItemType from "../ItemType";

import AsyncCreatableSelect from "react-select/async-creatable";
import AsyncSelect from "react-select/async";

import request from "sync-request";

const empty = {
  name: ""
};

class SurveyorModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: [],
      cSelected: [],
      itens: [],
      isOpen: false,
      isShow: this.props.isShow
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    axios
      .get("/surveyor/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });

    this.setState({
      itens: [
        {
          value: "arcondicionado",
          label: "Ar Condicionado",
          data: { id: "123" }
        },
        { value: "armario", label: "Armario", data: { id: "123" } }
      ]
    });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do ambiente")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do ambiente")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(this.state.cSelected);
    console.log(this.state.itens);

    axios
      .post("/environments", values, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        axios
          .post(
            "/environments/additem2/" + res.data.id,
            { itens: this.state.itens },
            {
              headers: {
                "Content-Type": "application/json"
              }
            }
          )
          .then(res => {
            console.log(res.data);

            setSubmitting(false);

            history.push("/app/environments/list");
          })
          .catch(error => {
            console.log(error);
            this.setState({
              showerror: true,
              messageerror: JSON.stringify(error)
            });
            setSubmitting(false);
          });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          showerror: true,
          messageerror: JSON.stringify(error)
        });
        setSubmitting(false);
      });

    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/cadastro/register");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Vistoriador"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  loadItens = inputValue => {
    return axios
      .get("/environments/itens", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  passToCallback = e => {
    this.props.callback(e);
  };

  render() {
    var _init = this.state.init;

    return (
      <div style={{ width: "80%" }} className="animated fadeIn">
        <Modal
          style={{ width: "80%" }}
          isOpen={this.state.isShow}
          toggle={this.toggleInfo}
          className={"modal-primary modal-lg"}
        >
          <ModalHeader toggle={this.toggleInfo}>{this.props.name}</ModalHeader>
          <ModalBody>
            <ButtonGroup>
              <Button
                color="primary"
                onClick={e => {
                  this.setState({ isOpen: !this.state.isOpen });
                }}
              >
                <i color="primary" className="fa fa-edit" /> Novo Cliente
              </Button>
            </ButtonGroup>

            <BootstrapTable
              data={this.state.table}
              version="4"
              bordered={false}
              hover
              pagination
              search
              options={{
                noDataText: "Sem Informações",
                onRowClick: this.passToCallback
              }}
              searchPlaceholder={"Busca"}
            >
              <TableHeaderColumn width="300" isKey dataField="name" dataSort>
                Nome
              </TableHeaderColumn>

              <TableHeaderColumn
                defaultSorted
                width="200"
                dataField="cpf"
                dataSort
                dataFormat={this.dateFormatter}
              >
                CPF
              </TableHeaderColumn>

              <TableHeaderColumn
                defaultSorted
                width="200"
                dataField="fone1"
                dataSort
                dataFormat={this.dateFormatter}
              >
                Telefone
              </TableHeaderColumn>
              <TableHeaderColumn
                defaultSorted
                width="200"
                dataField="email"
                dataSort
                dataFormat={this.dateFormatter}
              >
                Email
              </TableHeaderColumn>
              <TableHeaderColumn
                width="150"
                dataField="button"
                dataFormat={this.buttonFormatter}
              />
            </BootstrapTable>

            <br />

            <Collapse isOpen={this.state.isOpen}>
              <Formik
                initialValues={this.state.init}
                enableReinitialize
                validate={this.myValidator(this.validationSchemaComplete)}
                onSubmit={this.handleSubmit}
                render={({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  isValid,
                  setTouched
                }) => (
                  <Container>
                    <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                      <Card>
                        <CardHeader>Dados</CardHeader>
                        <CardBody>
                          <FormGroup>
                            <Label className="green-label" for="name">
                              Nome
                            </Label>
                            <Input
                              className="col-md-8"
                              invalid={touched.name && !!errors.name}
                              type="text"
                              name="name"
                              id="name"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.name}
                            />
                            <FormFeedback>{errors.name}</FormFeedback>
                          </FormGroup>

                          <Row form className="col-md-15">
                            <FormGroup className="col-md-4">
                              <Label className="green-label" for="cpf">
                                CPF (acesso)
                              </Label>
                              <Input
                                invalid={touched.cpf && !!errors.cpf}
                                type="text"
                                name="cpf"
                                id="cpf"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.cpf}
                                mask="999.999.999-99"
                                maskChar=" "
                                tag={InputMask}
                              />
                              <FormFeedback>{errors.cpf}</FormFeedback>
                            </FormGroup>
                            <FormGroup className="col-md-4">
                              <Label className="green-label" for="rg">
                                RG
                              </Label>
                              <Input
                                invalid={touched.rg && !!errors.rg}
                                type="text"
                                name="rg"
                                id="rg"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.rg}
                                mask="99.999.999-9"
                                maskChar=" "
                                tag={InputMask}
                              />
                              <FormFeedback>{errors.rg}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <FormGroup>
                            <Label className="green-label" for="orgao">
                              Orgao Emissor
                            </Label>
                            <Input
                              className="col-md-4"
                              invalid={touched.orgao && !!errors.orgao}
                              type="text"
                              name="orgao"
                              id="orgao"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.orgao}
                            />
                            <FormFeedback>{errors.orgao}</FormFeedback>
                          </FormGroup>

                          <Row form className="col-md-15">
                            <FormGroup className="col-md-3">
                              <Label className="green-label" for="sexo">
                                Sexo
                              </Label>
                              <Select
                                name="form-field-name2"
                                value={this.state.sexo}
                                options={[
                                  { value: "Homem", label: "Homem" },
                                  { value: "Mulher", label: "Mulher" }
                                ]}
                                onChange={sexo => {
                                  errors.sexo = "";
                                  if (sexo == null) {
                                    sexo = "";
                                  }
                                  this.setState({ sexo });
                                }}
                                placeholder="Selecione"
                                theme={theme => ({
                                  ...theme,
                                  borderRadius: 0,
                                  colors: {
                                    ...theme.colors,
                                    primary25: "white",
                                    primary: "#97B834"
                                  }
                                })}
                              />

                              <Label
                                style={{ color: "#f63c3a", fontSize: "80%" }}
                              >
                                {errors.sexo}
                              </Label>
                            </FormGroup>

                            <FormGroup>
                              <Label className="green-label" for="naturalidade">
                                Naturalidade
                              </Label>
                              <Input
                                className="col-md-10"
                                invalid={
                                  touched.naturalidade && !!errors.naturalidade
                                }
                                type="text"
                                name="naturalidade"
                                id="naturalidade"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.naturalidade}
                              />
                              <FormFeedback>{errors.naturalidade}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-md-4">
                              <Label
                                className="green-label"
                                for="nacionalidade"
                              >
                                Nacionalidade
                              </Label>
                              <Input
                                invalid={
                                  touched.nacionalidade &&
                                  !!errors.nacionalidade
                                }
                                type="text"
                                name="nacionalidade"
                                id="nacionalidade"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.nacionalidade}
                              />
                              <FormFeedback>
                                {errors.nacionalidade}
                              </FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-md-4">
                              <Label className="green-label" for="estadocivil">
                                Estado Civil
                              </Label>
                              <Input
                                invalid={
                                  touched.estadocivil && !!errors.estadocivil
                                }
                                type="text"
                                name="estadocivil"
                                id="estadocivil"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.estadocivil}
                              />
                              <FormFeedback>{errors.estadocivil}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <FormGroup>
                            <Label className="green-label" for="description">
                              Observação
                            </Label>
                            <Input
                              className="col-md-10"
                              invalid={
                                touched.description && !!errors.description
                              }
                              type="textarea"
                              name="description"
                              id="description"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.description}
                            />
                            <FormFeedback>{errors.description}</FormFeedback>
                          </FormGroup>
                        </CardBody>
                      </Card>

                      <Card>
                        <CardHeader>Endereço</CardHeader>
                        <CardBody>
                          <Row form className="col-md-15">
                            <FormGroup className="col-3">
                              <Label className="green-label" for="cep">
                                CEP
                              </Label>
                              <Input
                                invalid={touched.cep && !!errors.cep}
                                type="text"
                                name="cep"
                                id="cep"
                                onChange={e => {
                                  handleChange(e);

                                  var cep = e.target.value;
                                  cep = cep.replace("-", "");
                                  cep = cep.replace(" ", "");

                                  if (cep.length == 8) {
                                    var res = request(
                                      "GET",
                                      "https://viacep.com.br/ws/" +
                                        cep +
                                        "/json/"
                                    );

                                    console.log(res);
                                    var add = JSON.parse(res.body);
                                    values.street = add.logradouro;
                                    values.bairro = add.bairro;
                                    values.city = add.localidade;
                                  }
                                }}
                                onBlur={handleBlur}
                                value={values.cep}
                                mask="99999-999"
                                maskChar=" "
                                tag={InputMask}
                              />
                              <FormFeedback>{errors.cep}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-6">
                              <Label className="green-label" for="street">
                                Logradouro
                              </Label>
                              <Input
                                invalid={touched.street && !!errors.street}
                                type="text"
                                name="street"
                                id="street"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.street}
                              />
                              <FormFeedback>{errors.street}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-2">
                              <Label className="green-label" for="number">
                                Numero
                              </Label>
                              <Input
                                invalid={touched.number && !!errors.number}
                                type="text"
                                name="number"
                                id="number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.number}
                              />
                              <FormFeedback>{errors.number}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-3">
                              <Label className="green-label" for="complemento">
                                Complemento
                              </Label>
                              <Input
                                invalid={
                                  touched.complemento && !!errors.complemento
                                }
                                type="text"
                                name="complemento"
                                id="complemento"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.complemento}
                              />
                              <FormFeedback>{errors.complemento}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-5">
                              <Label className="green-label" for="bairro">
                                Bairro
                              </Label>
                              <Input
                                invalid={touched.bairro && !!errors.bairro}
                                type="text"
                                name="bairro"
                                id="bairro"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.bairro}
                              />
                              <FormFeedback>{errors.bairro}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-5">
                              <Label className="green-label" for="city">
                                Cidade
                              </Label>
                              <Input
                                invalid={touched.city && !!errors.city}
                                type="text"
                                name="city"
                                id="city"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.city}
                              />
                              <FormFeedback>{errors.city}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-3">
                              <Label className="green-label" for="state">
                                Estado
                              </Label>
                              <Input
                                invalid={touched.state && !!errors.state}
                                type="text"
                                name="state"
                                id="state"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.state}
                              />
                              <FormFeedback>{errors.state}</FormFeedback>
                            </FormGroup>
                          </Row>
                        </CardBody>
                      </Card>

                      <Card>
                        <CardHeader>Contato</CardHeader>
                        <CardBody>
                          <Row form className="col-md-15">
                            <FormGroup className="col-5">
                              <Label className="green-label" for="fone1">
                                Telefone 1
                              </Label>
                              <Input
                                invalid={touched.fone1 && !!errors.fone1}
                                type="text"
                                name="fone1"
                                id="fone1"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.fone1}
                                mask="(99) 99999-9999"
                                maskChar=" "
                                tag={InputMask}
                              />
                              <FormFeedback>{errors.fone1}</FormFeedback>
                            </FormGroup>

                            <FormGroup className="col-5">
                              <Label className="green-label" for="fone2">
                                Telefone 2
                              </Label>
                              <Input
                                invalid={touched.fone2 && !!errors.fone2}
                                type="text"
                                name="fone2"
                                id="fone2"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.fone2}
                                mask="(99) 99999-9999"
                                maskChar=" "
                                tag={InputMask}
                              />
                              <FormFeedback>{errors.fone2}</FormFeedback>
                            </FormGroup>
                          </Row>

                          <Row form className="col-md-15">
                            <FormGroup className="col-8">
                              <Label className="green-label" for="email">
                                Email
                              </Label>
                              <Input
                                invalid={touched.email && !!errors.email}
                                type="text"
                                name="email"
                                id="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                              />
                              <FormFeedback>{errors.email}</FormFeedback>
                            </FormGroup>
                          </Row>
                        </CardBody>
                      </Card>

                      <div style={{ height: "100px", width: "100%" }}>
                        <Row className="justify-content-sm-start">
                          <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <LaddaButton
                              type="submit"
                              className="col-6 btn btn-primary btn-ladda"
                              loading={isSubmitting}
                              onClick={e => {
                                handleSubmit(e);
                                this.toggleBtn("expLeft");
                              }}
                            >
                              Confirmar
                            </LaddaButton>
                          </Col>
                        </Row>
                      </div>
                    </Form>
                  </Container>
                )}
              />
            </Collapse>
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => {
                if (this.state.isOpen) {
                  this.setState({ isOpen: false });
                } else {
                  this.setState({ isShow: false });
                }

                this.props.callback(e);
              }}
            >
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SurveyorModal)
);
