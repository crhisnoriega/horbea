import React, { Component } from "react";
import {
  ButtonGroup,
  Button,
  FormGroup,
  Label,
  Input,
  Row,
  Card,
  CardBody,
  CardHeader,
  Col
} from "reactstrap";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";

import axios from "axios";

import { connect } from "react-redux";

import {
  doLogin,
  setEditUser,
  setEditAgency,
  setEditSurver
} from "../../redux/redux";

import history from "../../history/history";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { registerLocale, setDefaultLocale } from "react-datepicker";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import pt from "date-fns/locale/pt";

// select
import Select from "react-select";

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));

const currDate = new Date();
const currYear = currDate.getFullYear();
const currMonth = currDate.getMonth();

const events = [
  {
    title: "All Day Event very long title",
    allDay: true,
    timeslots: 20,
    start: new Date(currYear, currMonth, 0),
    end: new Date(currYear, currMonth, 1)
  },
  {
    title: "Long Event",
    start: new Date(currYear, currMonth, 7),
    end: new Date(currYear, currMonth, 10)
  }
];

// todo: reactive custom calendar toolbar component

class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      startDate: new Date(),
      endDate: new Date(),
      imobiliarias: { value: "all", label: "Todos" },
      tipo_servico: { value: "all", label: "Todos" },
      vistoriador: { value: "all", label: "Todos" },
      situacao: { value: "all", label: "Todas" },
      issub: false,
      listimobiliarias: [],
      listvistoriadores: []
    };
  }

  handleSelect = event => {
    console.log(event.data);
    this.props.setEditSurver(event.data);
    history.push("/app/surver/create");
  };

  componentDidMount() {
    this.setState({ show: true });

    axios
      .get("/surveyor/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        if (res.data) {
          var list = [];
          res.data.forEach(element => {
            list.push({ value: element.id, label: element.name });
          });
          this.setState({ listvistoriadores: list });
        }
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("/agency/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        if (res.data) {
          var list = [];
          res.data.forEach(element => {
            list.push({ value: element.id, label: element.name });
          });
          this.setState({ listimobiliarias: list });
        }
      })
      .catch(error => {
        console.log(error);
      });

    this.doSearch();
  }

  doSearch = () => {
    this.setState({ issub: true });

    axios
      .get("/surver/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);

        var filterlist = res.data.filter(item => {
          var d1 = moment(this.state.startDate);
          var d2 = moment(this.state.endDate);
          var current = moment(item.createdAt);

          console.log(current.isAfter(d1));

          return (
            current.isAfter(d1) &&
            d2.isAfter(current) &&
            (this.state.situacao.value == "1" ||
              this.state.situacao.value == "all")
          );
        });

        var tmp = [];
        for (var index = 0; index < filterlist.length; index++) {
          var surver = filterlist[index];
          tmp.push({
            title:
              (surver.agency ? surver.agency.label : "") +
              "\n" +
              surver.tipo_servico +
              " " +
              (surver.vistoriador ? surver.vistoriador.name : ""),
            allDay: true,
            content: surver.vistoriador ? surver.vistoriador.name : "",
            start: surver.createdAt,
            end: surver.createdAt,
            data: surver
          });
        }

        this.setState({ events: tmp, issub: false });
      })
      .catch(error => {
        console.log(error);
        this.setState({ events: [], issub: false });
      });
  };

  Event = ({ event }) => {
    var title = event.title.substring(0, 10);
    var title2 = event.title.substring(10, 20);

    return (
      <div style={{ whiteSpace: "normal" }}>
        {title}
        {title2}
      </div>
    );
  };

  EventAgenda = ({ event }) => {
    var title = event.title.substring(0, 10);
    var title2 = event.title.substring(10, 20);

    return (
      <div style={{ whiteSpace: "normal" }}>
        {title}
        {title2}
      </div>
    );
  };

  onSelectDay = () => {
    alert("day");
  };

  render() {
    var errors = {};
    return (
      <div className="animated">
        <Card>
          <CardHeader>Calendario</CardHeader>
          <CardBody>
            <ButtonGroup>
              <Button
                style={{ width: "200px" }}
                color="primary"
                onClick={e => {
                  history.push("/app/surver/create");
                }}
              >
                <i color="primary" className="fa fa-plus" />
                Criar Vistoria
              </Button>
            </ButtonGroup>
            <ButtonGroup>
              <Button
                style={{ width: "200px", marginLeft: "10px" }}
                color="primary"
                onClick={e => {
                  history.push("/app/surver/list");
                }}
              >
                <i color="primary" className="fa fa-edit" />
                Lista
              </Button>
            </ButtonGroup>

            <Row form className="col-md-15" style={{ marginTop: "30px" }}>
              <FormGroup className="col-md-2">
                <Label className="green-label" for="imobiliarias">
                  Imobiliárias
                </Label>
                <Select
                  name="form-field-name2"
                  value={this.state.imobiliarias}
                  options={this.state.listimobiliarias}
                  onChange={imobiliarias => {
                    errors.imobiliarias = "";
                    if (imobiliarias == null) {
                      imobiliarias = "";
                    }
                    this.setState({ imobiliarias });
                  }}
                  placeholder="Selecionar"
                  theme={theme => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                      ...theme.colors,
                      primary25: "white",
                      primary: "#97B834"
                    }
                  })}
                />

                <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                  {errors.deviceReadonly}
                </Label>
              </FormGroup>

              <FormGroup className="col-md-2">
                <Label className="green-label" for="tipo_servico">
                  Tipo de Serviço
                </Label>
                <Select
                  name="form-field-name2"
                  value={this.state.tipo_servico}
                  options={[
                    { value: "all", label: "Todos" },
                    {
                      value: "vistoria_entrada",
                      label: "Vistoria de Entrada"
                    }
                  ]}
                  onChange={tipo_servico => {
                    errors.tipo_servico = "";
                    if (tipo_servico == null) {
                      tipo_servico = "";
                    }
                    this.setState({ tipo_servico });
                  }}
                  placeholder="Selecionar"
                  theme={theme => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                      ...theme.colors,
                      primary25: "white",
                      primary: "#97B834"
                    }
                  })}
                />

                <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                  {errors.deviceReadonly}
                </Label>
              </FormGroup>

              <FormGroup className="col-md-2">
                <Label className="green-label" for="vistoriador">
                  Vistoriador
                </Label>
                <Select
                  name="form-field-name2"
                  value={this.state.vistoriador}
                  options={this.state.listvistoriadores}
                  onChange={vistoriador => {
                    errors.vistoriador = "";
                    if (vistoriador == null) {
                      vistoriador = "";
                    }
                    this.setState({ vistoriador });
                  }}
                  placeholder="Selecionar"
                  theme={theme => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                      ...theme.colors,
                      primary25: "white",
                      primary: "#97B834"
                    }
                  })}
                />

                <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                  {errors.deviceReadonly}
                </Label>
              </FormGroup>

              <FormGroup className="col-md-3">
                <Label className="green-label" for="situacao">
                  Situação
                </Label>
                <Select
                  name="form-field-name2"
                  value={this.state.situacao}
                  options={[
                    { value: "all", label: "Todas" },
                    {
                      value: "1",
                      label: "Aguardando atribuir ao vistoriador"
                    },
                    { value: "2", label: "Atribuido ao vistoriador" },
                    { value: "3", label: "Em posse do vistoriador" },
                    { value: "4", label: "Transmitido" }
                  ]}
                  onChange={situacao => {
                    errors.situacao = "";
                    if (situacao == null) {
                      situacao = "";
                    }
                    this.setState({ situacao });
                  }}
                  placeholder="Selecionar"
                  theme={theme => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                      ...theme.colors,
                      primary25: "white",
                      primary: "#97B834"
                    }
                  })}
                />

                <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                  {errors.deviceReadonly}
                </Label>
              </FormGroup>

              <FormGroup className="col-md-3">
                <Col md="4">
                  <Label htmlFor="data_inicio">Inicio</Label>
                </Col>
                <Col xs="15" md="11">
                  <DatePicker
                    className="form-control"
                    selected={this.state.startDate}
                    onChange={e => {
                      this.setState({ startDate: e });
                    }}
                    locale="br"
                  />
                </Col>

                <Col md="4">
                  <Label htmlFor="data_termino">Término</Label>
                </Col>
                <Col xs="15" md="11">
                  <DatePicker
                    className="form-control"
                    selected={this.state.endDate}
                    onChange={e => {
                      this.setState({ endDate: e });
                    }}
                    locale="br"
                  />

                  <LaddaButton
                    type="submit"
                    className="col-8 btn btn-primary btn-ladda"
                    loading={this.state.issub}
                    onClick={e => {
                      this.doSearch();
                    }}
                  >
                    <i color="primary" className="fa fa-search" /> Procurar
                  </LaddaButton>
                </Col>
              </FormGroup>

              <BigCalendar
                selectable
                style={{ height: "100vh", width: "100%" }}
                className="d-sm-down-none"
                {...this.props}
                events={this.state.events}
                views={["month", "week", "day"]}
                step={30}
                defaultDate={new Date(currYear, currMonth, 1)}
                defaultView="month"
                toolbar={true}
                messages={{
                  month: "Mes",
                  day: "Dia",
                  today: "Hoje",
                  week: "Semana",
                  previous: "Anterior",
                  next: "Próximo"
                }}
                onSelectSlot={e => {
                  console.log(e);
                  localStorage.setItem("day", e.start);
                  history.push("/app/surver/create");
                }}
                onSelectEvent={e => {
                  this.handleSelect(e);
                }}
                components={{
                  event: this.Event,
                  agenda: {
                    event: this.EventAgenda
                  }
                }}
              />
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency,
  setEditSurver
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Calendar);
