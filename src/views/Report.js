import React, { Component } from "react";

import { CardHeader, CardBody, Card, Button, Row, Col } from "reactstrap";

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

import { connect } from "react-redux";
import { doLogin, setEditUser } from "../redux/redux";




import { Editor } from "@tinymce/tinymce-react";


class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true
    };
  }

  componentDidMount() {}

  onShow = () => {
    this.setState({ show: true });
  };

  onHide = () => {
    this.setState({ show: false });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>Cabeçalho</CardHeader>
          <CardBody>
            <Editor
              apiKey='os47siri7k3mj29ahpltwa6mtm24jd07cymunljr1ofbdgkz'
              initialValue=""
              init={{
                language_url: "http://54.186.182.55:3002/v1/language/pt_BR.js",
                language: 'pt_BR',
                plugins: "link image code"
               
              }}
              onChange={this.handleEditorChange}
            />
          </CardBody>
        </Card>

        <Card>
          <CardHeader>Mensagens</CardHeader>
          <CardBody>
            <h5>Mensagem Inicial</h5>
            <Editor
            apiKey='os47siri7k3mj29ahpltwa6mtm24jd07cymunljr1ofbdgkz'
              initialValue=""
              init={{
                plugins: "link image code",
                toolbar:
                  "undo redo | bold italic | alignleft aligncenter alignright | code"
              }}
              onChange={this.handleEditorChange}
            />

            <h5>Mensagem Inicial</h5>
            <Editor
            apiKey='os47siri7k3mj29ahpltwa6mtm24jd07cymunljr1ofbdgkz'
              initialValue=""
              init={{
                plugins: "link image code",
                toolbar:
                  "undo redo | bold italic | alignleft aligncenter alignright | code"
              }}
              onChange={this.handleEditorChange}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);
