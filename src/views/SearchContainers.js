import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
}
from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditAgency } from "../redux/redux";

import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";
import request from "sync-request";


import { geolocated } from "react-geolocated";

const empty = {
  name: "",
  cnpj: "",
  socialname: "",
  cep: "",
  street: "",
  number: "",
  complemento: "",
  bairro: "",
  city: "",
  state: "",
  fone1: "",
  email: ""
};

class SeachContainers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      type: { value: "none", label: "Selecione um expediente" },
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      cep: null,
      logradouro: null,
      numero: null
    };
  }

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  componentDidMount() {
    if (this.props.session.edit_agency != null) {
      console.log(this.props.session.edit_agency);
      this.setState({
        init: this.props.session.edit_agency,
        isEdit: true,
        image: this.props.session.edit_agency.photo
      });
    }
    this.setState({ show: true });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome da imobiliaria"),
      cnpj: Yup.string().required("CNPJ"),
      socialname: Yup.string().required("razao social obrigatoria"),
      cep: Yup.string().required("CEP obrigatorio"),
      street: Yup.string().required("logradouro obrigatorio"),
      number: Yup.string().required("numero obrigatorio"),
      bairro: Yup.string().required("bairro obrigatorio"),
      city: Yup.string().required("cidade obrigatorio"),
      state: Yup.string().required("estado obrigatorio"),
      fone1: Yup.string().required("telefone"),
      email: Yup.string()
        .email("email invalido")
        .required("email")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      }
      catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  geoDecode = (lat, lng) => {
    axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk', {})
      .then(response => {
        this.setState({ logradouro: JSON.stringify(response.data.results[0].formatted_address) })
      })
  }

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    values.photo = this.state.image;

    if (this.state.isEdit) {
      axios
        .put("/agency/" + this.props.session.edit_agency.id, values, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          console.log(res.data);

          this.props.setEditAgency(null);

          this.setState({ init: empty, showinfo: true });
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    }
    else {
      axios
        .post("/agency", values, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          console.log(res.data);
          this.setState({ init: empty, showinfo: true });
          setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    }
    return;
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  handleCloseError = e => {
    this.setState({ show: false });
  };

  handleCloseInfo = e => {
    history.push("/app/agency/list");
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      console.log(fileInfo);
      this.setState({ image: fileInfo.base64 });
    }; // reader.onload
  };


  render() {
    var _init = this.state.init;

    console.log(_init);

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />
<Card>
                  <CardHeader>Verificar Caçambas</CardHeader>
                  <CardBody>
                    <FormGroup>
                        <Label className="green-label" for="cep">
                          CEP
                        </Label>
                        <Input
                         className="col-md-2"
                          value={this.state.cep}
                          type="text"
                          name="cep"
                          id="cep"
                          onChange={e => {
                          
                            var cep = e.target.value;
                            cep = cep.replace("-", "");
                            cep = cep.replace(" ", "");

                            if (cep.length == 8) {
                              var res = request(
                                "GET",
                                "https://viacep.com.br/ws/" + cep + "/json/"
                              );

                              console.log(res);
                              var add = JSON.parse(res.body);
                              this.setState({logradouro:add.logradouro});
                            }
                          }}
                        
                          mask="99999-999"
                          maskChar=" "
                          tag={InputMask}
                        />
                       
                      </FormGroup>

                      <FormGroup>
                        <Label className="green-label" for="street">
                          Logradouro
                        </Label>
                        <Input
                          className="col-md-8"
                          value={this.state.logradouro}
                          type="text"
                          name="street"
                          id="street"
                        
                        />
                       
                      </FormGroup>
                      
                      <FormGroup >
                        <Label className="green-label" for="number">
                          Numero
                        </Label>
                        <Input
                          className="col-md-2"
                         
                          type="text"
                          name="number"
                          id="number"
                         
                        />
                     
                      </FormGroup>

                    
                  </CardBody>
                </Card>

               
                  <div class="row justify-content-center">

                      
                       <LaddaButton
                       
                        
                        className="btn btn-primary btn-ladda"
                        
                        onClick={e => {
                           history.push("/app/complaint/list")
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="danger" className="fa fa-check" /> Confirmar
                      </LaddaButton>
                    
                      <LaddaButton
                        style={{marginLeft:"10px"}}
                        className="btn btn-danger btn-ladda"
                        
                        onClick={e => {
                           if(this.props.coords) {
                              this.geoDecode(this.props.coords.latitude, this.props.coords.longitude)
                            }
       
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Minha Localização
                      </LaddaButton>
                      
                  
                  </div>
              
        

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro salvando Vistoriador
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency
};

const routerSeachContainers = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SeachContainers)
);


export default geolocated({
  positionOptions: {
    enableHighAccuracy: true,
  },
  userDecisionTimeout: 5000,
})(routerSeachContainers);
