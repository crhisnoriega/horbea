import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  ButtonGroup,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
}
from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser, setEditAgency } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from 'react-tooltip'

import data from "./_data_agency";

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: [],
      filter: "",
      show: false,
      users: [],
      showconfirm: false,
      currentagency: null
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
    /* axios
       .post(
         "/api/getapicall",
         {
           url: "/users/search?query=" + filter.toLowerCase(),
           data: "",
           headers: {
             "Content-Type": "application/json",
             Authorization: "Basic " + this.props.session.token
           }
         },
         {
           headers: {
             "Content-Type": "application/json"
           }
         }
       )
       .then(res => {
         console.log(res.data);
         
         this.setState({
           users: res.data,
           show: false
         });
       });*/
  }

  componentDidMount() {
    axios
      .get("/agency/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        }
        else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.props.setEditUser(user);
  };

  onEditAgency = agency => {
    console.log(agency);
    this.props.setEditAgency(agency);
    history.push("/app/agency/register");
  };

  onDeleteAgency = agency => {
    console.log(agency);
    axios
      .delete("/agency/" + agency.id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        let filteredArray = this.state.table.filter(s => s.id !== agency.id);
        this.setState({ table: filteredArray, showconfirm: false });
      })
      .catch(error => console.log(error));
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="edit"
          data-tip="Editar Imobiliaria"
          color="primary"
          className="btn-brand"
          onClick={e => {
            this.onEditAgency(row);
          }}
        >
          <i className="fa fa-clipboard" />
        </Button>

        <Button
          data-for="remove"
          data-tip="Apagar Imobiliaria"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.setState({currentagency:row, showconfirm:true});
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>
        
        <ReactTooltip id='edit' multiline={true}/>
        <ReactTooltip id='remove' multiline={true}/>
      </div>
    );
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("DD/MM/YYYY HH:mm:SS");
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>A caçamba que você deseja denunciar está na lista abaixo?</CardHeader>
              <CardBody>
                <div className="row justify-content-center mb-3 mt-3">
                <Button
                    style={{marginLeft:"10px", width:"150px"}}
                    color="primary"
                    onClick={e => {
                      this.props.setEditAgency(null);
                      alert("Obrigado por colaborar!");
                      history.push("/app/select-map");
                    }}
                  >
                    <i color="primary" className="fa fa-edit" /> Está
                  </Button>
                  
                  <Button
                    style={{marginLeft:"10px", width:"150px"}}
                    color="danger"
                    onClick={e => {
                      this.props.setEditAgency(null);
                      history.push("/app/complaint/register");
                    }}
                  >
                    <i color="danger" className="fa fa-edit" /> Nao Está
                  </Button>
                  
                   
                </div>
                <BootstrapTable
                  data={this.state.table}
                  version="4"
                  bordered={false}
                  hover
                  pagination
                  search
                  options={{
                    noDataText: "Sem caçambas em um raio de 100m"
                  }}
                  searchPlaceholder={"Busca"}
                >
                  <TableHeaderColumn  isKey  dataField="number" dataSort>
                    Numero
                  </TableHeaderColumn>
                  <TableHeaderColumn  dataField="address">
                    Endereço
                  </TableHeaderColumn>
                  <TableHeaderColumn  dataField="distancia">
                    Distancia
                  </TableHeaderColumn>
                 
                </BootstrapTable>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
         <Modal isOpen={this.state.showconfirm} className={this.props.className} backdrop={this.state.backdrop}>
          <ModalHeader toggle={this.toggle}>Apagar Imobiliaria</ModalHeader>
          <ModalBody>
            Deseja apagar a imobilaria {this.state.currentagency? this.state.currentagency.name:""} ?
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={(e) => this.onDeleteAgency(this.state.currentagency)}>Confirmar</Button>{' '}
            <Button color="secondary" onClick={(e) => this.setState({showconfirm:false})}>Cancelar</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);
