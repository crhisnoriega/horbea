import React, { Component } from "react";

import {
  ButtonGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import {
  setEditItem,
  addItens,
  setEditUser,
  setEditSurver
} from "../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import AsyncSelect from "react-select/async";
import CreatableSelect from "react-select/creatable";
import AsyncCreatableSelect from "react-select/async-creatable";

import ItemType from "./ItemType";
import ItemTypeAsTable from "./ItemTypeAsTable";

const empty = {
  name: ""
};

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: [],
      cSelected: [],
      options: []
    };

    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
  }

  toggleBtn = name => {};

  componentDidMount() {
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    console.log(this.props.session.item);
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    this.setState({
      init: {
        name: this.props.session.item ? this.props.session.item.name : null
      },
      table: [{ name: "Ar Condicionado" }, { name: "Armario" }]
    });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do item")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do item")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(values);

    if (this.props.session.item) {
      axios
        .post(
          "/environments/updateitem/" + this.props.session.item.id,
          {
            name: values.name,
            options: {
              qualificacao: this.state.qualificacao,
              marcas: this.state.marcas,
              modelos: this.state.modelos,
              materiais: this.state.materiais,
              detalhes: this.state.detalhes,
              pintura: this.state.pintura,
              cores: this.state.cores
            }
          },
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          console.log(res.data);
          this.props.setEditItem(null);
          history.push("/app/itens/list");
        })
        .catch(e => {
          console.log(e);
          this.props.setEditItem(null);
        });

      setSubmitting(false);
    } else {
      axios
        .post(
          "/environments/createitem",
          {
            name: values.name,
            options: {
              qualificacao: this.state.qualificacao,
              marcas: this.state.marcas,
              modelos: this.state.modelos,
              materiais: this.state.materiais,
              detalhes: this.state.detalhes,
              pintura: this.state.pintura,
              cores: this.state.cores
            }
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          console.log(res.data);
          history.push("/app/itens/list");
        });

      setSubmitting(false);
    }
    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/cadastro/register");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Vistoriador"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  loadQual = inputValue => {
    if (inputValue.length < 3) return null;

    return axios
      .post(
        "/api/getapicall",
        {
          url: "/devices/search",
          data: "?query=" + inputValue + "&max=20",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        console.log(results);

        return results;
      })
      .catch(error => {
        var results = [{ value: "all", label: "Todos" }];

        console.log(results);

        return results;
      });
  };

  onRadioBtnClick(rSelected) {
    this.setState({ rSelected });
  }

  render() {
    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />

        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Dados do Item</CardHeader>
                  <CardBody>
                    <FormGroup>
                      <Label className="green-label" for="name">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>

                    <ItemTypeAsTable
                      items={[{ name: "Bom estado" }, { name: "Ruim" }]}
                      example="item em Bom estado"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.qualificacao
                          : null
                      }
                      name="Qualificação"
                      select={e => {
                        this.setState({ qualificacao: e });
                      }}
                    />

                    <ItemTypeAsTable
                      items={[{ name: "Eletrolux" }, { name: "Brastemp" }]}
                      example="item da marca Eletrolux"
                      name="Marcas"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.marcas
                          : null
                      }
                      select={e => {
                        this.setState({ marcas: e });
                      }}
                    />

                    <ItemTypeAsTable
                      items={[{ name: "Modelo 1" }, { name: "Modelo 2" }]}
                      example="item do modelo Modelo 1"
                      name="Modelos"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.modelos
                          : null
                      }
                      select={e => {
                        this.setState({ modelos: e });
                      }}
                    />

                    <ItemTypeAsTable
                      items={[{ name: "Granito" }, { name: "Madeira" }]}
                      name="Materiais"
                      example="item feito no material Granito"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.materiais
                          : null
                      }
                      select={e => {
                        this.setState({ materiais: e });
                      }}
                    />

                    <ItemTypeAsTable
                      items={[{ name: "Com infiltração" }]}
                      example="item com Com infiltração"
                      name="Detalhes"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.detalhes
                          : null
                      }
                      select={e => {
                        this.setState({ detalhes: e });
                      }}
                    />

                    <ItemTypeAsTable
                      items={[]}
                      example="item na pintura"
                      name="Pintura"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.pintura
                          : null
                      }
                      select={e => {
                        this.setState({ pintura: e });
                      }}
                    />

                    <ItemTypeAsTable
                      items={[{ name: "Vermelho" }, { name: "Branco" }]}
                      example="item da cor Vermelho"
                      name="Cores"
                      selected={
                        this.props.session.item &&
                        this.props.session.item.options
                          ? this.props.session.item.options.cores
                          : null
                      }
                      select={e => {
                        this.setState({ cores: e });
                      }}
                    />
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <Button
                        color="primary"
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro salvando Vistoriador
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver,
  addItens,
  setEditItem
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
