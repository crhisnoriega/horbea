import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  ButtonGroup,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditUser, setEditAgency } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from "react-tooltip";

import data from "./_data";

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: data.rows,
      filter: "",
      show: false,
      users: [],
      showconfirm: false,
      currentagency: null
    };
  }

  componentDidMount() {
    axios
      .get("/logs/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  dateFormatter = (cell, row) => {
    return moment(cell).format("DD/MM/YYYY HH:mm:SS");
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>Logs do Item</CardHeader>
              <CardBody>
                <BootstrapTable
                  data={this.state.table}
                  version="4"
                  bordered={false}
                  hover
                  pagination
                  search
                  options={{
                    noDataText: "Sem Informações"
                  }}
                  searchPlaceholder={"Busca"}
                >
                  <TableHeaderColumn
                    defaultSorted
                    width="200"
                    dataField="createdAt"
                    dataSort
                    dataFormat={this.dateFormatter}
                  >
                    Data/Hora
                  </TableHeaderColumn>
                  <TableHeaderColumn width="200" dataField="user" dataSort>
                    Usuario
                  </TableHeaderColumn>
                  <TableHeaderColumn width="200" isKey dataField="register">
                    Cadastro
                  </TableHeaderColumn>
                  <TableHeaderColumn width="200" dataField="ip">
                    IP
                  </TableHeaderColumn>
                  <TableHeaderColumn width="200" dataField="action">
                    Ação
                  </TableHeaderColumn>
                  <TableHeaderColumn width="300" dataField="description">
                    Descrição
                  </TableHeaderColumn>
                </BootstrapTable>
              </CardBody>
            </Card>

            <div style={{ height: "100px", width: "100%" }}>
              <Row className="justify-content-sm-start">
                <Col sm="6" md={{ size: 4 }}>
                  <Button
                    color="primary"
                    type="submit"
                    className="col-6 btn btn-primary btn-ladda"
                    onClick={e => {
                      history.goBack();
                    }}
                  >
                    <i color="primary" className="fa fa-arrow" /> Voltar
                  </Button>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.showconfirm}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}>Apagar Imobiliaria</ModalHeader>
          <ModalBody>
            Deseja apagar a imobilaria{" "}
            {this.state.currentagency ? this.state.currentagency.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => this.onDeleteAgency(this.state.currentagency)}
            >
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={e => this.setState({ showconfirm: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditUser,
  setEditAgency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);
