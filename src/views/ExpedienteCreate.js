import React, { Component } from "react";

import {
  ButtonGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { addRule, doLogin, setEditUser, setEditSurver } from "../redux/redux";

import InputMask from "react-input-mask";
import { CompactPicker } from "react-color";
// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import moment from "moment";

import history from "../history/history";

import ReactTooltip from "react-tooltip";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ItemType from "./ItemType";

import AsyncCreatableSelect from "react-select/async-creatable";
import AsyncSelect from "react-select/async";

const empty = {
  name: ""
};

class ExpedienteCreate extends Component {
  constructor(props) {
    super(props);

    this.itensTable = {};

    this.expediente = null;

    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: [],
      cSelected: [],
      itens: [],
      values: {},
      data: {},
      currentrow: null,
      currentvalue: "",

      currentitens: [],

      selectedrow: null,
      color: "red",

      showColor: null
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    var json = localStorage.getItem("expediente");
    if (json) {
      this.expediente = JSON.parse(json);

      localStorage.removeItem("expediente");
      this.setState({ init: { name: this.expediente.name } });
    }

    axios
      .get("/expediente/search?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do ambiente")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome da chave")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(this.itensTable);
    console.log(this.state.table);

    values.agency = this.state.table;
    values.value2 = this.itensTable;
    values.value = this.state.color;
    values.rules = this.props.session.rules;

    axios
      .post("/expediente", values, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ showinfo: true });
        setSubmitting(false);
      })
      .catch(error => {
        this.setState({ showerror: true, messageerror: JSON.stringify(error) });
        setSubmitting(false);
      });

    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/expediente/list");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Chave"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  loadItens = inputValue => {
    return axios
      .get("/environments/itens", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  loadEnvironments = inputValue => {
    return axios
      .get("/environments", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleEditableInput = e => {};

  handleInputChange = (row, event) => {
    let data = [...this.state.data];
    data[row.value] = event.target.value;

    this.setState({ data });
  };

  addItensToEnv = (agencyid, value) => {
    console.log(agencyid);
    console.log(value);

    this.itensTable[agencyid] = value;
  };

  editFormater = (cell, row) => {
    return (
      <div>
        {this.state.currentrow && this.state.currentrow.id == row.id ? (
          <InputGroup>
            <Input
              name="input"
              type="text"
              onChange={e => {
                this.setState({ currentvalue: e.target.value }, () => {
                  this.addItensToEnv(
                    this.state.currentrow.id,
                    this.state.currentvalue
                  );
                });
              }}
              value={this.state.currentvalue}
              mask="999.99"
              maskChar=" "
              tag={InputMask}
            />
            <InputGroupAddon addonType="prepend">
              <Button
                type="button"
                color="danger"
                onClick={e => {
                  row.label = this.state.currentvalue;
                  this.addItensToEnv(this.state.currentrow.id, null);
                  this.setState({
                    currentrow: null,
                    currentvalue: null
                  });
                }}
              >
                <i className="fa fa-remove" /> Cancelar
              </Button>
            </InputGroupAddon>
          </InputGroup>
        ) : (
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button
                type="button"
                color="primary"
                onClick={e => {
                  this.setState({
                    currentrow: row
                  });
                }}
              >
                <i className="fa fa-check" /> Habilitar
              </Button>
            </InputGroupAddon>
          </InputGroup>
        )}
      </div>
    );
  };

  photoFormater = (cell, row) => {
    return <src className="sem-imagem-small" src={row.photo} />;
  };

  typeFormater = (cell, row) => {
    if (row.type == "semanal") {
      return row.days_week;
    }
    if (row.type == "anual") {
      return row.day.label + " " + row.mes.label;
    }

    if (row.type == "dia") {
      return moment(row.oneday).format("DD/MM/YYYY HH:mm:SS");
    }
    return <div />;
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="remove"
          data-tip="Apagar Regra"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            this.setState({ currentsurver: row, showconfirm: true });
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="edit" multiline={true} />
        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  onDeleteRule = surver => {
    console.log(surver);
    let filteredArray = this.props.session.rules.filter(
      s => s.name !== surver.name
    );
    this.props.addRule(filteredArray);
    this.setState({ showconfirm: false });
  };

  render() {
    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />

        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Dados do Expediente</CardHeader>
                  <CardBody>
                    <Row>
                      <Col xs="8">
                        <FormGroup>
                          <Label className="green-label" for="name">
                            Nome
                          </Label>
                          <InputGroup>
                            <Input
                              invalid={touched.name && !!errors.name}
                              type="text"
                              name="name"
                              id="name"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.name}
                            />
                            <FormFeedback>{errors.name}</FormFeedback>
                          </InputGroup>
                          <FormFeedback>{errors.name}</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Regras</CardHeader>

                  <CardBody>
                    <ButtonGroup>
                      <Button
                        color="primary"
                        onClick={e => {
                          history.push("/app/rule/create");
                        }}
                      >
                        <i color="primary" className="fa fa-edit" /> Nova Regra
                      </Button>
                    </ButtonGroup>
                    <BootstrapTable
                      data={this.props.session.rules}
                      version="4"
                      bordered={false}
                      hover
                      pagination
                      search
                      options={{
                        noDataText: "Sem Informações"
                      }}
                      searchPlaceholder={"Busca"}
                    >
                      <TableHeaderColumn
                        width="200"
                        isKey
                        dataField="name"
                        dataSort
                      >
                        Nome
                      </TableHeaderColumn>

                      <TableHeaderColumn width="200" dataField="work" dataSort>
                        Trabalha
                      </TableHeaderColumn>
                      <TableHeaderColumn width="200" dataField="type" dataSort>
                        Tipo
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="200"
                        dataField="days_week"
                        dataSort
                        dataFormat={this.typeFormater}
                      >
                        Dia
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        width="200"
                        dataField="inicio"
                        dataSort
                      >
                        Inicio
                      </TableHeaderColumn>
                      <TableHeaderColumn width="200" dataField="final" dataSort>
                        Termino
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        defaultSorted
                        width="200"
                        dataField="createdAt"
                        dataSort
                        dataFormat={this.dateFormatter}
                      >
                        Ultima Atualização
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        defaultSorted
                        width="200"
                        dataField="createdAt"
                        dataSort
                        dataFormat={this.buttonFormatter}
                      />
                    </BootstrapTable>
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <LaddaButton
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </LaddaButton>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro efetuando Cadastro
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.showconfirm}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}>Apagar Regra</ModalHeader>
          <ModalBody>
            Deseja apagar Regra{" "}
            {this.state.currentsurver ? this.state.currentsurver.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => this.onDeleteRule(this.state.currentsurver)}
            >
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={e => this.setState({ showconfirm: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = { addRule };

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ExpedienteCreate)
);
