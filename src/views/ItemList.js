import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setEditItem } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ReactTooltip from "react-tooltip";

import data from "./_data_agency";

class ItemList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: [],
      filter: "",
      show: false,
      users: [],
      showconfirm: false,
      currentsurver: null
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
  }

  componentDidMount() {
    console.log(this.props.session.itens);
    axios
      .get("/environments/itens", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({ table: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  onChange = e => {
    var _this = this;
    if (this.state.typeTimer) {
      clearTimeout(this.state.typeTimer);
    }

    var filter = e.target.value;

    this.setState({
      typeTimer: setTimeout(function() {
        if (filter.length >= 3) {
          _this.searchUser(filter);
        } else {
          _this.setState({ devices: [] });
        }
      }, 1000)
    });
  };

  callback = user => {
    this.props.setEditUser(user);
  };

  onEditSurver = surver => {
    console.log(surver);

    if (surver != null) {
      surver.stopPropagation();
    }

    history.push("/app/itens/register");
  };

  onDeleteSurver = surver => {
    console.log(surver);
    axios
      .delete("/environments/itens/" + surver.id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        let filteredArray = this.state.table.filter(s => s.id !== surver.id);
        this.setState({ table: filteredArray, showconfirm: false });
      })
      .catch(error => console.log(error));
  };

  buttonFormatter = (cell, row, enumObject, rowIndex) => {
    console.log(cell);
    console.log(row);
    console.log(enumObject);
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="edit"
          data-tip="Editar Vistoriador"
          color="primary"
          className="btn-brand"
          onClick={e => {
            e.stopPropagation();
            console.log(row);
            if (row != null) {
              this.props.setEditItem(row);
            }
            history.push("/app/itens/create");
          }}
        >
          <i color="primary" className="fa fa-edit" />
        </Button>

        <Button
          data-for="remove"
          data-tip="Apagar Item"
          color="danger"
          style={{ marginLeft: "5px" }}
          className="btn-brand"
          onClick={e => {
            e.stopPropagation();
            this.setState({ currentsurver: row, showconfirm: true });
          }}
        >
          <i color="danger" className="fa fa-remove" />
        </Button>

        <ReactTooltip id="edit" multiline={true} />
        <ReactTooltip id="remove" multiline={true} />
      </div>
    );
  };

  dateFormatter = (cell, row) => {
    return moment(cell).format("DD/MM/YYYY HH:mm:SS");
  };

  rowItemClick = item => {
    console.log(item);
    if (item != null) {
      this.props.setEditItem(item);
    }
    history.push("/app/itens/create");
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>Itens</CardHeader>

              <CardBody>
                <ButtonGroup>
                  <Button
                    color="primary"
                    onClick={e => {
                      history.push("/app/itens/create");
                    }}
                  >
                    <i color="primary" className="fa fa-edit" />
                    Adicionar Item
                  </Button>
                </ButtonGroup>
                <BootstrapTable
                  data={this.state.table}
                  version="4"
                  bordered={false}
                  hover
                  pagination
                  search
                  options={{
                    noDataText: "Sem Informações",
                    onRowClick: this.rowItemClick
                  }}
                  searchPlaceholder={"Busca"}
                >
                  <TableHeaderColumn
                    width="300"
                    isKey
                    dataField="name"
                    dataSort
                  >
                    Nome
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    defaultSorted
                    width="200"
                    dataField="createdAt"
                    dataSort
                    dataFormat={this.dateFormatter}
                  >
                    Ultima Alteração
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    width="150"
                    dataField="button"
                    dataFormat={this.buttonFormatter}
                  />
                </BootstrapTable>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.showconfirm}
          className={this.props.className}
          backdrop={this.state.backdrop}
        >
          <ModalHeader toggle={this.toggle}>Apagar Vistoriador</ModalHeader>
          <ModalBody>
            Deseja apagar o item{" "}
            {this.state.currentsurver ? this.state.currentsurver.name : ""} ?
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={e => this.onDeleteSurver(this.state.currentsurver)}
            >
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={e => this.setState({ showconfirm: false })}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditItem
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemList);
