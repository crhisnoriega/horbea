import React, { Component } from "react";

import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardHeader,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import { doLogin, setEditUser, setEditSurver } from "../redux/redux";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
// import "react-select/dist/react-select.min.css";

import history from "../history/history";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/dist//react-bootstrap-table-all.min.css";

import ItemType from "./ItemType";

import AsyncCreatableSelect from "react-select/async-creatable";
import AsyncSelect from "react-select/async";

const empty = {
  name: ""
};

class ModelSurverCreate extends Component {
  constructor(props) {
    super(props);

    this.itensTable = {};
    this.model = null;

    this.state = {
      deviceReadonly: {},
      disabled: {},
      readonly: {},
      administrator: {},
      viewer: {},
      init: empty,
      isEdit: false,
      showerror: false,
      showinfo: false,
      table: [],
      cSelected: [],
      itens: [],
      values: {},
      data: {},
      currentrow: null,
      currentvalue: "",

      currentitens: [],

      selectedrow: null
    };
  }

  toggleBtn = name => {};

  componentDidMount() {
    this.model = null;
    var json = localStorage.getItem("model");
    if (json) {
      this.model = JSON.parse(json);
      this.itensTable = this.model.itens;
      this.setState({
        table: this.model.environments,
        init: { name: this.model.name }
      });
      localStorage.removeItem("model");
    }
    this.setState({
      itens: [
        {
          value: "arcondicionado",
          label: "Ar Condicionado",
          data: { id: "123" }
        },
        { value: "armario", label: "Armario", data: { id: "123" } }
      ]
    });
  }

  // validation
  validationSchemaComplete = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do modelo")
    });
  };

  validationSchemaEdit = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do modelo")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    console.log(this.itensTable);
    console.log(this.state.table);

    values.environments = this.state.table;
    values.itens = this.itensTable;

    if (this.model) {
      axios
        .put("/model/" + this.model.id, values, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          this.setState({ showinfo: true });
          setSubmitting(false);
        })
        .catch(error => {
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    } else {
      axios
        .post("/model", values, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(res => {
          this.setState({ showinfo: true });
          setSubmitting(false);
        })
        .catch(error => {
          this.setState({
            showerror: true,
            messageerror: JSON.stringify(error)
          });
          setSubmitting(false);
        });
    }

    return;
  };

  handleCloseError = e => {
    this.setState({ showerror: false });
  };

  handleCloseInfo = e => {
    history.push("/app/model/list");
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    console.log(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  checkFormatter = (cell, row, enumObject, rowIndex) => {
    var _cell = cell;
    return (
      <div>
        <Button
          data-for="check"
          data-tip="Editar Vistoriador"
          color={this.state.cSelected.includes(row) ? "primary" : "danger"}
          className="btn-brand"
          onClick={() => this.onCheckboxBtnClick(row)}
          active={this.state.cSelected.includes(row)}
        >
          <i className="fa fa-check" />
        </Button>
      </div>
    );
  };

  loadItens = inputValue => {
    return axios
      .get("/environments/itens", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  loadEnvironments = inputValue => {
    return axios
      .get("/environments", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [{ value: "all", label: "Todos" }];

        for (var index = 0; index < res.data.length; index++) {
          var item = res.data[index];
          results.push({ value: item.id, label: item.name });
        }

        this.setState({ all: results });

        return results;
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleEditableInput = e => {};

  handleInputChange = (row, event) => {
    let data = [...this.state.data];
    data[row.value] = event.target.value;

    this.setState({ data });
  };

  addItensToEnv = (environmentid, itens) => {
    console.log(environmentid);
    console.log(itens);

    var envItens = this.itensTable[environmentid];

    if (envItens == null) {
      envItens = [];
    }

    var tmp = envItens.concat(itens);

    this.itensTable[environmentid] = tmp;

    this.setState({
      currentitens: tmp
    });
  };

  updateCurrentItensToEnv = environmentid => {
    console.log("update");
    console.log(environmentid);
    console.log(this.state.currentitens);
    this.itensTable[environmentid] = this.state.currentitens;
  };

  rowClick = e => {
    console.log(e);
    this.setState({
      currentitens: this.itensTable[e.value],
      selectedrow: e
    });
  };

  editFormater = (cell, row) => {
    return (
      <div>
        {this.state.currentrow == row.value ? (
          <InputGroup>
            <Input
              name="input"
              type="text"
              onChange={e => {
                this.setState({ currentvalue: e.target.value });
              }}
              value={this.state.currentvalue}
            />
            <InputGroupAddon addonType="prepend">
              <Button
                type="button"
                color="primary"
                onClick={e => {
                  row.label = this.state.currentvalue;
                  this.setState({
                    currentrow: null,
                    currentvalue: null
                  });
                }}
              >
                <i className="fa fa-search" /> Confirmar
              </Button>
            </InputGroupAddon>
          </InputGroup>
        ) : (
          <InputGroup>
            <Input
              className="myitemtable"
              name="input"
              type="text"
              value={row.label}
            />
            <InputGroupAddon addonType="prepend">
              <Button
                type="button"
                color="primary"
                onClick={e => {
                  this.setState({
                    currentrow: row.value,
                    currentvalue: row.label
                  });
                }}
              >
                <i className="fa fa-search" /> Editar
              </Button>
            </InputGroupAddon>
          </InputGroup>
        )}
      </div>
    );
  };

  render() {
    var _init = this.state.init;

    return (
      <div className="animated fadeIn">
        <Loading show={this.state.show} showSpinner={false} />

        <Formik
          enableReinitialize
          initialValues={_init}
          validate={this.myValidator(
            this.state.isEdit
              ? this.validationSchemaEdit
              : this.validationSchemaComplete
          )}
          onSubmit={this.handleSubmit}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched
          }) => (
            <Container>
              <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                <Card>
                  <CardHeader>Dados</CardHeader>
                  <CardBody>
                    <FormGroup>
                      <Label className="green-label" for="name">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>
                  </CardBody>
                </Card>

                <Card>
                  <CardHeader>Ambientes</CardHeader>
                  <CardBody>
                    <Row>
                      <FormGroup className="col-md-6">
                        <Label className="green-label" for="name">
                          Lista de Ambientes
                        </Label>
                        <AsyncSelect
                          className="my-col-8"
                          defaultOptions
                          value={values.environments}
                          loadOptions={this.loadEnvironments}
                          onChange={e => {
                            console.log(e);

                            this.setState({
                              table: [e, ...this.state.table],
                              selectedrow: e
                            });

                            values.environments = null;

                            axios
                              .get("/environments/itens/" + e.value, {
                                headers: {
                                  "Content-Type": "application/json"
                                }
                              })
                              .then(res => {
                                console.log(res.data);
                                this.addItensToEnv(e.value, res.data);
                              });
                          }}
                          formatCreateLabel={userInput =>
                            `Criar nova opção: ${userInput}`
                          }
                          loadingMessage={e =>
                            `Carregando opções próximas de '${e.inputValue}'`
                          }
                          noOptionsMessage={e =>
                            `Sem opções disponíveis para escolha`
                          }
                          placeholder="Adicione um ambiente"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />
                        <BootstrapTable
                          tableHeaderClass={"col-hidden"}
                          data={this.state.table}
                          version="4"
                          bordered={false}
                          hover
                          options={{
                            noDataText: "Sem Informações",
                            onRowClick: this.rowClick
                          }}
                        >
                          <TableHeaderColumn
                            width="300"
                            dataField="edit"
                            isKey
                            dataSort
                            dataFormat={this.editFormater}
                          />
                        </BootstrapTable>
                      </FormGroup>

                      <FormGroup className="col-md-6">
                        <Label className="green-label">Lista de Itens</Label>

                        <AsyncSelect
                          className="my-col-8"
                          isMulti
                          defaultOptions
                          value={this.state.currentitens}
                          loadOptions={this.loadItens}
                          onChange={e => {
                            console.log(e);
                            this.setState(
                              {
                                currentitens: e
                              },
                              () => {
                                if (this.state.selectedrow) {
                                  this.updateCurrentItensToEnv(
                                    this.state.selectedrow.value
                                  );
                                }
                              }
                            );
                          }}
                          formatCreateLabel={userInput =>
                            `Criar nova opção: ${userInput}`
                          }
                          loadingMessage={e =>
                            `Carregando opções próximas de '${e.inputValue}'`
                          }
                          noOptionsMessage={e =>
                            `Sem opções disponíveis para escolha`
                          }
                          placeholder="Selecione o itens"
                          theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "white",
                              primary: "#97B834"
                            }
                          })}
                        />
                      </FormGroup>
                    </Row>
                  </CardBody>
                </Card>

                <div style={{ height: "100px", width: "100%" }}>
                  <Row className="justify-content-sm-start">
                    <Col sm="6" md={{ size: 4 }}>
                      <Button
                        color="primary"
                        type="submit"
                        className="col-6 btn btn-primary btn-ladda"
                        loading={isSubmitting}
                        onClick={e => {
                          handleSubmit(e);
                          this.toggleBtn("expLeft");
                        }}
                      >
                        <i color="primary" className="fa fa-check" /> Salvar
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Form>
            </Container>
          )}
        />

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro efetuando Cadastro
          </ModalHeader>
          <ModalBody>{this.state.messageerror}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleCloseError(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showinfo}
          toggle={this.toggleInfo}
          className={"modal-primary"}
        >
          <ModalHeader toggle={this.toggleInfo}>Salvar Vistoriador</ModalHeader>
          <ModalBody>Cadastro com sucesso!</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={e => this.handleCloseInfo(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
  userEdit: state.userEdit
});

const mapDispatchToProps = {
  setEditUser,
  setEditSurver
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ModelSurverCreate)
);
