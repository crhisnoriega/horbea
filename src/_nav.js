export default {
  items: [
    {
      title: true,
      name: "Agendamento",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "" // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Dashboard",
      url: "/app/dashboard",
      icon: "icon-calendar"
    },
    {
      name: "Agendar",
      url: "/app/booking",
      icon: "icon-directions"
    },
    {
      name: "Historico",
      url: "/app/bookinglist",
      icon: "icon-list"
    }

    /*{
      name: "Denuncias",
      url: "/agency",
      icon: "icon-home",
      children: [
        {
          name: "Mapa",
          url: "/app/select-map",
          icon: "icon-note"
        },
        
        {
          name: "Busca",
          url: "/app/complaint/search",
          icon: "icon-note"
        },
        {
          name: "Caçambas",
          url: "/app/complaint/list",
          icon: "icon-note"
        },
        {
          name: "Registro",
          url: "/app/complaint/register",
          icon: "icon-note"
        }
      ]
    }*/
  ]
};
