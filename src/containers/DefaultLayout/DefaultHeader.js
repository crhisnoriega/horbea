import React, { Component } from "react";
import {
  Row,
  Badge,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav
}
from "reactstrap";
import PropTypes from "prop-types";

import {
  AppAsideToggler,
  AppHeaderDropdown,
  AppNavbarBrand,
  AppSidebarToggler
}
from "@coreui/react";

import DefaultHeaderDropdown from "./DefaultHeaderDropdown";


import logo from "../../assets/img/brand/colabora.png";
import logo2 from "../../assets/img/brand/colabora.png";

import sygnet from "../../assets/img/brand/sygnet.svg";
import avatar from "../../assets/img/avatars/6.jpg";

import history from "../../history/history";

import { connect } from "react-redux";
import { clearSession } from "../../redux/redux";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
      offers: [],
      avatar: null
    };
  }
  componentDidMount() {}

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  logoutHandler(e) {
    this.props.clearSession({});
    history.push("/login");
  }


  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand style={{cursor:"pointer"}} onClick={e => history.push("/app/dashboard")}
          full={{ src: logo, width: 47, height: 47, alt: "Colabora" }}
          minimized={{ src: logo2, width: 200, alt: "Colabora" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown>
            <Dropdown  style={{ marginRight:"20px" }} isOpen={this.state.dropdownOpen} toggle={this.toggle}>
             <Row>
                <DropdownToggle nav>
                  
                </DropdownToggle>
              </Row>

              <DropdownMenu right>
                <DropdownItem header tag="div" className="text-center">
                  <div style={{color:"white", fontSize:"14px"}}>Conta</div>
                </DropdownItem>
                <DropdownItem onClick={e => history.push("/app/profile")}>
                  <i className="fa fa-user" />
                  Perfil
                </DropdownItem>
                <DropdownItem onClick={e => this.logoutHandler(e)}>
                  <i className="fa fa-lock" />
                  Sair
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </AppHeaderDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-md-down-none" />*/}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => ({
  session: state.session
  // geod: state.geod,
});

const mapDispatchToProps = {
  clearSession
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultHeader);
