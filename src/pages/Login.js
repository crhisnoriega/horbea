import { withRouter, Link } from "react-router-dom";
import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Media,
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";

import axios from "axios";
import history from "../history/history";
import base64 from "react-native-base64";

import logo from "../assets/img/brand/colabora.png";

import { connect } from "react-redux";
import { activateGeod, closeGeod, doLogin, setToken } from "../redux/redux";

import StringMask from "string-mask";
import InputMask from "react-input-mask";

import scriptLoader from "react-async-script-loader";
import ProgressButton from "react-progress-button";

import { geolocated } from "react-geolocated";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      password: "",
      validate: {
        invaliduser: false,
        invalidpassword: false
      },
      showerror: false,
      message: "",
      loading: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  loadJS = function(src) {
    var tag = document.createElement("script");
    tag.async = false;
    tag.type = "text/javascript";
    tag.src = src;
    document.getElementsByTagName("body")[0].appendChild(tag);
  };

  loadCSS = function(src) {
    var tag = document.createElement("script");
    tag.async = false;
    tag.type = "text/javascript";
    tag.src = src;
    document.getElementsByTagName("body")[0].appendChild(tag);
  };

  componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed }) {}

  componentDidMount() {}

  handleSuccess = mediaStream => {
    alert(mediaStream);
  };

  routeChange(e) {
    e.preventDefault();
    console.log(this.state.user);
    console.log(this.state.password);

    let login = this.state.user.replace(/\./g, "").replace(/\-/g, "");

    axios
      .post(
        "https://b5y6qy86zj.execute-api.us-east-1.amazonaws.com/v1/users",
        { type: "login", login: login },
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          }
        }
      )
      .then(res => {
        if (res.data.body != null) {
          if (res.data.body.password == this.state.password) {
            res.data.body.password = null;
            this.props.doLogin(res.data.body);
            history.push("/app/select-map");
          } else {
            this.setState({
              showerror: true,
              message: "Senha inválida"
            });
          }
        } else {
          this.setState({
            showerror: true,
            message: "Usuário ou senha inválidos"
          });
        }
      });
  }

  handleClose(e) {
    this.setState({ showerror: false, showerrornetwork: false });
  }

  onTakePhoto(dataUri) {
    // Do stuff with the dataUri photo...
    console.log("takePhoto");
  }

  handleLogin = () => {
    this.setState({ loading: true });
    axios
      .post(
        "/agenda/ajax_login_action_json.php",
        "vEmail=" + this.state.user + "&vPassword=" + this.state.password,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(res => {
        console.log(res.data);

        this.setState({ loading: false });
        if (typeof res.data === 'object') {          
          this.props.doLogin(res.data);
          history.push("/app/dashboard");
        } else {
          this.setState({
            showerror: true,
            message: "usuário ou senha inválidos"
          });
        }
      });
  };

  geoDecode = (lat, lng) => {
    axios
      .get(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
          lat +
          "," +
          lng +
          "&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAmGiadpYHW4yK67VMqMTnf8bpt6Q9OIWk",
        {}
      )
      .then(response =>
        alert(JSON.stringify(response.data.results[0].formatted_address))
      );
  };

  onTakePhoto(dataUri) {
    // Do stuff with the dataUri photo...
    alert("takePhoto");
  }

  render() {
    return (
      <div id="login_body" className="login_app flex-row align-items-center">
        <Container style={{ marginTop: "-100px" }}>
          <Row className="justify-content-center" style={{ marginTop: "90px" }}>
            <Col md="4">
              <CardGroup
                style={{
                  background: "rgba(233, 84, 39, 0.02)",
                  borderRadius: "5px",
                  border: "1px solid #E95327"
                }}
              >
                <Card className="p-2">
                  <CardBody>
                    <Form className="form" onSubmit={e => this.routeChange(e)}>
                      <Row>
                        <Media
                          style={{
                            display: "table",
                            margin: "0 auto",
                            height: "150px"
                          }}
                          object
                          src={logo}
                        />
                      </Row>

                      <div
                        style={{ textAlign: "center", marginBottom: "30px" }}
                      >
                        <p
                          style={{
                            fontSize: "14px",
                            color: "#E95327",
                            fontWeight: "bold",
                            align: "center"
                          }}
                        >
                          CENTRAL DE
                          <br />
                          <span
                            style={{ color: "#E95327", fontWeight: "bold" }}
                          >
                            {" "}
                            AGENDAMENTOS{" "}
                          </span>{" "}
                          <br />
                        </p>
                      </div>

                      <InputGroup className="mb-2">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="user"
                          type="text"
                          placeholder="Email"
                          returnKeyType="go"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            this.handleInputChange(e);
                          }}
                          invalid={this.state.validate.invaliduser}
                        />
                      </InputGroup>
                      <InputGroup className="mb-2">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="password"
                          type="password"
                          placeholder="Senha"
                          autoComplete="current-password"
                          value={this.state.password}
                          onChange={e => {
                            this.handleInputChange(e);
                          }}
                          invalid={this.state.validate.invalidpassword}
                        />
                      </InputGroup>
                      <Row className="mt-1">
                        <LaddaButton
                          style={{ display: "table", margin: "0 auto" }}
                          color="primary"
                          className="col-6 btn btn-primary btn-ladda"
                          onClick={e => this.handleLogin()}
                          loading={this.state.loading}
                        >
                          Entrar
                        </LaddaButton>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>{this.state.message}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerrornetwork}
          toggle={this.toggleInfo}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>Erro inesperado</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  activateGeod,
  closeGeod,
  doLogin,
  setToken
};

const routerLogin = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);

export default routerLogin;
