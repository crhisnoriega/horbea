import React from "react";
import Loadable from "react-loadable";

function Loading() {
  return <div style={{color:"#E95327"}}>Carregando...</div>;
}

const Hello = Loadable({
  loader: () => import("./pages/Hello"),
  loading: Loading
});

const Dashboard = Loadable({
  loader: () => import("./views/Dashboard"),
  loading: Loading
});

const Calendar = Loadable({
  loader: () => import("./views/Calendar"),
  loading: Loading
});

const Devices = Loadable({
  loader: () => import("./views/Devices"),
  loading: Loading
});

const DevicesList = Loadable({
  loader: () => import("./views/DevicesList"),
  loading: Loading
});



const Reports = Loadable({
  loader: () => import("./views/Reports"),
  loading: Loading
});

const UsersList = Loadable({
  loader: () => import("./views/UsersList"),
  loading: Loading
});

const Users = Loadable({
  loader: () => import("./views/Users"),
  loading: Loading
});

const Surveyors = Loadable({
  loader: () => import("./views/Surveyors"),
  loading: Loading
});

const SurveyorsList = Loadable({
  loader: () => import("./views/SurveyorsList"),
  loading: Loading
});

const RegisterBooking = Loadable({
  loader: () => import("./views/RegisterBooking"),
  loading: Loading
});

const AgencyList = Loadable({
  loader: () => import("./views/AgencyList"),
  loading: Loading
});

const SurverCreate = Loadable({
  loader: () => import("./views/SurverCreate"),
  loading: Loading
});

const EnvironmentList = Loadable({
  loader: () => import("./views/EnvironmentList"),
  loading: Loading
});

const EnvironmentCreate = Loadable({
  loader: () => import("./views/EnvironmentCreate"),
  loading: Loading
});

const ItemList = Loadable({
  loader: () => import("./views/ItemList"),
  loading: Loading
});

const ItemCreate = Loadable({
  loader: () => import("./views/ItemCreate"),
  loading: Loading
});

const SurverList = Loadable({
  loader: () => import("./views/SurverList"),
  loading: Loading
});

const ModelSurverCreate = Loadable({
  loader: () => import("./views/ModelSurverCreate"),
  loading: Loading
});

const ModelSurverList = Loadable({
  loader: () => import("./views/ModelSurverList"),
  loading: Loading
});


const Map = Loadable({
  loader: () => import("./views/Map"),
  loading: Loading
});

const MapAdmin = Loadable({
  loader: () => import("./views/MapAdmin"),
  loading: Loading
});


const RegisterBookingNow = Loadable({
  loader: () => import("./views/RegisterBookingNow"),
  loading: Loading
});

const ServiceSurverCreate = Loadable({
  loader: () => import("./views/ServiceSurverCreate"),
  loading: Loading
});

const ServiceSurveList = Loadable({
  loader: () => import("./views/ServiceSurveList"),
  loading: Loading
});

const ProfileEdit = Loadable({
  loader: () => import("./views/ProfileEdit"),
  loading: Loading
});

const KeysRoomList = Loadable({
  loader: () => import("./views/KeysRoomList"),
  loading: Loading
});

const KeysRoomCreate = Loadable({
  loader: () => import("./views/KeysRoomCreate"),
  loading: Loading
});

const MedidoresList = Loadable({
  loader: () => import("./views/MedidoresList"),
  loading: Loading
});

const MedidoresCreate = Loadable({
  loader: () => import("./views/MedidoresCreate"),
  loading: Loading
});

const RuleExpedienteCreate = Loadable({
  loader: () => import("./views/RuleExpedienteCreate"),
  loading: Loading
});

const ExpedienteCreate = Loadable({
  loader: () => import("./views/ExpedienteCreate"),
  loading: Loading
});

const ExpedienteList = Loadable({
  loader: () => import("./views/ExpedienteList"),
  loading: Loading
});

const Report = Loadable({
  loader: () => import("./views/Report"),
  loading: Loading
});

const LogSurverList = Loadable({
  loader: () => import("./views/LogSurverList"),
  loading: Loading
});

const SurverCreateStatic = Loadable({
  loader: () => import("./views/SurverCreateStatic"),
  loading: Loading
});


const SearchContainers = Loadable({
  loader: () => import("./views/SearchContainers"),
  loading: Loading
});

const ContainerList = Loadable({
  loader: () => import("./views/ContainerList"),
  loading: Loading
});

const BookingList = Loadable({
  loader: () => import("./views/BookingList"),
  loading: Loading
});





const routes = [
  {
    path: "/app/dashboard",
    name: "Painel de Controle",
    component: Dashboard,
    exact: true
  },

  {
    path: "/app/calendar",
    name: "Dashboard",
    component: Calendar,
    exact: true
  },
  {
    path: "/app/users/list",
    name: "Usuarios",
    component: UsersList,
    exact: true
  },
  {
    path: "/app/users/register",
    name: "Registrar Usuario",
    component: Users,
    exact: true
  },
  {
    path: "/app/surveyors/list",
    name: "Vistoriadores",
    component: SurveyorsList,
    exact: true
  },
  {
    path: "/app/surveyors/register",
    name: "Registrar Vistoriador",
    component: Surveyors,
    exact: true
  },
  {
    path: "/app/agency/list",
    name: "Imobiliarias",
    component: AgencyList,
    exact: true
  },
  
  {
    path: "/app/surver/create",
    name: "Adicionar Vistoria",
    component: SurverCreate,
    exact: true
  },
  {
    path: "/app/surver/list",
    name: "Lista Vistoria",
    component: SurverList,
    exact: true
  },
  {
    path: "/app/environments/list",
    name: "Ambientes",
    component: EnvironmentList,
    exact: true
  },
  {
    path: "/app/environments/create",
    name: "Criar Ambiente",
    component: EnvironmentCreate,
    exact: true
  },
  {
    path: "/app/itens/list",
    name: "Itens",
    component: ItemList,
    exact: true
  },
  {
    path: "/app/itens/create",
    name: "Criar Item",
    component: ItemCreate,
    exact: true
  },
  {
    path: "/app/model/create",
    name: "Criar Model",
    component: ModelSurverCreate,
    exact: true
  },
  {
    path: "/app/model/list",
    name: "Listar Model",
    component: ModelSurverList,
    exact: true
  },
  {
    path: "/app/service/create",
    name: "Tipo de Serviço - Criar",
    component: ServiceSurverCreate,
    exact: true
  },
  {
    path: "/app/service/list",
    name: "Tipo de Serviço - Listar",
    component: ServiceSurveList,
    exact: true
  },
  {
    path: "/app/profile",
    name: "Perfil",
    component: ProfileEdit,
    exact: true
  },
  {
    path: "/app/keys/list",
    name: "Chaves",
    component: KeysRoomList,
    exact: true
  },
  {
    path: "/app/keys/create",
    name: "Criar Chaves",
    component: KeysRoomCreate,
    exact: true
  },
  {
    path: "/app/medidor/list",
    name: "Medidores",
    component: MedidoresList,
    exact: true
  },
  {
    path: "/app/medidor/create",
    name: "Criar Medidor",
    component: MedidoresCreate,
    exact: true
  },
  {
    path: "/app/expediente/create",
    name: "Criar Expediente",
    component: ExpedienteCreate,
    exact: true
  },
  {
    path: "/app/expediente/list",
    name: "Expedientes",
    component: ExpedienteList,
    exact: true
  },
  {
    path: "/app/rule/create",
    name: "Criar Regra",
    component: RuleExpedienteCreate,
    exact: true
  },
  {
    path: "/app/settings/report",
    name: "Editar Relatorio",
    component: Report,
    exact: true
  },
  {
    path: "/app/logs/surver",
    name: "Logs Vistoria",
    component: LogSurverList,
    exact: true
  },
  {
    path: "/app/surver/show",
    name: "Visualizar Vistoria",
    component: SurverCreateStatic,
    exact: true
  },
  {
    path: "/app/map",
    name: "Mapa",
    component: Map,
    exact: true
  },
  {
    path: "/app/booking",
    name: "Mapa",
    component: RegisterBookingNow,
    exact: true
  }
,
  {
    path: "/app/complaint/register",
    name: "Registrar Denuncia",
    component: RegisterBooking,
    exact: true
  },
  {
    path: "/app/complaint/search",
    name: "Procurar Denuncia",
    component: SearchContainers,
    exact: true
  },
  {
    path: "/app/complaint/list",
    name: "Caçambas",
    component: ContainerList,
    exact: true
  },
  
  {
    path: "/app/bookinglist",
    name: "Caçambas",
    component: BookingList,
    exact: true
  },
  
  {
    path: "/app/map/admin",
    name: "MapAdmin",
    component: MapAdmin,
    exact: true
  },
    
  
];

export default routes;
