const express = require("express");

const API_PORT = 3001;
const app = express();
const axios = require("axios");
const router = express.Router();
const bodyParser = require("body-parser");
const logger = require("morgan");

const WebSocket = require("ws");
const qs = require("qs");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));

const url = "http://35.237.5.236/api";

//const url = "http://35.237.249.76:8082/api";

////////////////////////////////////////////////////////////
router.post("/login", async(req, res) => {
  console.log(req.body);

  try {
    var r1 = await axios.post(
      url + "/session",
      "email=dem@demo.com&password=demo", {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }
    );

    console.log(r1);

    res.json(r1.data);
  }
  catch (error) {
    console.log(error);
    res.json({ error: error });
  }
});

router.post("/apicall", async(req, res) => {
  console.log("called");
  console.log(req.body);

  try {
    console.log(req.body.url);
    var r1 = await axios.post(url + req.body.url, req.body.data, {
      headers: req.body.headers,
      withCredentials: true
    });

    //console.log(r1.data);

    res.json(r1.data);
  }
  catch (error) {
    console.log(error);
    res.json({ error: error });
  }
});

router.post("/putapicall", async(req, res) => {
  console.log(req.body);
  try {
    var r1 = await axios.put(url + req.body.url, req.body.data, {
      headers: req.body.headers
    });

    console.log(r1.data);

    res.json(r1.data);
  }
  catch (error) {
    console.log(error);
    res.json({ error: error });
  }
});

router.post("/getapicall", async(req, res) => {
  console.log(req.body);
  try {
    var r1 = await axios.get(url + req.body.url + req.body.data, {
      headers: req.body.headers
    });

    console.log("reponse");
    console.log(r1.data);

    res.json(r1.data);
  }
  catch (error) {
    console.log(error);
    res.json({ error: error });
  }
});

router.post("/getreport", async(req, res) => {
  console.log(req.body);
  try {
    var r1 = await axios.get(url + req.body.url + req.body.data, {
      responseType: "arraybuffer",
      headers: req.body.headers
    });

    console.log("reponse");
    console.log(r1);

    const fs = require("fs");
    fs.writeFileSync("file12.xlsx", r1.data);

    res.setHeader("Content-disposition", "attachment; filename=data.xlsx");
    res.setHeader(
      "Content-type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.download("file12.xlsx");
  }
  catch (error) {
    console.log(error);
    res.json({ error: error });
  }
});

conectWebSocket = () => {
  axios
    .post(
      "http://35.237.249.76:8082/api/session",
      qs.stringify({
        email: "admin",
        password: "admin"
      }), {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }
    )
    .then(res => {
      var socket = new WebSocket("ws://35.237.249.76:8082/api/socket");
      console.log(socket);
    })
    .catch(error => {
      console.log(error);
    });
};

// conectWebSocket();

app.use("/api", router);

app.listen(API_PORT, () => console.log(`API port ${API_PORT}`));
